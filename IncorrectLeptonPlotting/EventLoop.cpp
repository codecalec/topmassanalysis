#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <cmath>
#include <unordered_map>

#include "TFile.h"
#include "TH1.h"
#include "EventLoop.h"


EventLoop::EventLoop(std::string treeName, std::vector<std::string> inputFiles)
{
    m_treeName = TString(treeName);

    if (inputFiles.size() == 0) throw std::runtime_error("No input files provided");

    // Initialise TChain and add files to it
    m_chain = new TChain(m_treeName);
    for (std::string inputFile: inputFiles) {
        m_chain->Add(inputFile.c_str()); //convert std::string to char*
        std::cout << "Added file: " << inputFile << std::endl;
    }

    m_data = new Data(m_chain);
}

void EventLoop::execute()
{
    // Counters
    int correct_count = 0;
    int incorrect_count = 0;
    int issue_count = 0;
    int issue_selected_lepton_count = 0;
    int issue_wrong_parent_pdgid_count = 0;
    int issue_jpsi_parent_info_count = 0;

    int correct_no_top_count = 0;

    int W_parent_count = 0;

    int gp_i = -1; // index of grand parent
    MatchingStatus matching_status; // States if event is correctly matched
    LeptonType lepton_type; // States if event is electron or muon channel

    // Info about correct lepton
    std::vector<int>* grand_parent_pdgId = 0;
    std::vector<int>* parent_pdgId = 0;
    std::vector<float>* lepton_truth_pt = 0;
    std::vector<float>* lepton_truth_eta = 0;
    std::vector<float>* lepton_truth_phi = 0;

    int lepton_selected = 0;

    float correct_lepton_pt, correct_lepton_eta, correct_lepton_phi;

    float event_weight;
    TH1F* hist_pt = new TH1F("hist_pt", "Correct Lepton p_t", HIST_NBINS, LEPTON_PT_MIN, LEPTON_PT_MAX);
    TH1F* hist_eta = new TH1F("hist_eta", "Correct Lepton eta", HIST_NBINS, LEPTON_ETA_MIN, LEPTON_ETA_MAX);
    TH1F* hist_phi = new TH1F("hist_phi", "Correct lepton phi", HIST_NBINS, LEPTON_PHI_MIN, LEPTON_PHI_MAX);

    std::unordered_map<int, int> muon_parent_pdgid_map;
    std::unordered_map<int, int> electron_parent_pdgid_map;

    for (int i = 0; i < m_chain->GetEntries(); ++i) {

        m_chain->GetEntry(i);

        // Check if event is incorrectly matched
        matching_status = isCorrectlyMatched();
        switch (matching_status) {
        case correct:
            correct_count++;
            break;
        case incorrect:
            incorrect_count++;
            break;
        case issue:
            issue_count++;
            break;
        case issue_selected_lepton:
            issue_selected_lepton_count++;
            break;
        case issue_wrong_parent_pdgid:
            issue_wrong_parent_pdgid_count++;
            break;
        case issue_jpsi_parent_info:
            issue_jpsi_parent_info_count++;
            break;
        }

        try {
            lepton_type = getLeptonType();
        } catch (const std::runtime_error& e) {
            std::cerr << e.what() << std::endl;
            continue;
        }

        // Find correct lepton for events with selected lepton with no top parent
        if (matching_status == MatchingStatus::issue_wrong_parent_pdgid) {
            try {
                switch (lepton_type) {
                case electron:
		    lepton_selected = m_data->electron_selected;
		    grand_parent_pdgId = m_data->electron_truthGrandParent_pdgId;
                    parent_pdgId = m_data->electron_truthParent_pdgId;
                    lepton_truth_pt = m_data->electron_truth_pt;
                    lepton_truth_eta = m_data->electron_truth_eta;
                    lepton_truth_phi = m_data->electron_truth_phi;

		    electron_parent_pdgid_map[grand_parent_pdgId->at(lepton_selected)] += 1;
                    break;
                case muon:
		    lepton_selected = m_data->muon_selected;
		    grand_parent_pdgId = m_data->muon_truthGrandParent_pdgId;
		    
                    parent_pdgId = m_data->muon_truthParent_pdgId;
                    lepton_truth_pt = m_data->muon_truth_pt;
                    lepton_truth_eta = m_data->muon_truth_eta;
                    lepton_truth_phi = m_data->muon_truth_phi;

		    muon_parent_pdgid_map[grand_parent_pdgId->at(lepton_selected)] += 1;
                    break;
                }
	        

                // Find the lepton with a top grand parent
                for (gp_i = 0; gp_i < grand_parent_pdgId->size(); gp_i++) {
                    if (std::abs(grand_parent_pdgId->at(gp_i)) == 6) {
                        // gp_i is index of grand_parent
                        correct_lepton_pt = lepton_truth_pt->at(gp_i);
                        correct_lepton_eta = lepton_truth_eta->at(gp_i);
                        correct_lepton_phi = lepton_truth_phi->at(gp_i);
                        break;
                    }
                }

                if (gp_i == grand_parent_pdgId->size())
                    throw std::runtime_error("Not lepton with top grandparent found");

                event_weight = getEventWeight(gp_i, lepton_type);
                hist_pt->Fill(correct_lepton_pt, event_weight);
                hist_eta->Fill(correct_lepton_eta, event_weight);
                hist_phi->Fill(correct_lepton_phi, event_weight);
            } catch (const std::runtime_error& e) {
                //std::cerr << e.what() << std::endl;
            };
        }
    }

    std::ofstream pdgid_file;
    pdgid_file.open("muon_pdgid_results.csv");
    for (auto& entry : muon_parent_pdgid_map){
      pdgid_file << entry.first << "," << entry.second << std::endl;
    }
    pdgid_file.close();

    pdgid_file.open("electron_pdgid_results.csv");
    for (auto& entry : electron_parent_pdgid_map){
      pdgid_file << entry.first << "," << entry.second << std::endl;
    }
    pdgid_file.close();
      
    TFile f("IncorrectLeptonResults.root","RECREATE");
    hist_pt->Write();
    hist_eta->Write();
    hist_phi->Write();
    f.Close();

    std::cout << "Correct: " << correct_count << std::endl
              << "Incorrect: " << incorrect_count << std::endl
              << "Issue: " << issue_count << std::endl
              << "Issue - Selected Lepton: " << issue_selected_lepton_count << std::endl
              << "Issue - Lepton does not have correct top parent: " << issue_wrong_parent_pdgid_count << std::endl
              << "Issue - Jpsi does not have a clear top or antitop parent: " << issue_jpsi_parent_info << std::endl;


    std::cout << "Matched with no top grandparent: "
              << correct_no_top_count
              << "/"
              << correct_count << std::endl;

    std::cout << "W Lepton without top grandparent: " << W_parent_count << std::endl;
}

float EventLoop::getEventWeight(int lepton_truth_i, LeptonType lepton_type)
{
    float event_weight = m_data->jvtEventWeight * m_data->pileupEventWeight * m_data->MV2c10_77_EventWeight;

    // Weight from Jpsi muons
    const std::vector<int> selected_Jpsi_muons = m_data->Jpsi_muons->at(m_data->Jpsi_selected);
    for (int muon_i = 0; muon_i < selected_Jpsi_muons.size(); muon_i++) {
        event_weight *= m_data->MU_SF_ID_LowPt->at(muon_i);
    }

    switch (lepton_type) {
    case electron:
        event_weight *= m_data->EL_SF_Trigger_TightLH->at(lepton_truth_i)
                        * m_data->EL_SF_Reco->at(lepton_truth_i)
                        * m_data->EL_SF_ID_TightLH->at(lepton_truth_i)
                        * m_data->EL_SF_Iso_Gradient->at(lepton_truth_i);
        break;
    case muon:
        event_weight *= m_data->MU_SF_Trigger_Medium->at(lepton_truth_i)
                        * m_data->MU_SF_TTVA->at(lepton_truth_i)
                        * m_data->MU_SF_ID_Medium->at(lepton_truth_i)
                        * m_data->MU_SF_Isol_Gradient->at(lepton_truth_i);
        break;
    }

    if (event_weight == 0.0) throw std::runtime_error("Event Weight is 0");

    return event_weight;
}

EventType EventLoop::getEventType()
{
    return getEventType(m_data->mcChannelNumber);
}

LeptonType EventLoop::getLeptonType()
{
    if (m_data->electron_selected >= 0 && m_data->muon_selected >= 0)
        throw std::runtime_error("Both electron and muon selected");
    if (m_data->electron_selected >= 0) {

        switch (m_data->electron_truth_pdgId->at(m_data->electron_selected)) {
        case -11:
            return LeptonType::electron;
        case 11:
            return LeptonType::electron;
        default:
            throw std::runtime_error(std::string("Error in pdgID of electron pdgID=") + std::to_string(m_data->electron_truth_pdgId->at(m_data->electron_selected)));
        }
    } else if (m_data->muon_selected >= 0) {
        switch (m_data->muon_truth_pdgId->at(m_data->muon_selected)) {
        case -13:
            return LeptonType::muon;
        case 13:
            return LeptonType::muon;
        default:
            throw std::runtime_error(std::string("Error in pdgID of muon pdgID=") + std::to_string(m_data->muon_truth_pdgId->at(m_data->muon_selected)));
        }
    } else {
        throw std::runtime_error("No lepton selected in event");
    }
}

EventType EventLoop::getEventType(const int DSID)
{
    EventType type;
    if (DSID%2)
        return type = top_event;
    else
        return type = anti_top_event;
}

MatchingStatus EventLoop::isCorrectlyMatched()
{
    int lepton_parent = 0, Jpsi_parent = 0;
    int grand_parent_pdgid = 0;

    if (m_data->electron_selected >= 0 && m_data->muon_selected >= 0)
        throw std::runtime_error("Both electron and muon selected");

// Check if selected particle is the correct lepton.
    if (m_data->electron_selected >= 0) {
        switch (m_data->electron_truth_pdgId->at(m_data->electron_selected)) {
        case -11:
            lepton_parent = 1;
            break;
        case 11:
            lepton_parent = -1;
            break;
        default:
            // std::cerr << std::string("Error in pdgID of electron pdgID=") + std::to_string(m_data->electron_truth_pdgId->at(m_data->electron_selected)) << std::endl;
            return MatchingStatus::issue_selected_lepton;
        }

        grand_parent_pdgid = m_data->electron_truthGrandParent_pdgId->at(m_data->electron_selected);

    } else if (m_data->muon_selected >= 0) {
        switch (m_data->muon_truth_pdgId->at(m_data->muon_selected)) {
        case -13:
            lepton_parent = 1;
            break;
        case 13:
            lepton_parent = -1;
            break;
        default:
            // std::cerr << std::string("Error in pdgID of muon pdgID=") + std::to_string(m_data->muon_truth_pdgId->at(m_data->muon_selected)) << std::endl;
            return MatchingStatus::issue_selected_lepton;
        }

        grand_parent_pdgid = m_data->muon_truthGrandParent_pdgId->at(m_data->muon_selected);
    } else {
        throw std::runtime_error("No lepton selected in event");
    }

    if (std::abs(grand_parent_pdgid) != 6) {
        return MatchingStatus::issue_wrong_parent_pdgid;
    }

    float Jpsi_barcode = m_data->Jpsi_truthMatch_barcode->at(m_data->Jpsi_selected);
    for (int i = 0; i < m_data->truth_barcode->size(); i++) {
        if (Jpsi_barcode == m_data->truth_barcode->at(i)) {
            if (m_data->truth_topParent->at(i) == m_data->truth_antiTopParent->at(i)) {
                // std::cerr << "Data has both top and antitop in JPsi history" << std::endl;
                return MatchingStatus::issue_jpsi_parent_info;
            }

            if (m_data->truth_topParent->at(i) == 1)
                Jpsi_parent = 1;
            else if (m_data->truth_antiTopParent->at(i) == 1)
                Jpsi_parent = -1;
            break;
        }
    }

    if (lepton_parent == 0 || Jpsi_parent == 0) {
        std::cerr << "Issue determing top parent lepton=" << lepton_parent << " Jpsi=" << Jpsi_parent << std::endl;
        return MatchingStatus::issue;
    }

    if (lepton_parent == Jpsi_parent)
        return MatchingStatus::correct;
    else
        return MatchingStatus::incorrect;
}


MatchingStatus EventLoop::isCorrectlyMatchedProject()
{
    // PDGID
    // e^- = 11 e^+ = -11
    // mu^- = 13 mu^+ = -13

    // TODO: figure out why m_data->variable[int] doesn't work
    // but m_data->variable->at(int) does

    // TODO: Use Parent/GrandParent info to do selection

    EventType type = getEventType();

    int lepton_parent = 0, Jpsi_parent = 0;

    if (m_data->electron_selected >= 0 && m_data->muon_selected >= 0)
        throw std::runtime_error("Both electron and muon selected");

    if (m_data->electron_selected >= 0) {
        switch (m_data->electron_truth_pdgId->at(m_data->electron_selected)) {
        case -11:
            lepton_parent = 1;
            break;
        case 11:
            lepton_parent = -1;
            break;
        default:
            std::cerr << std::string("Error in pdgID of electron pdgID=") + std::to_string(m_data->electron_truth_pdgId->at(m_data->electron_selected)) << std::endl;
            return MatchingStatus::issue;
        }
    } else if (m_data->muon_selected >= 0) {
        switch (m_data->muon_truth_pdgId->at(m_data->muon_selected)) {
        case -13:
            lepton_parent = 1;
            break;
        case 13:
            lepton_parent = -1;
            break;
        default:
            std::cerr << std::string("Error in pdgID of muon pdgID=") + std::to_string(m_data->muon_truth_pdgId->at(m_data->muon_selected)) << std::endl;
            return MatchingStatus::issue;
        }
    } else {
        throw std::runtime_error("No lepton selected in event");
    }

    float Jpsi_barcode = m_data->Jpsi_truthMatch_barcode->at(m_data->Jpsi_selected);
    for (int i = 0; i < m_data->truth_barcode->size(); i++) {
        if (Jpsi_barcode == m_data->truth_barcode->at(i)) {
            if (m_data->truth_topParent->at(i) == m_data->truth_antiTopParent->at(i)) {
                // std::cerr << "Data has both top and antitop in JPsi history" << std::endl;
                return MatchingStatus::issue;
            }

            if (m_data->truth_topParent->at(i) == 1)
                Jpsi_parent = 1;
            else if (m_data->truth_antiTopParent->at(i) == 1)
                Jpsi_parent = -1;
            break;
        }
    }

    if (lepton_parent == 0 || Jpsi_parent == 0) {
        std::cerr << "Issue determing top parent lepton=" << lepton_parent << " Jpsi=" << Jpsi_parent << std::endl;
        return MatchingStatus::issue;
    }

    if (lepton_parent == Jpsi_parent && lepton_parent != type) {
        std::string error = std::string("Lepton and Jpsi match but not with DSID type:") + std::to_string(type) + std::string(" parent:") + std::to_string(lepton_parent);
        return MatchingStatus::issue;
    }

    if (lepton_parent == Jpsi_parent)
        return MatchingStatus::correct;
    else
        return MatchingStatus::incorrect;
}
