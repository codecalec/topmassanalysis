from ROOT import gSystem
from ROOT.ROOT import EnableImplicitMT

EnableImplicitMT(4)

gSystem.Load("incorrectlepton.so")

from ROOT import EventLoop
from ROOT import vector as std_vector
from ROOT import TFile, TCanvas

from pathlib import Path


def generate_incorrect_lepton_hists():
    year_list = [2015, 2016, 2017, 2018]
    dsid_list = range(411143, 411161)  # top and antitop with 169-176MeV

    file_list = []
    for year in year_list:
        for dsid in dsid_list:
            file_list.append(
                "/atlas/aveltman/DATAMC/TopMass/topParentNtuples/"
                + str(year)
                + "/"
                + str(dsid)
                + "*"
            )

    # EventLoop Construct takes std::vector
    cpp_file_list = std_vector("string")()
    for f in file_list:
        cpp_file_list.push_back(f)

    event_loop = EventLoop("nominal", cpp_file_list)
    event_loop.execute()


if __name__ == "__main__":
    if not Path("./IncorrectLeptonResults.root").is_file():
        generate_incorrect_lepton_hists()

    f = TFile("IncorrectLeptonResults.root")
    hist_pt = f.Get("hist_pt")
    hist_eta = f.Get("hist_eta")
    hist_phi = f.Get("hist_phi")

    c1 = TCanvas("c1", "c1", 600, 400)
    for h in [hist_pt, hist_eta, hist_phi]:
        h.Draw()
        c1.SaveAs(h.GetName() + ".png")
        c1.Clear()
