#ifndef EVENTLOOP_H
#define EVENTLOOP_H

#include <vector>
#include <string>

#include "TString.h"
#include "TChain.h"
#include "TMath.h"

#include "Data.h"

// Bounds for histograms
const float LEPTON_PT_MIN = 0.0;
const float LEPTON_PT_MAX = 180000.0;
const float LEPTON_PHI_MIN = 0.0;
const double LEPTON_PHI_MAX = TMath::Pi();
const float LEPTON_ETA_MIN = 0.0;
const float LEPTON_ETA_MAX = 4.0;
const int HIST_NBINS = 60;

enum EventType {
    top_event = 1,
    anti_top_event = -1,
};

enum LeptonType {
    electron = 11,
    muon = 13,
};

enum MatchingStatus {
    correct,
    incorrect,
    issue,    
    issue_selected_lepton,
    issue_wrong_parent_pdgid,
    issue_jpsi_parent_info,
};

class EventLoop
{
public:
    EventLoop(std::string treeName, std::vector<std::string> inputFiles);

    void execute();

    TString GetTreeName()
    {
        return m_treeName;
    }

protected:
    TString m_treeName;
    TChain* m_chain = 0;
    Data* m_data = 0;
    float getEventWeight(int lepton_truth_i, LeptonType lepton_type);
    EventType getEventType();
    EventType getEventType(int DSID);
    LeptonType getLeptonType();
    MatchingStatus isCorrectlyMatched();
    MatchingStatus isCorrectlyMatchedProject();
};

#endif
