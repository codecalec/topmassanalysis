import ROOT
import os, sys, errno
from glob import glob
from atlasplots import atlas_style as astyle
from atlasplots import utils as autils
from ROOT import *

astyle.SetAtlasStyle()
ROOT.gROOT.SetBatch(ROOT.kTRUE)

### Define some useful functions
def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


# Take input list and load the tree that has the relevant information,
# checks the number of entries and returns the tree
def getTree(dsidList):
    fileList = []
    for dsid in dsidList:
        fileList.append("/atlas/DATAMC/TopMass/topMassNtuples/" + dsid + "/*")
    tree = ROOT.TChain("nominal")
    for fileName in fileList:
        tree.Add(fileName)
    print tree.GetEntries(), "entries in dsids", dsidList
    return tree


# Take the event selection output and matches the lepton with the J/Psi
# that pass the selection criteria
def m3l(event):
    Ji = event.Jpsi_selected
    Jpt, Jeta, Jphi, Jm = (
        event.Jpsi_pt[Ji],
        event.Jpsi_eta[Ji],
        event.Jpsi_phi[Ji],
        event.Jpsi_m[Ji],
    )
    J4V = ROOT.TLorentzVector()
    J4V.SetPtEtaPhiM(Jpt, Jeta, Jphi, Jm)

    Li, Lpt, Leta, Lphi, Le = -1, 0.0, 0.0, 0.0, 0.0
    if event.electron_selected >= 0:
        Li = event.electron_selected
        Lpt, Leta, Lphi, Le = (
            event.electron_pt[Li],
            event.electron_eta[Li],
            event.electron_phi[Li],
            event.electron_E[Li],
        )
    elif event.muon_selected >= 0:
        Li = event.muon_selected
        Lpt, Leta, Lphi, Le = (
            event.muon_pt[Li],
            event.muon_eta[Li],
            event.muon_phi[Li],
            event.muon_E[Li],
        )
    else:
        print ("bad!")
    L4V = ROOT.TLorentzVector()
    L4V.SetPtEtaPhiE(Lpt, Leta, Lphi, Le)
    return (J4V + L4V).M() * 0.001


# Take into account event weights and detector effects
def evtW(event):
    eventWeight = 1.0
    eventWeight *= (
        event.mcWeightOrg
        * event.jvtEventWeight
        * event.pileupEventWeight
        * event.MV2c10_77_EventWeight
        / event.totalEventsWeighted
    )

    # Muons from Jpsi
    muonsFromJpsi = []
    for ji, (pt, decay_muons) in enumerate(zip(event.Jpsi_pt, event.Jpsi_muons)):
        if ji == event.Jpsi_selected:
            for d in decay_muons:
                muonsFromJpsi.append(d)
    eventWeight *= (
        event.MU_SF_ID_LowPt[muonsFromJpsi[0]] * event.MU_SF_ID_LowPt[muonsFromJpsi[1]]
    )

    Li, Lpt, Leta, Lphi, Le = -1, 0.0, 0.0, 0.0, 0.0
    if event.electron_selected >= 0:
        Li = event.electron_selected
        eventWeight *= (
            event.EL_SF_Trigger_TightLH[Li]
            * event.EL_SF_Reco[Li]
            * event.EL_SF_ID_TightLH[Li]
            * event.EL_SF_Iso_Gradient[Li]
        )
    elif event.muon_selected >= 0:
        Li = event.muon_selected
        eventWeight *= (
            event.MU_SF_Trigger_Medium[Li]
            * event.MU_SF_ID_Medium[Li]
            * event.MU_SF_TTVA[Li]
            * event.MU_SF_Isol_Gradient[Li]
        )
    else:
        print ("bad!")
    return eventWeight


# Create histograms when plotting
def binHist(dataset, pdf, smooth, nBin, integral=-1):
    h_data = dataset.binnedClone("h_data", "h_data").createHistogram("m", nBin)
    h_fit = pdf.createHistogram("m", nBin * smooth)
    if integral < 0:
        dataInt = 1
        fitInt = h_data.Integral() / h_fit.Integral() * smooth
    else:
        dataInt = integral / h_data.Integral()
        fitInt = integral / h_fit.Integral() * smooth

    h_data.Scale(dataInt)
    h_fit.Scale(fitInt)
    return h_data, h_fit, h_data.Integral()


### Store plots in a folder
mkdir_p("./plots")

### Import the Monte Carlo datasets, and the ATLAS data. Define the TTrees.
tree169 = getTree(["mtop169"])
tree171 = getTree(["mtop171"])
tree172 = getTree(["mtop172"])
tree172p25 = getTree(["mtop172p25"])
tree172p5 = getTree(["mtop172p5"])
tree172p75 = getTree(["mtop172p75"])
tree173 = getTree(["mtop173"])
tree174 = getTree(["mtop174"])
tree176 = getTree(["mtop176"])
treeData = getTree(["data"])

### Fill histograms with m(lepton,J/Psi)
c = ROOT.TCanvas("c", "c", 800, 700)
h169 = ROOT.TH1F("h169", "h169", 60, 0, 300)
h171 = ROOT.TH1F("h171", "h171", 60, 0, 300)
h172 = ROOT.TH1F("h172", "h172", 60, 0, 300)
h172p25 = ROOT.TH1F("h172p25", "h172p25", 60, 0, 300)
h172p5 = ROOT.TH1F("h172p5", "h172p5", 60, 0, 300)
h172p75 = ROOT.TH1F("h172p75", "h172p75", 60, 0, 300)
h173 = ROOT.TH1F("h173", "h173", 60, 0, 300)
h174 = ROOT.TH1F("h174", "h174", 60, 0, 300)
h176 = ROOT.TH1F("h176", "h176", 60, 0, 300)
# Monte Carlo needs to take into account event weights and detector effects
for event in tree169:
    h169.Fill(m3l(event), evtW(event))
for event in tree171:
    h171.Fill(m3l(event), evtW(event))
for event in tree172:
    h172.Fill(m3l(event), evtW(event))
for event in tree172p25:
    h172p25.Fill(m3l(event), evtW(event))
for event in tree172p5:
    h172p5.Fill(m3l(event), evtW(event))
for event in tree172p75:
    h172p75.Fill(m3l(event), evtW(event))
for event in tree173:
    h173.Fill(m3l(event), evtW(event))
for event in tree174:
    h174.Fill(m3l(event), evtW(event))
for event in tree176:
    h176.Fill(m3l(event), evtW(event))

"""
# Scales the Monte Carlo to data for shape comparison
h169.Scale(hData.Integral()/h169.Integral())
h171.Scale(hData.Integral()/h171.Integral())
h172.Scale(hData.Integral()/h172.Integral())
h172p25.Scale(hData.Integral()/h172p25.Integral())
h172p5.Scale(hData.Integral()/h172p5.Integral())
h172p75.Scale(hData.Integral()/h172p75.Integral())
h173.Scale(hData.Integral()/h173.Integral())
h174.Scale(hData.Integral()/h174.Integral())
h176.Scale(hData.Integral()/h176.Integral())
"""

### Draw the histograms to check for reasonable distributions.
h169.GetXaxis().SetTitle("m_{l+J/#psi} [GeV]")
h169.GetYaxis().SetTitleOffset(1.5)
h169.GetYaxis().SetTitle("Events/L#sigma")
h169.SetMaximum(h169.GetMaximum() * 1.3)
# Add aestethics because aesthetics
h169.SetLineColor(2)
h169.SetMarkerColor(2)
h169.SetMarkerStyle(20)
h169.SetMarkerSize(0.5)
h171.SetLineColor(3)
h171.SetMarkerColor(3)
h171.SetMarkerStyle(20)
h171.SetMarkerSize(0.5)
h172.SetLineColor(4)
h172.SetMarkerColor(4)
h172.SetMarkerStyle(20)
h172.SetMarkerSize(0.5)
h172p25.SetLineColor(5)
h172p25.SetMarkerColor(5)
h172p25.SetMarkerStyle(20)
h172p25.SetMarkerSize(0.5)
h172p5.SetLineColor(6)
h172p5.SetMarkerColor(6)
h172p5.SetMarkerStyle(20)
h172p5.SetMarkerSize(0.5)
h172p75.SetLineColor(7)
h172p75.SetMarkerColor(7)
h172p75.SetMarkerStyle(20)
h172p75.SetMarkerSize(0.5)
h173.SetLineColor(8)
h173.SetMarkerColor(8)
h173.SetMarkerStyle(20)
h173.SetMarkerSize(0.5)
h174.SetLineColor(9)
h174.SetMarkerColor(9)
h174.SetMarkerStyle(20)
h174.SetMarkerSize(0.5)
h176.SetLineColor(46)
h176.SetMarkerColor(46)
h176.SetMarkerStyle(20)
h176.SetMarkerSize(0.5)

h169.Draw("E P same")
h171.Draw("E P same")
h172.Draw("E P same")
h172p25.Draw("E P same")
h172p5.Draw("E P same")
h172p75.Draw("E P same")
h173.Draw("E P same")
h174.Draw("E P same")
h176.Draw("E P same")

astyle.ATLASLabel(0.4, 0.87, "work in progress")
autils.DrawText(0.5, 0.82, "#sqrt{s} = 13 TeV, 139 fb^{-1}", size=0.03)
legend = ROOT.TLegend(0.6, 0.45, 0.9, 0.8)
legend.AddEntry(h169, "m_{t} = 169 GeV MC", "p l")
legend.AddEntry(h171, "m_{t} = 171 GeV MC", "p l")
legend.AddEntry(h172, "m_{t} = 172 GeV MC", "p l")
legend.AddEntry(h172p25, "m_{t} = 172.25 GeV MC", "p l")
legend.AddEntry(h172p5, "m_{t} = 172.5 GeV MC", "p l")
legend.AddEntry(h172p75, "m_{t} = 172.75 GeV MC", "p l")
legend.AddEntry(h173, "m_{t} = 173 GeV MC", "p l")
legend.AddEntry(h174, "m_{t} = 174 GeV MC", "p l")
legend.AddEntry(h176, "m_{t} = 176 GeV MC", "p l")
legend.SetBorderSize(0)
legend.SetFillColor(0)
legend.SetTextSize(0.03)
legend.Draw()
c.Update()
c.SaveAs("./plots/data_vs_mc.png")

### Setup a workspace
mlo, mhi = 0, 300
w = ROOT.RooWorkspace()
m = w.factory("m[" + str(mlo) + "," + str(mhi) + "]")
# Fill the datasets
ds169 = ROOT.RooDataSet("ds169", "ds169", ROOT.RooArgSet(m))
for event in tree169:
    p = m3l(event)
    if mlo < p < mhi:
        m.setVal(p)
        ds169.add(ROOT.RooArgSet(m), evtW(event))
ds171 = ROOT.RooDataSet("ds171", "ds171", ROOT.RooArgSet(m))
for event in tree171:
    p = m3l(event)
    if mlo < p < mhi:
        m.setVal(p)
        ds171.add(ROOT.RooArgSet(m))
ds172 = ROOT.RooDataSet("ds172", "ds172", ROOT.RooArgSet(m))
for event in tree172:
    p = m3l(event)
    if mlo < p < mhi:
        m.setVal(p)
        ds172.add(ROOT.RooArgSet(m))
ds172p25 = ROOT.RooDataSet("ds172p25", "ds172p25", ROOT.RooArgSet(m))
for event in tree172p25:
    p = m3l(event)
    if mlo < p < mhi:
        m.setVal(p)
        ds172p25.add(ROOT.RooArgSet(m))
ds172p5 = ROOT.RooDataSet("ds172p5", "ds172p5", ROOT.RooArgSet(m))
for event in tree172p5:
    p = m3l(event)
    if mlo < p < mhi:
        m.setVal(p)
        ds172p5.add(ROOT.RooArgSet(m))
ds172p75 = ROOT.RooDataSet("ds172p75", "ds172p75", ROOT.RooArgSet(m))
for event in tree172p75:
    p = m3l(event)
    if mlo < p < mhi:
        m.setVal(p)
        ds172p75.add(ROOT.RooArgSet(m))
ds173 = ROOT.RooDataSet("ds173", "ds173", ROOT.RooArgSet(m))
for event in tree173:
    p = m3l(event)
    if mlo < p < mhi:
        m.setVal(p)
        ds173.add(ROOT.RooArgSet(m))
ds174 = ROOT.RooDataSet("ds174", "ds174", ROOT.RooArgSet(m))
for event in tree174:
    p = m3l(event)
    if mlo < p < mhi:
        m.setVal(p)
        ds174.add(ROOT.RooArgSet(m))
ds176 = ROOT.RooDataSet("ds176", "ds176", ROOT.RooArgSet(m))
for event in tree176:
    p = m3l(event)
    if mlo < p < mhi:
        m.setVal(p)
        ds176.add(ROOT.RooArgSet(m))
# dsData = ROOT.RooDataSet('dsData','dsData',ROOT.RooArgSet(m))
# for event in treeData:
#    p = m3l(event)
#    if mlo<p<mhi:
#        m.setVal(p)
#        dsData.add(ROOT.RooArgSet(m))

# Build the RooFit Model with the different mass points
# pdf is sum of a Gaussian and a Gamma function
massPoints = [169, 171, 172, 172.25, 172.75, 173, 174, 176]
pdfList = ROOT.RooArgList()
for mass in massPoints:
    w.factory(
        "Gamma::background{mass}(m,b_g{mass}[3.0, 1.0, 4.0],b_b{mass}[40.0, 20.0, 60.0],b_m{mass}[0.0])".format(
            mass=mass
        )
    )
    w.factory(
        "Gaussian::signal{mass}(m,s_m{mass}[70.0 ,60.0 ,80.0],s_s{mass}[20.0, 10.0 ,30.0])".format(
            mass=mass
        )
    )
    pdf = w.factory(
        "SUM::m3l{mass}(f{mass}[0.9, 0.1, 1.0]*signal{mass},background{mass})".format(
            mass=mass
        )
    )
    pdfList.add(pdf)

# fit pdf to each mass point and create histograms for plotting
fit169 = pdfList[0].fitTo(ds169, ROOT.RooFit.Save(), ROOT.RooFit.PrintLevel(3))
h_ds169, h_pdf169, integral169 = binHist(ds169, pdfList[0], 250, 25)
fit171 = pdfList[1].fitTo(ds171, ROOT.RooFit.Save(), ROOT.RooFit.PrintLevel(3))
h_ds171, h_pdf171, integral171 = binHist(ds171, pdfList[1], 250, 25)
fit172 = pdfList[2].fitTo(ds172, ROOT.RooFit.Save(), ROOT.RooFit.PrintLevel(3))
h_ds172, h_pdf172, integral172 = binHist(ds172, pdfList[2], 250, 25)
fit172p25 = pdfList[3].fitTo(ds172p25, ROOT.RooFit.Save(), ROOT.RooFit.PrintLevel(3))
h_ds172p25, h_pdf172p25, integral172p25 = binHist(ds172p25, pdfList[3], 250, 25)
# fit172p5 = pdfList[4].fitTo(ds172p5,ROOT.RooFit.Save(),ROOT.RooFit.PrintLevel(3))
# h_ds172p5,h_pdf172p5,integral172p5 = binHist(ds172p5,pdfList[4],250,25)
fit172p75 = pdfList[4].fitTo(ds172p75, ROOT.RooFit.Save(), ROOT.RooFit.PrintLevel(3))
h_ds172p75, h_pdf172p75, integral172p75 = binHist(ds172p75, pdfList[4], 250, 25)
fit173 = pdfList[5].fitTo(ds173, ROOT.RooFit.Save(), ROOT.RooFit.PrintLevel(3))
h_ds173, h_pdf173, integral173 = binHist(ds173, pdfList[5], 250, 25)
fit174 = pdfList[6].fitTo(ds174, ROOT.RooFit.Save(), ROOT.RooFit.PrintLevel(3))
h_ds174, h_pdf174, integral174 = binHist(ds174, pdfList[6], 250, 25)
fit176 = pdfList[7].fitTo(ds176, ROOT.RooFit.Save(), ROOT.RooFit.PrintLevel(3))
h_ds176, h_pdf176, integral176 = binHist(ds176, pdfList[7], 250, 25)

### Plot the individual mass points data and pdf fit distributions
# mtop = 169 GeV
c.Clear()
nframe = m.frame()
astyle.ATLASLabel(0.4, 0.87, "work in progress")
autils.DrawText(0.6, 0.82, "#sqrt{s} = 13 TeV", size=0.03)
h_mc169 = ROOT.RooDataHist(
    "mc169", "mc169", ROOT.RooArgList(m), ROOT.RooFit.Import(h_ds169)
)
h_mc169.plotOn(nframe, ROOT.RooFit.Name("mc169"))
pdfList[0].plotOn(nframe, ROOT.RooFit.Name("pdf169"))
pdfList[0].plotOn(
    nframe,
    ROOT.RooFit.Components("signal169"),
    ROOT.RooFit.Name("Gaus169"),
    ROOT.RooFit.LineStyle(kDashed),
    ROOT.RooFit.LineColor(kRed),
)
pdfList[0].plotOn(
    nframe,
    ROOT.RooFit.Components("background169"),
    ROOT.RooFit.Name("Gamma169"),
    ROOT.RooFit.LineStyle(kDashed),
    ROOT.RooFit.LineColor(kGreen),
)
nframe.GetYaxis().SetTitle("")
nframe.GetXaxis().SetTitle("m_{l+J/#psi} [GeV]")
nframe.Draw()
leg2 = TLegend(0.6, 0.7, 0.8, 0.9)
leg2.SetBorderSize(0)
leg2.SetTextSize(0.03)
leg2.AddEntry(nframe.findObject("mc169"), "m_{t} = 169 GeV MC", "P L")
leg2.AddEntry(nframe.findObject("pdf169"), "m_{t} = 169 GeV Fit", "L")
leg2.AddEntry(nframe.findObject("Gaus169"), "Signal Component", "L")
leg2.AddEntry(nframe.findObject("Gamma169"), "Background Component", "L")
leg2.Draw("same")
c.Update()
c.SaveAs("./plots/fit_results_with_components169.png")

# mtop = 171 GeV
c.Clear()
nframe = m.frame()
astyle.ATLASLabel(0.4, 0.87, "work in progress")
autils.DrawText(0.6, 0.82, "#sqrt{s} = 13 TeV", size=0.03)
h_mc171 = ROOT.RooDataHist(
    "mc171", "mc171", ROOT.RooArgList(m), ROOT.RooFit.Import(h_ds171)
)
h_mc171.plotOn(nframe, ROOT.RooFit.Name("mc171"))
pdfList[1].plotOn(nframe, ROOT.RooFit.Name("pdf171"))
pdfList[1].plotOn(
    nframe,
    ROOT.RooFit.Components("signal171"),
    ROOT.RooFit.Name("Gaus171"),
    ROOT.RooFit.LineColor(kRed),
    ROOT.RooFit.LineStyle(kDashed),
)
pdfList[1].plotOn(
    nframe,
    ROOT.RooFit.Components("background171"),
    ROOT.RooFit.Name("Gamma171"),
    ROOT.RooFit.LineColor(kGreen),
    ROOT.RooFit.LineStyle(kDashed),
)
nframe.GetYaxis().SetTitle("")
nframe.GetXaxis().SetTitle("m_{l+J/#psi} [GeV]")
nframe.Draw()
leg2 = TLegend(0.6, 0.7, 0.8, 0.9)
leg2.SetBorderSize(0)
leg2.SetTextSize(0.03)
leg2.AddEntry(nframe.findObject("mc171"), "m_{t} = 171 GeV MC", "P L")
leg2.AddEntry(nframe.findObject("pdf171"), "m_{t} = 171 GeV Fit", "L")
leg2.AddEntry(nframe.findObject("Gaus171"), "Signal Component", "L")
leg2.AddEntry(nframe.findObject("Gamma171"), "Background Component", "L")
leg2.Draw("same")
c.Update()
c.SaveAs("./plots/fit_results_with_components171.png")

# mtop = 172 GeV
c.Clear()
nframe = m.frame()
astyle.ATLASLabel(0.4, 0.87, "work in progress")
autils.DrawText(0.6, 0.82, "#sqrt{s} = 13 TeV", size=0.03)
h_mc172 = ROOT.RooDataHist(
    "mc172", "mc172", ROOT.RooArgList(m), ROOT.RooFit.Import(h_ds172)
)
h_mc172.plotOn(nframe, ROOT.RooFit.Name("mc172"))
pdfList[2].plotOn(nframe, ROOT.RooFit.Name("pdf172"))
pdfList[2].plotOn(
    nframe,
    ROOT.RooFit.Components("signal172"),
    ROOT.RooFit.Name("Gaus172"),
    ROOT.RooFit.LineColor(kRed),
    ROOT.RooFit.LineStyle(kDashed),
)
pdfList[2].plotOn(
    nframe,
    ROOT.RooFit.Components("background172"),
    ROOT.RooFit.Name("Gamma172"),
    ROOT.RooFit.LineColor(kGreen),
    ROOT.RooFit.LineStyle(kDashed),
)
nframe.GetYaxis().SetTitle("")
nframe.GetXaxis().SetTitle("m_{l+J/#psi} [GeV]")
nframe.Draw()
leg2 = TLegend(0.6, 0.7, 0.8, 0.9)
leg2.SetBorderSize(0)
leg2.SetTextSize(0.03)
leg2.AddEntry(nframe.findObject("mc172"), "m_{t} = 172 GeV MC", "P L")
leg2.AddEntry(nframe.findObject("pdf172"), "m_{t} = 172 GeV Fit", "L")
leg2.AddEntry(nframe.findObject("Gaus172"), "Signal Component", "L")
leg2.AddEntry(nframe.findObject("Gamma172"), "Background Component", "L")
leg2.Draw("same")
c.Update()
c.SaveAs("./plots/fit_results_with_components172.png")

# mtop = 172.25 GEV
c.Clear()
nframe = m.frame()
astyle.ATLASLabel(0.4, 0.87, "work in progress")
autils.DrawText(0.6, 0.82, "#sqrt{s} = 13 TeV", size=0.03)
h_mc17225 = ROOT.RooDataHist(
    "mc172.25", "mc172.25", ROOT.RooArgList(m), ROOT.RooFit.Import(h_ds172p25)
)
h_mc17225.plotOn(nframe, ROOT.RooFit.Name("mc172.25"))
pdfList[3].plotOn(nframe, ROOT.RooFit.Name("pdf172.25"))
pdfList[3].plotOn(
    nframe,
    ROOT.RooFit.Components("signal172.25"),
    ROOT.RooFit.Name("Gaus172.25"),
    ROOT.RooFit.LineColor(kRed),
    ROOT.RooFit.LineStyle(kDashed),
)
pdfList[3].plotOn(
    nframe,
    ROOT.RooFit.Components("background172.25"),
    ROOT.RooFit.Name("Gamma172.25"),
    ROOT.RooFit.LineColor(kGreen),
    ROOT.RooFit.LineStyle(kDashed),
)
nframe.GetYaxis().SetTitle("")
nframe.GetXaxis().SetTitle("m_{l+J/#psi} [GeV]")
nframe.Draw()
leg2 = TLegend(0.6, 0.7, 0.8, 0.9)
leg2.SetBorderSize(0)
leg2.SetTextSize(0.03)
leg2.AddEntry(nframe.findObject("mc172.25"), "m_{t} = 172.25 GeV MC", "P L")
leg2.AddEntry(nframe.findObject("pdf172.25"), "m_{t} = 172.25 GeV Fit", "L")
leg2.AddEntry(nframe.findObject("Gaus172.25"), "Signal Component", "L")
leg2.AddEntry(nframe.findObject("Gamma172.25"), "Background Component", "L")
leg2.Draw("same")
c.Update()
c.SaveAs("./plots/fit_results_with_components172p25.png")

"""
# mtop = 172.5 GeV
c.Clear()
nframe = m.frame()
astyle.ATLASLabel(0.4, 0.87, 'work in progress')
autils.DrawText(0.6, 0.82, '#sqrt{s} = 13 TeV', size=0.03)
h_mc172p5 = ROOT.RooDataHist('mc172.5','mc172.5',ROOT.RooArgList(m),ROOT.RooFit.Import(h_ds172p5))
h_mc172p5.plotOn(nframe,ROOT.RooFit.Name("mc172.5"))
pdfList[4].plotOn(nframe,ROOT.RooFit.Name("pdf172.5"))
pdfList[4].plotOn(nframe,ROOT.RooFit.Components("signal172.5"),ROOT.RooFit.Name("Gaus172.5"),ROOT.RooFit.LineColor(kRed),ROOT.RooFit.LineStyle(kDashed))
pdfList[4].plotOn(nframe,ROOT.RooFit.Components("background172.5"),ROOT.RooFit.Name("Gamma172.5"),ROOT.RooFit.LineColor(kGreen),ROOT.RooFit.LineStyle(kDashed))
nframe.GetYaxis().SetTitle("")
nframe.GetXaxis().SetTitle("m_{l+J/#psi} [GeV]")
nframe.Draw()
leg2 = TLegend(0.6,0.7,0.8,0.9)
leg2.SetBorderSize(0)
leg2.SetTextSize(0.03)
leg2.AddEntry(nframe.findObject("mc172.5"),"m_{t} = 172.5 GeV MC", "P L")
leg2.AddEntry(nframe.findObject("pdf172.5"),"m_{t} = 172.5 GeV Fit","L")
leg2.AddEntry(nframe.findObject("Gaus172.5"),"Signal Component","L")
leg2.AddEntry(nframe.findObject("Gamma172.5"),"Background Component","L")
leg2.Draw("same")
c.Update()
c.SaveAs('./plots/fit_results_with_components172p5.png')
"""

# mtop = 172.75 GeV
c.Clear()
nframe = m.frame()
astyle.ATLASLabel(0.4, 0.87, "work in progress")
autils.DrawText(0.6, 0.82, "#sqrt{s} = 13 TeV", size=0.03)
h_mc172p75 = ROOT.RooDataHist(
    "mc172.75", "mc172.75", ROOT.RooArgList(m), ROOT.RooFit.Import(h_ds172p75)
)
h_mc172p75.plotOn(nframe, ROOT.RooFit.Name("mc172.75"))
pdfList[4].plotOn(nframe, ROOT.RooFit.Name("pdf172.75"))
pdfList[4].plotOn(
    nframe,
    ROOT.RooFit.Components("signal172.75"),
    ROOT.RooFit.Name("Gaus172.75"),
    ROOT.RooFit.LineColor(kRed),
    ROOT.RooFit.LineStyle(kDashed),
)
pdfList[4].plotOn(
    nframe,
    ROOT.RooFit.Components("background172.75"),
    ROOT.RooFit.Name("Gamma172.75"),
    ROOT.RooFit.LineColor(kGreen),
    ROOT.RooFit.LineStyle(kDashed),
)
nframe.GetYaxis().SetTitle("")
nframe.GetXaxis().SetTitle("m_{l+J/#psi} [GeV]")
nframe.Draw()
leg2 = TLegend(0.6, 0.7, 0.8, 0.9)
leg2.SetBorderSize(0)
leg2.SetTextSize(0.03)
leg2.AddEntry(nframe.findObject("mc172.75"), "m_{t} = 172.75 GeV MC", "P L")
leg2.AddEntry(nframe.findObject("pdf172.75"), "m_{t} = 172.75 GeV Fit", "L")
leg2.AddEntry(nframe.findObject("Gaus172.75"), "Signal Component", "L")
leg2.AddEntry(nframe.findObject("Gamma172.75"), "Background Component", "L")
leg2.Draw("same")
c.Update()
c.SaveAs("./plots/fit_results_with_components172p75.png")

# mtop = 173 GeV
c.Clear()
nframe = m.frame()
astyle.ATLASLabel(0.4, 0.87, "work in progress")
autils.DrawText(0.6, 0.82, "#sqrt{s} = 13 TeV", size=0.03)
h_mc173 = ROOT.RooDataHist(
    "mc173", "mc173", ROOT.RooArgList(m), ROOT.RooFit.Import(h_ds173)
)
h_mc173.plotOn(nframe, ROOT.RooFit.Name("mc173"))
pdfList[5].plotOn(nframe, ROOT.RooFit.Name("pdf173"))
pdfList[5].plotOn(
    nframe,
    ROOT.RooFit.Components("signal173"),
    ROOT.RooFit.Name("Gaus173"),
    ROOT.RooFit.LineColor(kRed),
    ROOT.RooFit.LineStyle(kDashed),
)
pdfList[5].plotOn(
    nframe,
    ROOT.RooFit.Components("background173"),
    ROOT.RooFit.Name("Gamma173"),
    ROOT.RooFit.LineColor(kGreen),
    ROOT.RooFit.LineStyle(kDashed),
)
nframe.GetYaxis().SetTitle("")
nframe.GetXaxis().SetTitle("m_{l+J/#psi} [GeV]")
nframe.Draw()
leg2 = TLegend(0.6, 0.7, 0.8, 0.9)
leg2.SetBorderSize(0)
leg2.SetTextSize(0.03)
leg2.AddEntry(nframe.findObject("mc173"), "m_{t} = 173 GeV MC", "P L")
leg2.AddEntry(nframe.findObject("pdf173"), "m_{t} = 173 GeV Fit", "L")
leg2.AddEntry(nframe.findObject("Gaus173"), "Signal Component", "L")
leg2.AddEntry(nframe.findObject("Gamma173"), "Background Component", "L")
leg2.Draw("same")
c.Update()
c.SaveAs("./plots/fit_results_with_components173.png")

# mtop = 174 GeV
c.Clear()
nframe = m.frame()
astyle.ATLASLabel(0.4, 0.87, "work in progress")
autils.DrawText(0.6, 0.82, "#sqrt{s} = 13 TeV", size=0.03)
h_mc174 = ROOT.RooDataHist(
    "mc174", "mc174", ROOT.RooArgList(m), ROOT.RooFit.Import(h_ds174)
)
h_mc174.plotOn(nframe, ROOT.RooFit.Name("mc174"))
pdfList[6].plotOn(nframe, ROOT.RooFit.Name("pdf174"))
pdfList[6].plotOn(
    nframe,
    ROOT.RooFit.Components("signal174"),
    ROOT.RooFit.Name("Gaus174"),
    ROOT.RooFit.LineColor(kRed),
    ROOT.RooFit.LineStyle(kDashed),
)
pdfList[6].plotOn(
    nframe,
    ROOT.RooFit.Components("background174"),
    ROOT.RooFit.Name("Gamma174"),
    ROOT.RooFit.LineColor(kGreen),
    ROOT.RooFit.LineStyle(kDashed),
)
nframe.GetYaxis().SetTitle("")
nframe.GetXaxis().SetTitle("m_{l+J/#psi} [GeV]")
nframe.Draw()
leg2 = TLegend(0.6, 0.7, 0.8, 0.9)
leg2.SetBorderSize(0)
leg2.SetTextSize(0.03)
leg2.AddEntry(nframe.findObject("mc174"), "m_{t} = 174 GeV MC", "P L")
leg2.AddEntry(nframe.findObject("pdf174"), "m_{t} = 174 GeV Fit", "L")
leg2.AddEntry(nframe.findObject("Gaus174"), "Signal Component", "L")
leg2.AddEntry(nframe.findObject("Gamma174"), "Background Component", "L")
leg2.Draw("same")
c.Update()
c.SaveAs("./plots/fit_results_with_components174.png")

# mtop = 176 GeV
c.Clear()
nframe = m.frame()
astyle.ATLASLabel(0.4, 0.87, "work in progress")
autils.DrawText(0.6, 0.82, "#sqrt{s} = 13 TeV", size=0.03)
h_mc176 = ROOT.RooDataHist(
    "mc176", "mc176", ROOT.RooArgList(m), ROOT.RooFit.Import(h_ds176)
)
h_mc176.plotOn(nframe, ROOT.RooFit.Name("mc176"))
pdfList[7].plotOn(nframe, ROOT.RooFit.Name("pdf176"))
pdfList[7].plotOn(
    nframe,
    ROOT.RooFit.Components("signal176"),
    ROOT.RooFit.Name("Gaus176"),
    ROOT.RooFit.LineColor(kRed),
    ROOT.RooFit.LineStyle(kDashed),
)
pdfList[7].plotOn(
    nframe,
    ROOT.RooFit.Components("background176"),
    ROOT.RooFit.Name("Gamma176"),
    ROOT.RooFit.LineColor(kGreen),
    ROOT.RooFit.LineStyle(kDashed),
)
nframe.GetYaxis().SetTitle("")
nframe.GetXaxis().SetTitle("m_{l+J/#psi} [GeV]")
nframe.Draw()
leg2 = TLegend(0.6, 0.7, 0.8, 0.9)
leg2.SetBorderSize(0)
leg2.SetTextSize(0.03)
leg2.AddEntry(nframe.findObject("mc176"), "m_{t} = 176 GeV MC", "P L")
leg2.AddEntry(nframe.findObject("pdf176"), "m_{t} = 176 GeV Fit", "L")
leg2.AddEntry(nframe.findObject("Gaus176"), "Signal Component", "L")
leg2.AddEntry(nframe.findObject("Gamma176"), "Background Component", "L")
leg2.Draw("same")
c.Update()
c.SaveAs("./plots/fit_results_with_components176.png")

### Check that the fits work
# First, print
fit169.Print()
fit171.Print()
fit172.Print()
fit172p25.Print()
# fit172p5.Print()
fit172p75.Print()
fit173.Print()
fit174.Print()
fit176.Print()

# Then, plot
h_ds169.GetYaxis().SetTitle("")
h_ds169.GetXaxis().SetTitle("m_{l+J/#psi} [GeV]")
h_ds169.SetMaximum(h_ds169.GetMaximum() * 1.3)
h_ds169.SetMarkerColor(2)
h_ds169.SetLineColor(2)
h_pdf169.SetLineColor(2)
h_ds171.SetMarkerColor(3)
h_ds171.SetLineColor(3)
h_pdf171.SetLineColor(3)
h_ds172.SetMarkerColor(4)
h_ds172.SetLineColor(4)
h_pdf172.SetLineColor(4)
h_ds172p25.SetMarkerColor(5)
h_ds172p25.SetLineColor(5)
h_pdf172p25.SetLineColor(5)
# h_ds172p5.SetMarkerColor(6)
# h_ds172p5.SetLineColor(6)
# h_pdf172p5.SetLineColor(6)
h_ds172p75.SetMarkerColor(7)
h_ds172p75.SetLineColor(7)
h_pdf172p75.SetLineColor(7)
h_ds173.SetMarkerColor(8)
h_ds173.SetLineColor(8)
h_pdf173.SetLineColor(8)
h_ds174.SetMarkerColor(9)
h_ds174.SetLineColor(9)
h_pdf174.SetLineColor(9)
h_ds176.SetMarkerColor(46)
h_ds176.SetLineColor(46)
h_pdf176.SetLineColor(46)

h_ds169.Draw("p")
h_pdf169.Draw("hist same")
h_ds171.Draw("p same")
h_pdf171.Draw("hist same")
h_ds172.Draw("p same")
h_pdf172.Draw("hist same")
h_ds172p25.Draw("p same")
h_pdf172p25.Draw("hist same")
# h_ds172p5.Draw('p same')
# h_pdf172p5.Draw('hist same')
h_ds172p75.Draw("p same")
h_pdf172p75.Draw("hist same")
h_ds173.Draw("p same")
h_pdf173.Draw("hist same")
h_ds174.Draw("p same")
h_pdf174.Draw("hist same")
h_ds176.Draw("p same")
h_pdf176.Draw("hist same")

astyle.ATLASLabel(0.4, 0.87, "work in progress")
autils.DrawText(0.5, 0.82, "#sqrt{s} = 13 TeV", size=0.03)
legend = ROOT.TLegend(0.6, 0.3, 0.9, 0.8)
legend.AddEntry(h_ds169, "m_{t} = 169 GeV MC", "p")
legend.AddEntry(h_pdf169, "m_{t} = 169 GeV Fit", "l")
legend.AddEntry(h_ds171, "m_{t} = 171 GeV MC", "p")
legend.AddEntry(h_pdf171, "m_{t} = 171 GeV Fit", "l")
legend.AddEntry(h_ds172, "m_{t} = 172 GeV MC", "p")
legend.AddEntry(h_pdf172, "m_{t} = 172 GeV Fit", "l")
legend.AddEntry(h_ds172p25, "m_{t} = 172.25 GeV MC", "p")
legend.AddEntry(h_pdf172p25, "m_{t} = 172.25 GeV Fit", "l")
# legend.AddEntry(h_ds172p5,'m_{t} = 172.5 GeV MC','p')
# legend.AddEntry(h_pdf172p5,'m_{t} = 172.5 GeV Fit','l')
legend.AddEntry(h_ds172p75, "m_{t} = 172.75 GeV MC", "p")
legend.AddEntry(h_pdf172p75, "m_{t} = 172.75 GeV Fit", "l")
legend.AddEntry(h_ds173, "m_{t} = 173 GeV MC", "p")
legend.AddEntry(h_pdf173, "m_{t} = 173 GeV Fit", "l")
legend.AddEntry(h_ds174, "m_{t} = 174 GeV MC", "p")
legend.AddEntry(h_pdf174, "m_{t} = 174 GeV Fit", "l")
legend.AddEntry(h_ds176, "m_{t} = 176 GeV MC", "p")
legend.AddEntry(h_pdf176, "m_{t} = 176 GeV Fit", "l")
legend.SetBorderSize(0)
legend.SetFillColor(0)
legend.SetTextSize(0.03)
legend.Draw()
c.Update()
c.SaveAs("./plots/fit_results.png")

### Create the morph object
mu = w.factory("mu[169,176]")
paramVec = ROOT.TVectorD(8)
paramVec[0] = 169
paramVec[1] = 171
paramVec[2] = 172
paramVec[3] = 172.25
# paramVec[4]=172.5
paramVec[4] = 172.75
paramVec[5] = 173
paramVec[6] = 174
paramVec[7] = 176

morph = ROOT.RooMomentMorph(
    "RMM", "RMM", mu, ROOT.RooArgList(m), pdfList, paramVec, ROOT.RooMomentMorph.Linear
)
getattr(w, "import")(morph)

"""
### Plot some versions of the RMM object for different parameter values
# Test the morphing for various other top mass values not listed in the input samples
# Comment out code when morhping to data
c.Clear()
frame = m.frame()

mu.setVal(169)
morph.plotOn(frame,ROOT.RooFit.LineColor(2))

mu.setVal(170)
morph.plotOn(frame,ROOT.RooFit.LineColor(3))

mu.setVal(171)
morph.plotOn(frame,ROOT.RooFit.LineColor(4))

mu.setVal(172)
morph.plotOn(frame,ROOT.RooFit.LineColor(5))

mu.setVal(172.25)
morph.plotOn(frame,ROOT.RooFit.LineColor(6))

mu.setVal(172.5)
morph.plotOn(frame,ROOT.RooFit.LineColor(7))

mu.setVal(172.75)
morph.plotOn(frame,ROOT.RooFit.LineColor(8))

mu.setVal(173)
morph.plotOn(frame,ROOT.RooFit.LineColor(9))

mu.setVal(174)
morph.plotOn(frame,ROOT.RooFit.LineColor(46))

mu.setVal(175)
morph.plotOn(frame,ROOT.RooFit.LineColor(48))

mu.setVal(176)
morph.plotOn(frame,ROOT.RooFit.LineColor(42))

frame.GetXaxis().SetTitle('m_{l+J/#psi} [GeV]')
frame.GetYaxis().SetTitle('')
frame.GetYaxis().SetRangeUser(0,0.05)
frame.Draw()

astyle.ATLASLabel(0.4, 0.87, 'work in progress')
autils.DrawText(0.6, 0.82, '#sqrt{s} = 13 TeV', size=0.03)
legend = ROOT.TLegend(0.5,0.4,0.8,0.8)
h_clone1 = h_pdf169.Clone("h_clone1")
h_clone1.SetLineColor(2)
legend.AddEntry(h_clone1,'m_{t} = 169 GeV MC Fit','l')
h_clone11 = h_pdf169.Clone("h_clone11")
h_clone11.SetLineColor(3)
legend.AddEntry(h_clone11,'m_{t} = 170 GeV MC Interpolated','l')
h_clone2 = h_pdf169.Clone("h_clone2")
h_clone2.SetLineColor(4)
legend.AddEntry(h_clone2,'m_{t} = 171 GeV MC Fit','l')
h_clone3 = h_pdf169.Clone("h_clone3")
h_clone3.SetLineColor(5)
legend.AddEntry(h_clone3,'m_{t} = 172 GeV MC Fit','l')
h_clone4 = h_pdf169.Clone("h_clone4")
h_clone4.SetLineColor(6)
legend.AddEntry(h_clone4,'m_{t} = 172.25 GeV MC Fit','l')
h_clone5 = h_pdf169.Clone("h_clone5")
h_clone5.SetLineColor(7)
legend.AddEntry(h_clone5,'m_{t} = 172.5 GeV MC Fit','l')
h_clone6 = h_pdf169.Clone("h_clone6")
h_clone6.SetLineColor(8)
legend.AddEntry(h_clone6,'m_{t} = 172.75 GeV MC Fit','l')
h_clone7 = h_pdf169.Clone("h_clone7")
h_clone7.SetLineColor(9)
legend.AddEntry(h_clone7,'m_{t} = 173 GeV MC Fit','l')
h_clone8 = h_pdf169.Clone("h_clone8")
h_clone8.SetLineColor(46)
legend.AddEntry(h_clone8,'m_{t} = 174 GeV MC Fit','l')
h_clone9 = h_pdf169.Clone("h_clone9")
h_clone9.SetLineColor(48)
legend.AddEntry(h_clone9,'m_{t} = 175 GeV MC Interpolated','l')
h_clone10 = h_pdf169.Clone("h_clone10")
h_clone10.SetLineColor(42)
legend.AddEntry(h_clone10,'m_{t} = 176 GeV MC Fit','l')
legend.SetBorderSize(0)
legend.SetFillColor(0)
legend.SetTextSize(0.03)
legend.Draw()
c.Update()
c.SaveAs('./plots/morphs.png')
"""

# Morph to data
fit172p5 = morph.fitTo(ds172p5, ROOT.RooFit.Save(), ROOT.RooFit.PrintLevel(2))
h_ds172p5, h_pdf172p5, integral172p5 = binHist(ds172p5, morph, 250, 25)

h_ds172p5.GetXaxis().SetTitle("m_{l+J/#psi} [GeV]")
h_ds172p5.GetYaxis().SetTitle("Events")
h_ds172p5.SetMaximum(h_ds172p5.GetMaximum() * 1.3)
h_ds172p5.Draw("p")
h_pdf172p5.Draw("hist same")

astyle.ATLASLabel(0.4, 0.87, "Simulation")
autils.DrawText(0.5, 0.82, "#sqrt{s} = 13 TeV, 139 fb^{-1}", size=0.03)
result = "m_{{t}} = {0:.2f} +/- {1:.2f} (stat).\n".format(mu.getValV(), mu.getError())
# result = '#Delta m_{{t}} = {0:.2f} (stat).\n'.format(mu.getError())
autils.DrawText(0.5, 0.77, result, size=0.03)
legend = ROOT.TLegend(0.6, 0.6, 0.9, 0.7)
legend.AddEntry(h_ds172p5, "MC m_{t} = 172.5 GeV data", "p l")
legend.AddEntry(h_pdf172p5, "Morph best Fit", "l")
legend.SetBorderSize(0)
legend.SetFillColor(0)
legend.SetTextSize(0.03)
legend.Draw()

c.Update()
c.SaveAs("./plots/morph_data_result.png")

# print('\nTop quark mass is {0:.2f} +/- {1:.2f} (stat).\n'.format(mu.getValV(),mu.getError()))
# print('\nTop quark mass uncertainty is {1:.2f} (stat).\n'.format(mu.getValV(),mu.getError()))
