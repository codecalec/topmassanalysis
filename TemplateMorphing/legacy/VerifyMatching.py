import ROOT
import os, sys, errno
from glob import glob
from atlasplots import atlas_style as astyle
from atlasplots import utils as autils
from ROOT import *

astyle.SetAtlasStyle()
ROOT.gROOT.SetBatch(ROOT.kTRUE)

### Define some useful functions
def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


# Take input list and load the tree that has the relevant information,
# checks the number of entries and returns the tree
def getTree(dsidList):
    fileList = []
    for dsid in dsidList:
        fileList.append("/atlas/DATAMC/TopMass/topMassNtuples/" + dsid + "/*")
    tree = ROOT.TChain("nominal")
    for fileName in fileList:
        tree.Add(fileName)
    print(tree.GetEntries(), "entries in dsids", dsidList)
    return tree


# Take the event selection output and matches the lepton with the J/Psi
# that pass the selection criteria
def m3l(event):
    Ji = event.Jpsi_selected
    Jpt, Jeta, Jphi, Jm = (
        event.Jpsi_pt[Ji],
        event.Jpsi_eta[Ji],
        event.Jpsi_phi[Ji],
        event.Jpsi_m[Ji],
    )
    J4V = ROOT.TLorentzVector()
    J4V.SetPtEtaPhiM(Jpt, Jeta, Jphi, Jm)

    Li, Lpt, Leta, Lphi, Le = -1, 0.0, 0.0, 0.0, 0.0
    if event.electron_selected >= 0:
        Li = event.electron_selected
        Lpt, Leta, Lphi, Le = (
            event.electron_pt[Li],
            event.electron_eta[Li],
            event.electron_phi[Li],
            event.electron_E[Li],
        )
    elif event.muon_selected >= 0:
        Li = event.muon_selected
        Lpt, Leta, Lphi, Le = (
            event.muon_pt[Li],
            event.muon_eta[Li],
            event.muon_phi[Li],
            event.muon_E[Li],
        )
    else:
        print("bad!")
    L4V = ROOT.TLorentzVector()
    L4V.SetPtEtaPhiE(Lpt, Leta, Lphi, Le)
    return (J4V + L4V).M() * 0.001


# Take into account event weights and detector effects
def evtW(event):
    eventWeight = 1.0
    eventWeight *= (
        event.mcWeightOrg
        * event.jvtEventWeight
        * event.pileupEventWeight
        * event.MV2c10_77_EventWeight
        / event.totalEventsWeighted
    )

    # Muons from Jpsi
    muonsFromJpsi = []
    for ji, (pt, decay_muons) in enumerate(zip(event.Jpsi_pt, event.Jpsi_muons)):
        if ji == event.Jpsi_selected:
            for d in decay_muons:
                muonsFromJpsi.append(d)
    eventWeight *= (
        event.MU_SF_ID_LowPt[muonsFromJpsi[0]] * event.MU_SF_ID_LowPt[muonsFromJpsi[1]]
    )

    Li, Lpt, Leta, Lphi, Le = -1, 0.0, 0.0, 0.0, 0.0
    if event.electron_selected >= 0:
        Li = event.electron_selected
        eventWeight *= (
            event.EL_SF_Trigger_TightLH[Li]
            * event.EL_SF_Reco[Li]
            * event.EL_SF_ID_TightLH[Li]
            * event.EL_SF_Iso_Gradient[Li]
        )
    elif event.muon_selected >= 0:
        Li = event.muon_selected
        eventWeight *= (
            event.MU_SF_Trigger_Medium[Li]
            * event.MU_SF_ID_Medium[Li]
            * event.MU_SF_TTVA[Li]
            * event.MU_SF_Isol_Gradient[Li]
        )
    else:
        print("bad!")
    return eventWeight


# Create histograms when plotting
def binHist(dataset, pdf, smooth, nBin, integral=-1):
    h_data = dataset.binnedClone("h_data", "h_data").createHistogram("m", nBin)
    h_fit = pdf.createHistogram("m", nBin * smooth)
    if integral < 0:
        dataInt = 1
        fitInt = h_data.Integral() / h_fit.Integral() * smooth
    else:
        dataInt = integral / h_data.Integral()
        fitInt = integral / h_fit.Integral() * smooth

    h_data.Scale(dataInt)
    h_fit.Scale(fitInt)
    return h_data, h_fit, h_data.Integral()


### Store plots in a folder
mkdir_p("./plots")

### Import the Monte Carlo datasets, and the ATLAS data. Define the TTrees.
tree169 = getTree(["mtop169"])
tree171 = getTree(["mtop171"])
tree172 = getTree(["mtop172"])
# tree172p25 = getTree(["mtop172p25"])
tree172p5 = getTree(["mtop172p5"])
# tree172p75 = getTree(["mtop172p75"])
tree173 = getTree(["mtop173"])
tree174 = getTree(["mtop174"])
tree176 = getTree(["mtop176"])
treeData = getTree(["data"])

### Setup a workspace
f = TFile("other.root", "RECREATE")
mlo, mhi = 20, 200
w = ROOT.RooWorkspace()
m = w.factory("m[" + str(mlo) + "," + str(mhi) + "]")


trees = [
    tree169,
    tree171,
    tree172,
    # tree172p25,
    tree172p5,
    # tree172p75,
    tree173,
    tree174,
    tree176,
]
names = [
    "ds169",
    "ds171",
    "ds172",
    # "ds172p25",
    "ds172.5",
    # "ds172p75",
    "ds173",
    "ds174",
    "ds176",
]
names_mm = [
    "ds169_mm",
    "ds171_mm",
    "ds172_mm",
    # "ds172p25_mm",
    "ds172.5_mm",
    # "ds172p75_mm",
    "ds173_mm",
    "ds174_mm",
    "ds176_mm",
]

datasets = []
datasets_mm = []

for name, name_mm in zip(names, names_mm):
    # ds = w.factory("DataHist::{name}(m)".format(name=name))
    # datasets.append(ds)

    # ds_mm = w.factory("DataHist::{name}(m)".format(name=name_mm))
    # datasets_mm.append(ds_mm)

    datasets.append(ROOT.RooDataHist(name, name, ROOT.RooArgSet(m)))
    datasets_mm.append(ROOT.RooDataHist(name_mm, name_mm, ROOT.RooArgSet(m)))


for dataset, dataset_mm, tree in zip(datasets, datasets_mm, trees):
    for event in tree:
        p = m3l(event)
        if mlo < p < mhi:
            #CHANGE IF DSID NOT INDACATOR OF ORIGIN
            isAntiTopEvent = int(event.mc_DSID) % 2

            if event.electron_selected >= 0:
                Li = event.electron_selected
                leptonFromAntitop = event.electron_ID[Li] < 0
            elif event.muon_selected >= 0:
                Li = event.electron_selected
                leptonFromAntitop = event.muon_ID[Li] < 0
            else:
                print("bad!")

            m.setVal(p)
            if not (isAntiTopEvent ^ leptonFromAntitop):
                dataset.add(ROOT.RooArgSet(m), evtW(event))
            else:
                dataset_mm.add(ROOT.RooArgSet(m), evtW(event))

# Build the RooFit Model with the different mass points
# pdf is a crystalball function
massPoints = [
    169,
    171,
    172,
    # 172.25,
    172.5,
    # 172.75,
    173,
    174,
    176,
]

pdfList = ROOT.RooArgList()
pdfList_mm = ROOT.RooArgList()

for mass in massPoints:
    pdf = w.factory(
        "CBShape::trileptonmass{mass}(m,mean[70.0,60.0,80.0],sigma[20.0,10.0,35.0],alpha[-1.0,-5.0,-0.0],n[3.0,0.0,5.0])".format(
            mass=mass
        )
    )
    pdfList.add(pdf)

for mass in massPoints:
    pdf = w.factory(
        "CBShape::trileptonmass_mm{mass}(m,mean_mm[70.0,60.0,80.0],sigma_mm[20.0,10.0,35.0],alpha_mm[-1.0,-2.0,-0.0],n_mm[5.0,0.0,10.0])".format(
            mass=mass
        )
    )
    pdfList_mm.add(pdf)

for i, (dataset, dataset_mm) in enumerate(zip(datasets, datasets_mm)):
    pdfList[i].fitTo(dataset, ROOT.RooFit.Save(), ROOT.RooFit.PrintLevel(3))
    pdfList_mm[i].fitTo(dataset_mm, ROOT.RooFit.Save(), ROOT.RooFit.PrintLevel(3))


### Create the morph object



m3l_list = []
m3l_list_error = []
top_mass_list = []
top_mass_list_error = []


m3l_list_mm = []
m3l_list_mm_error = []
top_mass_list_mm = []
top_mass_list_mm_error = []

from copy import deepcopy

for dataset_temp, dataset_temp_mm, mass_temp, pdf_temp,pdf_temp_mm, index in zip(
    datasets, datasets_mm, massPoints,
    pdfList,pdfList_mm, range(0,len(massPoints))
)[1:-1]:

    print "Creating template at mass", mass_temp

    masses = deepcopy(massPoints)
    masses.pop(massPoints.index(mass_temp))


    pdfs = ROOT.RooArgList()
    pdfs_mm = ROOT.RooArgList()

    include = range(0,len(massPoints))
    include.remove(index)

    for i in include:
        pdfs.add(pdfList[i])
        pdfs_mm.add(pdfList_mm[i])


    mu = w.factory("mu[169,176]")
    paramVec = ROOT.TVectorD(len(masses))

    for i, mass in enumerate(masses):
        paramVec[i] = mass

    c = ROOT.TCanvas("c", "c", 800, 700)

    morph = ROOT.RooMomentMorph(
        "RMM"+str(mass_temp), "RMM"+str(mass_temp), mu, ROOT.RooArgList(m), pdfs, paramVec, ROOT.RooMomentMorph.Linear
    )
    getattr(w, "import")(morph)

    # Morph to data
    # Matched data
    morph.fitTo(dataset_temp, ROOT.RooFit.Save(), ROOT.RooFit.PrintLevel(0), ROOT.RooFit.Hesse(False), ROOT.RooFit.Minos(True))
    m3l_list.append(m.getValV())
    m3l_list_error.append(m.getError())
    top_mass_list.append(mu.getValV())
    top_mass_list.append(mu.getError())

    h_ds, h_pdf, integral = binHist(dataset_temp, morph, 250, 25)

    h_ds.GetXaxis().SetTitle("m_{l+J/#psi} [GeV]")
    h_ds.GetYaxis().SetTitle("Events")
    h_ds.SetMaximum(h_ds.GetMaximum() * 1.3)
    h_ds.Draw("p")
    h_pdf.Draw("hist same")

    astyle.ATLASLabel(0.4, 0.87, "Simulation")
    autils.DrawText(0.5, 0.82, "#sqrt{s} = 13 TeV, 139 fb^{-1}", size=0.03)
    result = "m_{{t}} = {0:.2f} +/- {1:.2f} (stat).\n".format(
        mu.getValV(), mu.getError()
    )
    # result = '#Delta m_{{t}} = {0:.2f} (stat).\n'.format(mu.getError())
    autils.DrawText(0.5, 0.77, result, size=0.03)
    legend = ROOT.TLegend(0.6, 0.6, 0.9, 0.7)
    legend.AddEntry(h_ds, str(mass_temp), "p l")
    legend.AddEntry(h_pdf, "Morph best Fit", "l")
    legend.SetBorderSize(0)
    legend.SetFillColor(0)
    legend.SetTextSize(0.03)
    legend.Draw()

    c.Update()
    c.SaveAs("./plots/morph_match_data_result_" + str(mass_temp) + ".png")

    f.Write()

    morph_mm = ROOT.RooMomentMorph(
        "RMM_mm"+str(mass_temp),
        "RMM_mm"+str(mass_temp),
        mu,
        ROOT.RooArgList(m),
        pdfList_mm,
        paramVec,
        ROOT.RooMomentMorph.Linear,
    )
    getattr(w, "import")(morph_mm)

    # Morph to data
    # Mismatched data
    morph_mm.fitTo(dataset_temp_mm, ROOT.RooFit.Save(), ROOT.RooFit.PrintLevel(0), ROOT.RooFit.Hesse(False), ROOT.RooFit.Minos(True))
    m3l_list_mm.append(m.getValV())
    m3l_list_mm_error.append(m.getError())
    top_mass_list_mm.append(mu.getValV())
    top_mass_list_mm.append(mu.getError())

    h_ds, h_pdf, integral = binHist(dataset_temp_mm, morph_mm, 250, 25)

    h_ds.GetXaxis().SetTitle("m_{l+J/#psi} [GeV]")
    h_ds.GetYaxis().SetTitle("Events")
    h_ds.SetMaximum(h_ds.GetMaximum() * 1.3)
    h_ds.Draw("p")
    h_pdf.Draw("hist same")

    astyle.ATLASLabel(0.4, 0.87, "Simulation")
    autils.DrawText(0.5, 0.82, "#sqrt{s} = 13 TeV, 139 fb^{-1}", size=0.03)
    result = "m_{{t}} = {0:.2f} +/- {1:.2f} (stat).\n".format(
        mu.getValV(), mu.getError()
    )
    # result = '#Delta m_{{t}} = {0:.2f} (stat).\n'.format(mu.getError())
    autils.DrawText(0.5, 0.77, result, size=0.03)
    legend = ROOT.TLegend(0.6, 0.6, 0.9, 0.7)
    legend.AddEntry(h_ds, str(mass_temp), "p l")
    legend.AddEntry(h_pdf, "Morph best Fit", "l")
    legend.SetBorderSize(0)
    legend.SetFillColor(0)
    legend.SetTextSize(0.03)
    legend.Draw()

    c.Update()
    c.SaveAs("./plots/morph_mismatch_data_result_" + str(mass_temp) + ".png")
# print('\nTop quark mass is {0:.2f} +/- {1:.2f} (stat).\n'.format(mu.getValV(),mu.getError()))
# print('\nTop quark mass uncertainty is {1:.2f} (stat).\n'.format(mu.getValV(),mu.getError()))

print(m3l_list,"+-",m3l_list_error)
print(m3l_list_mm,"+-",m3l_list_mm_error)

print(top_mass_list,"+-",top_mass_list_error)
print(top_mass_list_mm,"+-",top_mass_list_mm_error)

graph = ROOT.TGraphErrors(len(massPoints),top_mass_list,m3l_list,,top_mass_list_error,m3l_list_error)
graph.GetXaxis().SetTitle("#m_{t} [GeV]")
graph.GetYaxis().SetTitle("#m_{l+J/#psi} [GeV]")
graph.Draw("ALP")

graph_mm = ROOT.TGraphErrors(len(massPoints),top_mass_list_mm,m3l_list_mm,top_mass_list_mm_error,m3l_list_mm_error)
graph_mm.Draw("ALP same")

c.Update()
c.SaveAs("./plots/morphing_comparison.png")

f.Write()
