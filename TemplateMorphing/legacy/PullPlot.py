import ROOT
import os
import errno
from atlasplots import atlas_style as astyle
from atlasplots import utils as autils
from ROOT import *

astyle.SetAtlasStyle()
ROOT.gROOT.SetBatch(ROOT.kTRUE)

# Count number of entries in tree
def getNumOfTreeEntries(filename):
    tree = ROOT.TChain("nominal")
    tree.Add(filename)
    return tree.GetEntries()


# Define some useful functions
def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


# Take input list and load the tree that has the relevant information,
# checks the number of entries and returns the tree
def getTree(dsidList):
    fileList = []
    for dsid in dsidList:
        fileList.append("/atlas/aveltman/DATAMC/TopMass/topParentNtuples/" + dsid + "/*")
    tree = ROOT.TChain("nominal")
    for fileName in fileList:
        tree.Add(fileName)
    print(tree.GetEntries(), "entries in dsids", dsidList)
    return tree


# Take the event selection output and matches the lepton with the J/Psi
# that pass the selection criteria
def m3l(event):
    Ji = event.Jpsi_selected
    Jpt, Jeta, Jphi, Jm = (
        event.Jpsi_pt[Ji],
        event.Jpsi_eta[Ji],
        event.Jpsi_phi[Ji],
        event.Jpsi_m[Ji],
    )
    J4V = ROOT.TLorentzVector()
    J4V.SetPtEtaPhiM(Jpt, Jeta, Jphi, Jm)

    Li, Lpt, Leta, Lphi, Le = -1, 0.0, 0.0, 0.0, 0.0
    if event.electron_selected >= 0:
        Li = event.electron_selected
        Lpt, Leta, Lphi, Le = (
            event.electron_pt[Li],
            event.electron_eta[Li],
            event.electron_phi[Li],
            event.electron_E[Li],
        )
    elif event.muon_selected >= 0:
        Li = event.muon_selected
        Lpt, Leta, Lphi, Le = (
            event.muon_pt[Li],
            event.muon_eta[Li],
            event.muon_phi[Li],
            event.muon_E[Li],
        )
    else:
        print("bad!")
    L4V = ROOT.TLorentzVector()
    L4V.SetPtEtaPhiE(Lpt, Leta, Lphi, Le)
    return (J4V + L4V).M() * 0.001


# Take into account event weights and detector effects
def evtW(event):
    eventWeight = 1.0
    eventWeight *= (
        event.mcWeightOrg
        * event.jvtEventWeight
        * event.pileupEventWeight
        * event.MV2c10_70_EventWeight
        / event.totalEventsWeighted
    )

    # Muons from Jpsi
    muonsFromJpsi = []
    for ji, (pt, decay_muons) in enumerate(zip(event.Jpsi_pt, event.Jpsi_muons)):
        if ji == event.Jpsi_selected:
            for d in decay_muons:
                muonsFromJpsi.append(d)
    eventWeight *= (
        event.MU_SF_ID_LowPt[muonsFromJpsi[0]] * event.MU_SF_ID_LowPt[muonsFromJpsi[1]]
    )

    Li, Lpt, Leta, Lphi, Le = -1, 0.0, 0.0, 0.0, 0.0
    if event.electron_selected >= 0:
        Li = event.electron_selected
        eventWeight *= (
            event.EL_SF_Trigger_TightLH[Li]
            * event.EL_SF_Reco[Li]
            * event.EL_SF_ID_TightLH[Li]
            * event.EL_SF_Iso_Gradient[Li]
        )
    elif event.muon_selected >= 0:
        Li = event.muon_selected
        eventWeight *= (
            event.MU_SF_Trigger_Medium[Li]
            * event.MU_SF_ID_Medium[Li]
            * event.MU_SF_TTVA[Li]
            * event.MU_SF_Isol_Gradient[Li]
        )
    else:
        print("bad!")
    return eventWeight


# Create histograms when plotting
def binHist(dataset, pdf, smooth, nBin, integral=-1):
    h_data = dataset.createHistogram("m", nBin)
    h_fit = pdf.createHistogram("m", nBin * smooth)
    if integral < 0:
        dataInt = 1
        fitInt = h_data.Integral() / h_fit.Integral() * smooth
    else:
        dataInt = integral / h_data.Integral()
        fitInt = integral / h_fit.Integral() * smooth

    h_data.Scale(dataInt)
    h_fit.Scale(fitInt)
    return h_data, h_fit, h_data.Integral()


### Store plots in a folder
mkdir_p("./plots")

### Import the Monte Carlo datasets, and the ATLAS data. Define the TTrees.
tree169 = getTree(["mtop169"])
tree171 = getTree(["mtop171"])
tree172 = getTree(["mtop172"])
# tree172p25 = getTree(["mtop172p25"])
tree172p5 = getTree(["mtop172p5"])
# tree172p75 = getTree(["mtop172p75"])
tree173 = getTree(["mtop173"])
tree174 = getTree(["mtop174"])
tree176 = getTree(["mtop176"])
treeData = getTree(["data"])

### Setup a workspace
f = TFile("other.root", "RECREATE")
mlo, mhi = 25, 200
w = ROOT.RooWorkspace()
m = w.factory("m[" + str(mlo) + "," + str(mhi) + "]")


trees = [
    tree169,
    tree171,
    tree172,
    # tree172p25,
    # tree172p75,
    tree173,
    tree174,
    tree176,
]
names = [
    "ds169",
    "ds171",
    "ds172",
    # "ds172p25",
    # "ds172p75",
    "ds173",
    "ds174",
    "ds176",
]
names_mm = [
    "ds169_mm",
    "ds171_mm",
    "ds172_mm",
    # "ds172p25_mm",
    # "ds172p75_mm",
    "ds173_mm",
    "ds174_mm",
    "ds176_mm",
]
names_total = [
    "ds169_total",
    "ds171_total",
    "ds172_total",
    # "ds172p25_total",
    # "ds172p75_total",
    "ds173_total",
    "ds174_total",
    "ds176_total",
]

datasets = []
datasets_mm = []
datasets_total = []

for name, name_mm in zip(names, names_mm):
    # ds = w.factory("DataHist::{name}(m)".format(name=name))
    # datasets.append(ds)

    # ds_mm = w.factory("DataHist::{name}(m)".format(name=name_mm))
    # datasets_mm.append(ds_mm)

    datasets.append(ROOT.RooDataHist(name, name, ROOT.RooArgSet(m)))
    datasets_mm.append(ROOT.RooDataHist(name_mm, name_mm, ROOT.RooArgSet(m)))

for dataset, dataset_mm, tree in zip(datasets, datasets_mm, trees):
    for event in tree:
        p = m3l(event)
        if mlo < p < mhi:
            lepton_parent = -2
            if event.electron_selected >= 0:
                Li = event.electron_selected
                if (event.electron_ID[Li] < 0):
                    lepton_parent = 1
                else:
                    lepton_parent = -1
            elif event.muon_selected >= 0:
                Li = event.muon_selected
                if (event.muon_ID[Li] < 0):
                    lepton_parent = 1
                else:
                    lepton_parent = -1
            else:
                print ("bad!")

            Ji = event.Jpsi_selected
            Jpsi_barcode = event.Jpsi_truthMatch_barcode[Ji]
            Jpsi_parent = -2
            for i, barcode in enumerate(event.truth_barcode):
                if barcode == Jpsi_barcode:
                    Jpsi_topparent = bool(event.truth_topParent[i])
                    Jpsi_antitopparent = bool(event.truth_antiTopParent[i])
                    if (Jpsi_topparent == Jpsi_antitopparent):
                        Jpsi_parent = -2
                    elif (Jpsi_topparent == 1):
                        Jpsi_parent = 1
                    elif (Jpsi_antitopparent == 1):
                        Jpsi_parent = -1
                    break

            m.setVal(p)
            if Jpsi_parent == lepton_parent:
                dataset.add(ROOT.RooArgSet(m), evtW(event))
            else:
                dataset_mm.add(ROOT.RooArgSet(m), evtW(event))

for name_total, dataset, dataset_mm in zip(names_total, datasets, datasets_mm):
    ds = dataset.Clone(name_total)
    ds.add(dataset_mm)
    datasets_total.append(ds)

# Build the RooFit Model with the different mass points
# pdf is a crystalball function
massPoints = [
    169,
    171,
    172,
    # 172.25,
    # 172.75,
    173,
    174,
    176,
]

for ds, ds_mm, ds_total in zip(datasets, datasets_mm, datasets_total):
    getattr(w, "import")(ds)
    getattr(w, "import")(ds_mm)
    getattr(w, "import")(ds_total)

pdfList = ROOT.RooArgList()
pdfList_mm = ROOT.RooArgList()
pdfList_total = ROOT.RooArgList()

w.Print()

for mass in massPoints:
    # pdf = ROOT.RooHistPdf("template"+str(mass),"template"+str(mass),ROOT.RooArgSet(m),ROOT.RooArgSet(m),ds)
    pdf = w.factory("HistPdf::template{mass}(m,ds{mass})".format(mass=mass))
    pdfList.add(pdf)

for mass in massPoints:
    # pdf = ROOT.RooHistPdf("template_mm"+str(mass),"template_mm"+str(mass),ROOT.RooArgSet(m),ROOT.RooArgSet(m),ds)
    pdf = w.factory("HistPdf::template_mm{mass}(m,ds{mass}_mm)".format(mass=mass))
    pdfList_mm.add(pdf)

for mass in massPoints:
    pdf = w.factory("HistPdf::template_total{mass}(m,ds{mass}_total)".format(mass=mass))
    pdfList_total.add(pdf)

# Create smaller MC samples
size_of_subsets = treeData.GetEntries()
num_of_subsets = tree172p5.GetEntries() // size_of_subsets

subsets = []
subsets_mm = []
subsets_total = []

event_count = 0
set_count = 0

subset = ROOT.RooDataHist(
    "subset" + str(set_count), "subset" + str(set_count), ROOT.RooArgSet(m)
)
subset_mm = ROOT.RooDataHist(
    "subset" + str(set_count) + "_mm",
    "subset" + str(set_count) + "_mm",
    ROOT.RooArgSet(m),
)
subset_total = ROOT.RooDataHist(
    "subset" + str(set_count) + "_total",
    "subset" + str(set_count) + "_total",
    ROOT.RooArgSet(m),
)

print("Making", num_of_subsets, "of", size_of_subsets, "events")

for event in tree172p5:
    # check if reached right amount of subsets
    if set_count == num_of_subsets:
        break

    # filter event into subset
    p = m3l(event)
    if mlo < p < mhi:
        lepton_parent = -2
        if event.electron_selected >= 0:
            Li = event.electron_selected
            if (event.electron_ID[Li] < 0):
                lepton_parent = 1
            else:
                lepton_parent = -1
        elif event.muon_selected >= 0:
            Li = event.muon_selected
            if (event.muon_ID[Li] < 0):
                lepton_parent = 1
            else:
                lepton_parent = -1
        else:
            print ("bad!")

        Ji = event.Jpsi_selected
        Jpsi_barcode = event.Jpsi_truthMatch_barcode[Ji]
        Jpsi_parent = -2
        for i, barcode in enumerate(event.truth_barcode):
            if barcode == Jpsi_barcode:
                Jpsi_topparent = bool(event.truth_topParent[i])
                Jpsi_antitopparent = bool(event.truth_antiTopParent[i])
                if (Jpsi_topparent == Jpsi_antitopparent):
                    Jpsi_parent = -2
                elif (Jpsi_topparent == 1):
                    Jpsi_parent = 1
                elif (Jpsi_antitopparent == 1):
                    Jpsi_parent = -1
                break

        m.setVal(p)
        if Jpsi_parent == lepton_parent:
            subset.add(ROOT.RooArgSet(m), evtW(event))
        else:
            subset_mm.add(ROOT.RooArgSet(m), evtW(event))
        subset_total.add(ROOT.RooArgSet(m), evtW(event))

    # if subset is correct size, add to list and create new DataHist Object
    event_count += 1
    if event_count % size_of_subsets == 0:
        print("Filled subset",set_count)
        subsets.append(subset)
        subsets_mm.append(subset_mm)
        subsets_total.append(subset_total)

        subset = ROOT.RooDataHist(
            "subset" + str(set_count), "subset" + str(set_count), ROOT.RooArgSet(m)
        )
        subset_mm = ROOT.RooDataHist(
            "subset" + str(set_count) + "_mm",
            "subset" + str(set_count) + "_mm",
            ROOT.RooArgSet(m),
        )
        subset_total = ROOT.RooDataHist(
            "subset" + str(set_count) + "_total",
            "subset" + str(set_count) + "_total",
            ROOT.RooArgSet(m),
        )

        set_count += 1

# for i, (subset, subset_mm) in enumerate(zip(subsets, subsets_mm)):
    # subset = dataset.Clone("subset" + str(i) + "_total")
    # subset.add(dataset_mm)
    # subsets_total.append(subset)


pull_values = []
pull_values_mm = []
pull_values_total = []

actual_mass = 172.5

### Create the morph object
mu = w.factory("mu[169.0,176.0]")
mu_mm = w.factory("mu_mm[169.0,176.0]")
mu_total = w.factory("mu_total[169.0,176.0]")

# Create Vector with each mass point for morphing
paramVec = ROOT.TVectorD(len(massPoints))
for i, mass in enumerate(massPoints):
    paramVec[i] = mass


for i, (subset, subset_mm, subset_total) in enumerate(
    zip(subsets, subsets_mm, subsets_total)
):

    morph = ROOT.RooMomentMorph(
        "RMM" + str(i),
        "RMM" + str(i),
        mu,
        ROOT.RooArgList(m),
        pdfList,
        paramVec,
        ROOT.RooMomentMorph.Linear,
    )
    getattr(w, "import")(morph)
    morph.fitTo(
        subset,
        ROOT.RooFit.SumW2Error(False),
        ROOT.RooFit.Save(),
        ROOT.RooFit.PrintLevel(2),
    )
    pull_values.append(
        [mu.getValV(), mu.getError(), (mu.getValV() - actual_mass) / (mu.getError())]
    )
    mu.setVal(actual_mass)

    morph_mm = ROOT.RooMomentMorph(
        "RMM_mm" + str(i),
        "RMM_mm" + str(i),
        mu_mm,
        ROOT.RooArgList(m),
        pdfList_mm,
        paramVec,
        ROOT.RooMomentMorph.Linear,
    )
    getattr(w, "import")(morph_mm)
    morph_mm.fitTo(
        subset_mm,
        ROOT.RooFit.SumW2Error(False),
        ROOT.RooFit.Save(),
        ROOT.RooFit.PrintLevel(2),
    )
    pull_values_mm.append(
        [
            mu_mm.getValV(),
            mu_mm.getError(),
            (mu_mm.getValV() - actual_mass) / (mu_mm.getError()),
        ]
    )
    mu_mm.setVal(actual_mass)

    morph_total = ROOT.RooMomentMorph(
        "RMM_total" + str(i),
        "RMM_total" + str(i),
        mu_total,
        ROOT.RooArgList(m),
        pdfList_total,
        paramVec,
        ROOT.RooMomentMorph.Linear,
    )
    getattr(w, "import")(morph_total)
    morph_total.fitTo(
        subset_total,
        ROOT.RooFit.SumW2Error(False),
        ROOT.RooFit.Save(),
        ROOT.RooFit.PrintLevel(2),
    )
    pull_values_total.append(
        [
            mu_total.getValV(),
            mu_total.getError(),
            (mu_total.getValV() - actual_mass) / (mu_total.getError()),
        ]
    )
    mu_total.setVal(actual_mass)

c = ROOT.TCanvas("c", "c", 800, 700)

hpull = ROOT.TH1F("hpull", "hpull", 60, -2, 2)

print("pull")
for value in pull_values:
    print(value)
    hpull.Fill(value[2])
hpull.Draw("E P")
hpull.GetXaxis().SetTitle("m_t Pull")
hpull.GetYaxis().SetTitle("Count")
legend = ROOT.TLegend(0.6, 0.8, 0.9, 0.8)
legend.AddEntry(hpull, "Correctly Paired")
legend.SetBorderSize(0)
legend.SetFillColor(0)
legend.SetTextSize(0.03)
legend.Draw()
c.Update()
c.SaveAs("./plots/pull.png")


hpull_mm = ROOT.TH1F("hpull_mm", "hpull_mm", 60, -2, 2)

print("pull_mm")
for value in pull_values_mm:
    print(value)
    hpull_mm.Fill(value[2])
hpull_mm.Draw("E P")
hpull_mm.GetXaxis().SetTitle("m_t Pull")
hpull_mm.GetYaxis().SetTitle("Count")
legend = ROOT.TLegend(0.6, 0.8, 0.9, 0.8)
legend.AddEntry(hpull_mm, "Correctly Paired")
legend.SetBorderSize(0)
legend.SetFillColor(0)
legend.SetTextSize(0.03)
legend.Draw()
c.Update()
c.SaveAs("./plots/pull_mm.png")


hpull_total = ROOT.TH1F("hpull_total", "hpull_total", 60, -2, 2)
print("pull_total")
for value in pull_values_total:
    print(value)
    hpull_total.Fill(value[2])
hpull_total.Draw("E P")
hpull_total.GetXaxis().SetTitle("m_t Pull")
hpull_total.GetYaxis().SetTitle("Count")
legend = ROOT.TLegend(0.6, 0.8, 0.9, 0.8)
legend.AddEntry(hpull_total, "Correctly Paired")
legend.SetBorderSize(0)
legend.SetFillColor(0)
legend.SetTextSize(0.03)
legend.Draw()
c.Update()
c.SaveAs("./plots/pull_total.png")

f.Write()
f.Close()
