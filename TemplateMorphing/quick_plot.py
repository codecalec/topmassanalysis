import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import pathlib

PLOT_PATH = pathlib.Path("./plots")


def read_file(filename: str):
    df = pd.read_csv(filename)
    return df


def delta(x, s):
    return (
        np.sum(1 / s ** 2) * np.sum(x ** 2 / s ** 2) - np.sum(x / s ** 2) ** 2
    )


def gradient(x, y, s):
    numerator = np.sum(1 / s ** 2) * np.sum(x * y / s ** 2) - np.sum(
        x / s ** 2
    ) * np.sum(y / s ** 2)
    return numerator / delta(x, s)


def gradient_err(x, s):
    return np.sqrt(np.sum(1 / s ** 2) / delta(x, s))


def intercept(x, y, s):
    numerator = np.sum(x ** 2 / s ** 2) * np.sum(y / s ** 2) - np.sum(
        x / s ** 2
    ) * np.sum(x * y / s ** 2)
    return numerator / delta(x, s)


def intercept_err(x, s):
    return np.sqrt(np.sum(x ** 2 / s ** 2) / delta(x, s))


def plot_model(x, y, yerr):
    grad = gradient(x, y, yerr)
    grad_err = gradient_err(x, yerr)

    inter = intercept(x, y, yerr)
    inter_err = intercept_err(x, yerr)

    plt.plot(
        x,
        grad * x + inter,
        label=f"$m = {grad:.3f} \\pm {grad_err:.3f}$\n$c = {inter:.3f} \\pm {inter_err:.3f}$",
    )


if __name__ == "__main__":
    df = read_file("combined_param.csv")

    plt.style.use(["science", "no-latex"])

    plt.figure(dpi=200)
    plt.errorbar(
        df["mass"],
        df["mean"],
        yerr=df["mean_err"],
        ls="None",
        fmt="ko",
    )
    plt.xlabel(r"$m^{gen}_{top}$ [MeV]")
    plt.ylabel("mean")
    plot_model(df["mass"], df["mean"], yerr=df["mean_err"])
    plt.legend()
    plt.savefig(PLOT_PATH / "mean_plot.png")
    plt.clf()

    plt.figure(dpi=200)
    plt.errorbar(
        df["mass"],
        df["sigma"],
        yerr=df["sigma_err"],
        ls="None",
        fmt="ko",
        label="Sigma Parameters"
    )
    plt.xlabel(r"$m^{gen}_{top}$ [MeV]")
    plt.ylabel("sigma")
    plot_model(df["mass"], df["sigma"], yerr=df["sigma_err"])
    plt.legend()
    plt.savefig(PLOT_PATH / "sigma_plot.png")
    plt.clf()

    plt.figure(dpi=200)
    plt.errorbar(
        df["mass"],
        df["gamma"],
        yerr=df["gamma_err"],
        ls="None",
        fmt="ko",
        label="Gamma Parameters"
    )
    plt.xlabel(r"$m^{gen}_{top}$ [MeV]")
    plt.ylabel("gamma")
    plot_model(df["mass"], df["gamma"], yerr=df["gamma_err"])
    plt.legend()
    plt.savefig(PLOT_PATH / "gamma_plot.png")
    plt.clf()

    plt.figure(dpi=200)
    plt.errorbar(
        df["mass"],
        df["beta"],
        yerr=df["beta_err"],
        ls="None",
        fmt="ko",
        label="Beta Parameters"
    )
    plt.xlabel(r"$m^{gen}_{top}$ [MeV]")
    plt.ylabel("beta")
    plot_model(df["mass"], df["beta"], yerr=df["beta_err"])
    plt.legend()
    plt.savefig(PLOT_PATH / "beta_plot.png")
    plt.clf()

    plt.figure(dpi=200)
    plt.errorbar(
        df["mass"],
        df["mu"],
        yerr=df["mu_err"],
        ls="None",
        fmt="ko",
        label="Mu Parameters"
    )
    plt.xlabel(r"$m^{gen}_{top}$ [MeV]")
    plt.ylabel("mu")
    plot_model(df["mass"], df["mu"], yerr=df["mu_err"])
    plt.legend()
    plt.savefig(PLOT_PATH / "mu_plot.png")
    plt.clf()
