import csv
from typing import List
import numpy as np
import ROOT
from atlasplots import atlas_style as astyle
from atlasplots import utils as autils

from main import names, names_mm, massPoints


def fit_linear(
    x: List[float],
    y: List[float],
    y_err: List[float],
    model_name: str = None,
    axis_label: str = None,
    reference: float = 0,
    reference_err: float = 0,
) -> (float, float, float, float):
    from array import array

    c = ROOT.TCanvas()
    x = array("f", x)
    x_err = array(
        "f",
        [0] * len(x),
    )

    y = array("f", y)
    y_err = array("f", y_err)
    graph = ROOT.TGraphErrors(len(x), x, y, x_err, y_err)

    if reference:
        ref = array("f", [reference])
        ref_err = array("f", [reference_err])
        graph_ref = ROOT.TGraphErrors(
            1, array("f", [172.5]), ref, array("f", [0]), ref_err
        )
        graph_ref.SetMarkerColor(2)

    poly = ROOT.TF1("poly", "[0] * x + [1]", 169, 176)
    poly.SetLineColor(4)

    result = graph.Fit(poly, "S C E")
    grad, grad_err = result.Parameter(0), result.Error(0)
    intercept, intercept_err = result.Parameter(1), result.Error(1)

    graph.GetXaxis().SetTitle("m^{gen}_{top} [GeV]")
    graph.GetYaxis().SetTitle(axis_label)
    graph.Draw("ap")
    if reference:
        graph_ref.Draw("p")

    legend = ROOT.TLegend(0.22, 0.7, 0.3, 0.9)
    legend.AddEntry(graph, "Parameter from Templates", "p")
    if reference:
        legend.AddEntry(graph_ref, "172.5 GeV Reference Point", "p")
    legend.AddEntry(poly, "Weighted Linear Fit", "l")
    legend.AddEntry(poly, f"m = {grad:.4f} #pm {grad_err:.4f}", "")
    legend.AddEntry(poly, f"c = {intercept:.2f} #pm {intercept_err:.2f}", "")
    legend.SetBorderSize(0)
    legend.SetFillColor(0)
    legend.SetTextSize(0.03)
    legend.Draw()

    c.Update()
    c.SaveAs(f"./plots/Linear_model_{model_name}.png")

    return grad, grad_err, intercept, intercept_err


def fit_gaussian(datasets: List[ROOT.RooDataHist], chi_squared: bool = True):
    w = ROOT.RooWorkspace("fit", "fitting workspace")
    ds_names = names

    means = []
    means_err = []
    sigmas = []
    sigmas_err = []

    for ds, name, mass in zip(datasets, ds_names, massPoints):

        getattr(w, "import")(ds)
        gaus = w.factory(
            (
                f"Gaussian::gaus_{name}"
                f"(m, mean_{name}[50, 90], sigma_{name}[10, 40])"
            )
        )
        getattr(w, "import")(gaus)
        m = w.var("m")

        sample_mean = ds.mean(m)
        sample_sigma = ds.sigma(m)
        r_start = sample_mean - 2 * sample_sigma
        r_end = sample_mean + 2 * sample_sigma
        if r_start < 0:
            r_start = 0
        if r_end > 300:
            r_end = 300
        assert r_start < r_end
        print(name, r_start, r_end)

        fit_result = gaus.fitTo(
            ds,
            ROOT.RooFit.Range(r_start, r_end),
            ROOT.RooFit.SumW2Error(True),
            ROOT.RooFit.Strategy(2),
            ROOT.RooFit.Save(),
            ROOT.RooFit.PrintLevel(3),
        )

        frame = m.frame()
        frame.GetXaxis().SetTitle("m^{reco}_{l+J/#psi} [GeV]")
        frame.SetMinimum(0)
        ds.plotOn(frame, ROOT.RooFit.Name("data"))
        gaus.plotOn(frame, ROOT.RooFit.Name("gaus"))
        c = ROOT.TCanvas("c", "c", 800, 700)
        c.cd()
        frame.Draw()

        if chi_squared:
            n = fit_result.floatParsFinal().getSize()
            value = frame.chiSquare("gaus", "data", n)
            print(f"chi2 {name}", value, n)
            autils.DrawText(0.5, 0.69, f"#chi^{{2}}/dof = {value:.2f}", size=0.03)

        astyle.ATLASLabel(0.4, 0.87, "Simulation")
        autils.DrawText(0.5, 0.82, "#sqrt{s} = 13 TeV", size=0.03)

        mean = w.var(f"mean_{name}")
        sigma = w.var(f"sigma_{name}")

        means.append(mean.getValV())
        means_err.append(mean.getError())
        sigmas.append(sigma.getValV())
        sigmas_err.append(sigma.getError())
        autils.DrawText(
            0.5,
            0.78,
            (
                f"mean = {mean.getValV():.2f}"
                f" #pm {mean.getError():.2f} (stat).\n"
            ),
            size=0.03,
        )
        autils.DrawText(
            0.5,
            0.75,
            (
                f"#sigma = {sigma.getValV():.2f}"
                f" #pm {sigma.getError():.2f} (stat).\n"
            ),
            size=0.03,
        )

        legend = ROOT.TLegend(0.55, 0.5, 0.9, 0.6)
        legend.AddEntry("data", f"MC m_{{t}} = {mass} GeV Data", "p l")
        legend.AddEntry("gaus", gaus.GetTitle(), "l")
        legend.SetBorderSize(0)
        legend.SetFillColor(0)
        legend.SetTextSize(0.03)
        legend.Draw()

        c.Update()
        c.SaveAs(f"./plots/gaus_fit_{name}.png")

    with open("gaus_param.csv", "w") as f:
        writer = csv.writer(f)
        writer.writerow(["mass", "mean", "mean_err", "sigma", "sigma_err"])
        writer.writerows(
            np.transpose([massPoints, means, means_err, sigmas, sigmas_err])
        )

    del w, c


def fit_gamma(datasets: List[ROOT.RooDataHist], chi_squared: bool = True):
    c = ROOT.TCanvas("c", "c", 800, 700)
    w = ROOT.RooWorkspace("fit", "fitting workspace")

    # # ds172p5 = datasets.pop(len(datasets) // 2)
    # datasets.pop(len(datasets) // 2)
    # ds_names = names_mm
    # ds_names.pop(len(ds_names) // 2)
    # ds_mass = massPoints
    # ds_mass.pop(len(ds_mass) // 2)

    ds_names = names_mm

    gammas = []
    gammas_err = []
    betas = []
    betas_err = []
    mus = []
    mus_err = []

    for ds, name, mass in zip(datasets, ds_names, massPoints):

        getattr(w, "import")(ds)
        gamma = w.factory(
            (
                f"Gamma::gamma_{name}"
                f"(m, gam_{name}[1,10], beta_{name}[1,40], mu_{name}[-40,40])"
            )
        )
        getattr(w, "import")(gamma)
        m = w.var("m")

        r_start, r_end = 0, 300
        fit_result = gamma.fitTo(
            ds,
            ROOT.RooFit.Range(r_start, r_end),
            ROOT.RooFit.SumW2Error(True),
            ROOT.RooFit.Strategy(2),
            ROOT.RooFit.Save(),
            ROOT.RooFit.PrintLevel(2),
        )

        frame = m.frame()
        frame.GetXaxis().SetTitle("m^{reco}_{l+J/#psi} [GeV]")
        frame.SetMinimum(0)
        ds.plotOn(frame, ROOT.RooFit.Name("data"))
        gamma.plotOn(frame, ROOT.RooFit.Name("gamma"))
        c = ROOT.TCanvas("c", "c", 800, 700)
        c.cd()
        frame.Draw()

        if chi_squared:
            n = fit_result.floatParsFinal().getSize()
            value = frame.chiSquare("gamma", "data", n)
            print(f"chi2 {name}", value, n)
            autils.DrawText(0.5, 0.69, f"#chi^{{2}}/dof = {value:.2f}", size=0.03)

        astyle.ATLASLabel(0.4, 0.87, "Simulation")
        autils.DrawText(0.5, 0.82, "#sqrt{s} = 13 TeV", size=0.03)

        gam = w.var(f"gam_{name}")
        beta = w.var(f"beta_{name}")
        mu = w.var(f"mu_{name}")

        gammas.append(gam.getValV())
        gammas_err.append(gam.getError())
        betas.append(beta.getValV())
        betas_err.append(beta.getError())
        mus.append(mu.getValV())
        mus_err.append(mu.getError())
        autils.DrawText(
            0.5,
            0.78,
            (
                f"#gamma = {gam.getValV():.2f}"
                f" #pm {gam.getError():.2f} (stat).\n"
            ),
            size=0.03,
        )
        autils.DrawText(
            0.5,
            0.75,
            (
                f"#beta = {beta.getValV():.2f}"
                f" #pm {beta.getError():.2f} (stat).\n"
            ),
            size=0.03,
        )
        autils.DrawText(
            0.5,
            0.72,
            (
                f"#mu = {mu.getValV():.2f}"
                f" #pm {mu.getError():.2f} (stat).\n"
            ),
            size=0.03,
        )

        legend = ROOT.TLegend(0.6, 0.6, 0.9, 0.7)
        legend.AddEntry("data", f"MC m_{{t}} = {mass} GeV Data", "p l")
        legend.AddEntry("gamma", gamma.GetTitle(), "l")
        legend.SetBorderSize(0)
        legend.SetFillColor(0)
        legend.SetTextSize(0.03)
        legend.Draw()

        c.Update()
        c.SaveAs(f"./plots/gamma_fit_{name}.png")

    with open("gamma_param.csv", "w") as f:
        writer = csv.writer(f)
        writer.writerow(
            ["name", "gamma", "gamma_err", "beta", "beta_err", "mu", "mu_err"]
        )
        writer.writerows(
            np.transpose(
                [
                    massPoints,
                    gammas,
                    gammas_err,
                    betas,
                    betas_err,
                    mus,
                    mus_err,
                ]
            )
        )

    del w, c


def fit_CB(datasets: List[ROOT.RooDataHist], chi_squared: bool = True):
    w = ROOT.RooWorkspace("fit", "fitting workspace")
    ds_names = names

    means = []
    means_err = []
    sigmas = []
    sigmas_err = []
    alphas = []
    alphas_err = []
    ns = []
    ns_err = []

    for ds, name, mass in zip(datasets, ds_names, massPoints):

        getattr(w, "import")(ds)
        cb = w.factory(
            (
                f"CBShape::cb_{name}"
                f"(m, mean_{name}[50, 90], sigma_{name}[10, 40],"
                f"alpha_{name}[3, 0, 10], n_{name}[1.1,1,1.5])"
            )
        )

        getattr(w, "import")(cb)
        m = w.var("m")

        r_start = 0
        r_end = 140

        fit_result = cb.fitTo(
            ds,
            ROOT.RooFit.Range(r_start, r_end),
            ROOT.RooFit.SumW2Error(True),
            ROOT.RooFit.Hesse(False),
            # ROOT.RooFit.Strategy(2),
            ROOT.RooFit.Save(),
            ROOT.RooFit.PrintLevel(2),
        )

        frame = m.frame()
        frame.GetXaxis().SetTitle("m^{reco}_{l+J/#psi} [GeV]")
        frame.SetMinimum(0)
        ds.plotOn(frame, ROOT.RooFit.Name("data"))
        cb.plotOn(frame, ROOT.RooFit.Name("cb"))
        c = ROOT.TCanvas("c", "c", 800, 700)
        c.cd()
        frame.Draw()

        if chi_squared:
            n = fit_result.floatParsFinal().getSize()
            value = frame.chiSquare("cb", "data", n)
            print(f"chi2 {name}", value, n)
            autils.DrawText(0.5, 0.66, f"#chi^{{2}}/dof = {value:.2f}", size=0.03)

        astyle.ATLASLabel(0.4, 0.87, "Simulation")
        autils.DrawText(0.5, 0.82, "#sqrt{s} = 13 TeV", size=0.03)

        mean = w.var(f"mean_{name}")
        sigma = w.var(f"sigma_{name}")
        alpha = w.var(f"alpha_{name}")
        n = w.var(f"n_{name}")

        means.append(mean.getValV())
        means_err.append(mean.getError())
        sigmas.append(sigma.getValV())
        sigmas_err.append(sigma.getError())
        alphas.append(alpha.getValV())
        alphas_err.append(alpha.getError())
        ns.append(n.getValV())
        ns_err.append(n.getError())

        autils.DrawText(
            0.5,
            0.78,
            (
                f"mean = {mean.getValV():.2f}"
                f" #pm {mean.getError():.2f} (stat).\n"
            ),
            size=0.03,
        )
        autils.DrawText(
            0.5,
            0.75,
            (
                f"#sigma = {sigma.getValV():.2f}"
                f" #pm {sigma.getError():.2f} (stat).\n"
            ),
            size=0.03,
        )
        autils.DrawText(
            0.5,
            0.72,
            (
                f"#alpha = {alpha.getValV():.2f}"
                f" #pm {alpha.getError():.2f} (stat).\n"
            ),
            size=0.03,
        )
        autils.DrawText(
            0.5,
            0.69,
            (f"n = {n.getValV():.2f}" f" #pm {n.getError():.2f} (stat).\n"),
            size=0.03,
        )

        legend = ROOT.TLegend(0.6, 0.5, 0.9, 0.6)
        legend.AddEntry("data", f"MC m_{{t}} = {mass} GeV Data", "p l")
        legend.AddEntry("cb", cb.GetTitle(), "l")
        legend.SetBorderSize(0)
        legend.SetFillColor(0)
        legend.SetTextSize(0.03)
        legend.Draw()

        c.Update()
        c.SaveAs(f"./plots/cb_fit_{name}.png")


def fit_combined(
    datasets: List[ROOT.RooDataHist],
    names: List[str],
    chi_squared: bool = True,
):
    c = ROOT.TCanvas("c", "c", 800, 700)
    w = ROOT.RooWorkspace("fit", "fitting workspace")

    ds_names = names

    means = []
    means_err = []
    sigmas = []
    sigmas_err = []
    gammas = []
    gammas_err = []
    betas = []
    betas_err = []
    mus = []
    mus_err = []
    fracs = []
    fracs_err = []

    for ds, name, mass in zip(datasets, ds_names, massPoints):

        print(ds, name, mass)
        getattr(w, "import")(ds)
        gaus = w.factory(
            (
                f"Gaussian::gaus_{name}"
                f"(m, mean_{name}[50, 90], sigma_{name}[10, 40])"
            )
        )
        gamma = w.factory(
            (
                f"Gamma::gamma_{name}"
                f"(m, gam_{name}[1,10], beta_{name}[1,40], mu_{name}[-40,40])"
            )
        )
        combined = w.factory(
            (
                f"AddPdf::combined_{name}"
                f"(gaus_{name}, gamma_{name}, frac_{name}[0.2,0.05,1])"
            )
        )
        getattr(w, "import")(combined)
        m = w.var("m")

        r_start = 0
        r_end = 300

        fit_result = combined.fitTo(
            ds,
            ROOT.RooFit.Range(r_start, r_end),
            ROOT.RooFit.SumW2Error(True),
            ROOT.RooFit.Strategy(2),
            ROOT.RooFit.Save(),
            ROOT.RooFit.PrintLevel(2),
        )
        frac = w.var(f"frac_{name}").getValV()

        frame = m.frame()
        frame.GetXaxis().SetTitle("m^{reco}_{l+J/#psi} [GeV]")
        frame.SetMinimum(0)
        ds.plotOn(frame, ROOT.RooFit.Name("data"))
        combined.plotOn(frame, ROOT.RooFit.Name("combined"))
        gaus.plotOn(
            frame,
            ROOT.RooFit.Normalization(frac),
            ROOT.RooFit.LineColor(ROOT.kGreen),
            ROOT.RooFit.LineStyle(ROOT.kDashed),
            ROOT.RooFit.Name("gaus"),
        )
        gamma.plotOn(
            frame,
            ROOT.RooFit.Normalization(1 - frac),
            ROOT.RooFit.LineColor(ROOT.kRed),
            ROOT.RooFit.LineStyle(ROOT.kDashDotted),
            ROOT.RooFit.Name("gamma"),
        )
        c = ROOT.TCanvas("c", "c", 800, 700)
        c.cd()
        frame.Draw()

        if chi_squared:
            n = fit_result.floatParsFinal().getSize()
            value = frame.chiSquare("combined", "data", n)
            print(f"chi2 combined {name}", value, n)
            autils.DrawText(0.5, 0.73, f"#chi^{{2}}/dof = {value:.2f}", size=0.03)

        astyle.ATLASLabel(0.4, 0.87, "Simulation")
        autils.DrawText(0.5, 0.82, "#sqrt{s} = 13 TeV", size=0.03)

        mean = w.var(f"mean_{name}")
        sigma = w.var(f"sigma_{name}")
        gam = w.var(f"gam_{name}")
        beta = w.var(f"beta_{name}")
        mu = w.var(f"mu_{name}")
        frac = w.var(f"frac_{name}")

        means.append(mean.getValV())
        means_err.append(mean.getError())
        sigmas.append(sigma.getValV())
        sigmas_err.append(sigma.getError())
        gammas.append(gam.getValV())
        gammas_err.append(gam.getError())
        betas.append(beta.getValV())
        betas_err.append(beta.getError())
        mus.append(mu.getValV())
        mus_err.append(mu.getError())
        fracs.append(frac.getValV())
        fracs_err.append(frac.getError())

        legend = ROOT.TLegend(0.6, 0.6, 0.9, 0.7)
        legend.AddEntry("data", f"MC m_{{t}} = {mass} GeV Data", "p e")
        legend.AddEntry("combined", combined.GetTitle(), "l")
        legend.AddEntry("gaus", "Guassian PDF", "l")
        legend.AddEntry("gamma", "Gamma PDF", "l")

        legend.SetBorderSize(0)
        legend.SetFillColor(0)
        legend.SetTextSize(0.03)
        legend.Draw()

        c.Update()
        c.SaveAs(f"./plots/combined_fit_{name}.png")

    with open("combined_param.csv", "w") as f:
        writer = csv.writer(f)
        writer.writerow(
            [
                "mass",
                "mean",
                "mean_err",
                "sigma",
                "sigma_err",
                "gamma",
                "gamma_err",
                "beta",
                "beta_err",
                "mu",
                "mu_err",
                "frac",
                "frac_err",
            ]
        )
        writer.writerows(
            np.transpose(
                [
                    massPoints,
                    means,
                    means_err,
                    sigmas,
                    sigmas_err,
                    gammas,
                    gammas_err,
                    betas,
                    betas_err,
                    mus,
                    mus_err,
                    fracs,
                    fracs_err,
                ]
            )
        )

    del w, c


def fit_double_gaussian(
    datasets: List[ROOT.RooDataHist],
    ds_names: List[str],
    chi_squared: bool = True,
):
    w = ROOT.RooWorkspace("fit", "fitting workspace")

    means = []
    means_err = []
    sigmas = []
    sigmas_err = []
    means_0 = []
    means_0_err = []
    sigmas_0 = []
    sigmas_0_err = []
    fracs = []
    fracs_err = []

    for ds, name, mass in zip(datasets, ds_names, massPoints):

        getattr(w, "import")(ds)
        gaus = w.factory(
            (
                f"Gaussian::gaus_{name}"
                f"(m, mean_{name}[70, 50, 100], sigma_{name}[20, 5, 50])"
            )
        )
        gaus_0 = w.factory(
            (
                f"Gaussian::gaus_0_{name}"
                f"(m, mean_0_{name}[30,20, 50], sigma_0_{name}[10, 5, 40])"
            )
        )
        combined = w.factory(
            (
                f"AddPdf::gaus_com_{name}"
                f"(gaus_{name}, gaus_0_{name}, frac_{name}[0.9, 0.7, 1])"
            )
        )
        getattr(w, "import")(combined)
        m = w.var("m")

        r_start = 0
        r_end = 160

        fit_result = combined.fitTo(
            ds,
            ROOT.RooFit.Range(r_start, r_end),
            ROOT.RooFit.SumW2Error(True),
            ROOT.RooFit.Strategy(2),
            ROOT.RooFit.Save(),
            ROOT.RooFit.PrintLevel(3),
        )
        frac = w.var(f"frac_{name}").getValV()

        frame = m.frame()
        frame.GetXaxis().SetTitle("m^{reco}_{l+J/#psi} [GeV]")
        frame.SetMinimum(0)
        ds.plotOn(frame, ROOT.RooFit.Name("data"))
        combined.plotOn(frame, ROOT.RooFit.Name("combined"))
        gaus.plotOn(
            frame,
            ROOT.RooFit.Normalization(frac),
            ROOT.RooFit.LineColor(ROOT.kGreen),
            ROOT.RooFit.LineStyle(ROOT.kDashed),
            ROOT.RooFit.Name("gaus"),
        )
        gaus_0.plotOn(
            frame,
            ROOT.RooFit.Normalization(1 - frac),
            ROOT.RooFit.LineColor(ROOT.kRed),
            ROOT.RooFit.LineStyle(ROOT.kDashDotted),
            ROOT.RooFit.Name("gaus_0"),
        )
        c = ROOT.TCanvas("c", "c", 800, 700)
        c.cd()
        frame.Draw()

        if chi_squared:
            n = fit_result.floatParsFinal().getSize()
            value = frame.chiSquare("combined", "data", n)
            print(f"chi2 {name}", value, n)
            autils.DrawText(0.5, 0.65, f"#chi^{{2}}/dof = {value:.2f}", size=0.03)

        astyle.ATLASLabel(0.4, 0.87, "Simulation")
        autils.DrawText(0.5, 0.82, "#sqrt{s} = 13 TeV", size=0.03)

        frac = w.var(f"frac_{name}")
        fracs.append(frac.getValV())
        fracs_err.append(frac.getError())

        mean = w.var(f"mean_{name}")
        sigma = w.var(f"sigma_{name}")

        means.append(mean.getValV())
        means_err.append(mean.getError())
        sigmas.append(sigma.getValV())
        sigmas_err.append(sigma.getError())
        autils.DrawText(
            0.5,
            0.78,
            (
                f"#mu_1 = {mean.getValV():.2f}"
                f" #pm {mean.getError():.2f} (stat).\n"
            ),
            size=0.03,
        )
        autils.DrawText(
            0.5,
            0.75,
            (
                f"#sigma_1 = {sigma.getValV():.2f}"
                f" #pm {sigma.getError():.2f} (stat).\n"
            ),
            size=0.03,
        )

        mean_0 = w.var(f"mean_0_{name}")
        sigma_0 = w.var(f"sigma_0_{name}")

        means_0.append(mean_0.getValV())
        means_0_err.append(mean_0.getError())
        sigmas_0.append(sigma_0.getValV())
        sigmas_0_err.append(sigma_0.getError())
        autils.DrawText(
            0.5,
            0.72,
            (
                f"#mu_2 = {mean_0.getValV():.2f}"
                f" #pm {mean_0.getError():.2f} (stat).\n"
            ),
            size=0.03,
        )
        autils.DrawText(
            0.5,
            0.69,
            (
                f"#sigma_2 = {sigma_0.getValV():.2f}"
                f" #pm {sigma_0.getError():.2f} (stat).\n"
            ),
            size=0.03,
        )
        legend = ROOT.TLegend(0.55, 0.5, 0.9, 0.6)
        legend.AddEntry("data", f"MC m_{{t}} = {mass} GeV Data", "p l")
        legend.AddEntry("combined", gaus.GetTitle(), "l")
        legend.AddEntry("gaus", "Gaussian 1", "l")
        legend.AddEntry("gaus_0", "Gaussian 2", "l")
        legend.SetBorderSize(0)
        legend.SetFillColor(0)
        legend.SetTextSize(0.03)
        legend.Draw()

        c.Update()
        c.SaveAs(f"./plots/gaus_com_fit_{name}.png")

    with open("gaus_com_param.csv", "w") as f:
        writer = csv.writer(f)
        writer.writerow(
            [
                "mass",
                "mean",
                "mean_err",
                "sigma",
                "sigma_err",
                "mean_0",
                "mean_0_err",
                "sigma_0",
                "sigma_0_err",
                "frac",
                "frac_err",
            ]
        )
        writer.writerows(
            np.transpose(
                [
                    massPoints,
                    means,
                    means_err,
                    sigmas,
                    sigmas_err,
                    means_0,
                    means_0_err,
                    sigmas_0,
                    sigmas_0_err,
                    fracs,
                    fracs_err,
                ]
            )
        )

    del w, c
