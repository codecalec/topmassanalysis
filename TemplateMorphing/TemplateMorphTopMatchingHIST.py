import ROOT
import os
import errno
from atlasplots import atlas_style as astyle
from atlasplots import utils as autils

astyle.SetAtlasStyle()
ROOT.gROOT.SetBatch(ROOT.kTRUE)


# Define some useful functions
def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


# Take input list and load the tree that has the relevant information,
# checks the number of entries and returns the tree
def getTree(dsidList):
    fileList = []
    for dsid in dsidList:
        fileList.append(
            "/atlas/aveltman/DATAMC/TopMass/topParentNtuples/" + dsid + "/*"
        )
    tree = ROOT.TChain("nominal")
    for fileName in fileList:
        tree.Add(fileName)
    print(tree.GetEntries(), "entries in dsids", dsidList)
    return tree


# Take the event selection output and matches the lepton with the J/Psi
# that pass the selection criteria
def m3l(event):
    Ji = event.Jpsi_selected
    Jpt, Jeta, Jphi, Jm = (
        event.Jpsi_pt[Ji],
        event.Jpsi_eta[Ji],
        event.Jpsi_phi[Ji],
        event.Jpsi_m[Ji],
    )
    J4V = ROOT.TLorentzVector()
    J4V.SetPtEtaPhiM(Jpt, Jeta, Jphi, Jm)

    Li, Lpt, Leta, Lphi, Le = -1, 0.0, 0.0, 0.0, 0.0
    if event.electron_selected >= 0:
        Li = event.electron_selected
        Lpt, Leta, Lphi, Le = (
            event.electron_pt[Li],
            event.electron_eta[Li],
            event.electron_phi[Li],
            event.electron_E[Li],
        )
    elif event.muon_selected >= 0:
        Li = event.muon_selected
        Lpt, Leta, Lphi, Le = (
            event.muon_pt[Li],
            event.muon_eta[Li],
            event.muon_phi[Li],
            event.muon_E[Li],
        )
    else:
        print("bad!")
    L4V = ROOT.TLorentzVector()
    L4V.SetPtEtaPhiE(Lpt, Leta, Lphi, Le)
    return (J4V + L4V).M() * 0.001


# Take into account event weights and detector effects
def evtW(event):
    eventWeight = 1.0
    eventWeight *= (
        # event.mcWeightOrg
        event.jvtEventWeight
        * event.pileupEventWeight
        * event.MV2c10_77_EventWeight
        # / event.totalEventsWeighted
    )

    # Muons from Jpsi
    muonsFromJpsi = []
    for ji, (pt, decay_muons) in enumerate(
        zip(event.Jpsi_pt, event.Jpsi_muons)
    ):
        if ji == event.Jpsi_selected:
            for d in decay_muons:
                muonsFromJpsi.append(d)
    eventWeight *= (
        event.MU_SF_ID_LowPt[muonsFromJpsi[0]]
        * event.MU_SF_ID_LowPt[muonsFromJpsi[1]]
    )

    Li = -1
    if event.electron_selected >= 0:
        Li = event.electron_selected
        eventWeight *= (
            event.EL_SF_Trigger_TightLH[Li]
            * event.EL_SF_Reco[Li]
            * event.EL_SF_ID_TightLH[Li]
            * event.EL_SF_Iso_Gradient[Li]
        )
    elif event.muon_selected >= 0:
        Li = event.muon_selected
        eventWeight *= (
            event.MU_SF_Trigger_Medium[Li]
            * event.MU_SF_ID_Medium[Li]
            * event.MU_SF_TTVA[Li]
            * event.MU_SF_Isol_Gradient[Li]
        )
    else:
        print("bad!")
    return eventWeight


# Create histograms when plotting
def binHist(dataset, pdf, smooth, nBin, integral=-1):
    h_data = dataset.createHistogram("m", nBin)
    h_fit = pdf.createHistogram("m", nBin * smooth)
    if integral < 0:
        dataInt = 1
        fitInt = h_data.Integral() / h_fit.Integral() * smooth
    else:
        dataInt = integral / h_data.Integral()
        fitInt = integral / h_fit.Integral() * smooth

    h_data.Scale(dataInt)
    h_fit.Scale(fitInt)
    return h_data, h_fit, h_data.Integral()


def initialise_datasets(names, names_mm, names_total, trees):

    # Create empty datasets
    datasets = []
    datasets_mm = []
    datasets_total = []

    # Create datasets for each mass point
    for name, name_mm in zip(names, names_mm):
        datasets.append(ROOT.RooDataHist(name, name, ROOT.RooArgSet(m)))
        datasets_mm.append(
            ROOT.RooDataHist(name_mm, name_mm, ROOT.RooArgSet(m))
        )

    # Fill datasets accordingly
    for dataset, dataset_mm, tree in zip(datasets, datasets_mm, trees):
        count = 0
        count_mm = 0
        for event in tree:
            p = m3l(event)
            if mlo < p < mhi:
                lepton_parent = -2
                if event.electron_selected >= 0:
                    Li = event.electron_selected
                    if event.electron_truth_pdgId[Li] < 0:
                        lepton_parent = 1
                    else:
                        lepton_parent = -1
                elif event.muon_selected >= 0:
                    Li = event.muon_selected
                    if event.muon_truth_pdgId[Li] < 0:
                        lepton_parent = 1
                    else:
                        lepton_parent = -1
                else:
                    print("bad!")

                Ji = event.Jpsi_selected
                Jpsi_barcode = event.Jpsi_truthMatch_barcode[Ji]
                Jpsi_parent = -2
                for i, barcode in enumerate(event.truth_barcode):
                    if barcode == Jpsi_barcode:
                        Jpsi_topparent = bool(event.truth_topParent[i])
                        Jpsi_antitopparent = bool(event.truth_antiTopParent[i])
                        if Jpsi_topparent == Jpsi_antitopparent:
                            Jpsi_parent = -2
                        elif Jpsi_topparent == 1:
                            Jpsi_parent = 1
                        elif Jpsi_antitopparent == 1:
                            Jpsi_parent = -1
                        break

                m.setVal(p)
                if Jpsi_parent == lepton_parent:
                    count += 1
                    dataset.add(ROOT.RooArgSet(m), evtW(event))
                else:
                    count_mm += 1
                    dataset_mm.add(ROOT.RooArgSet(m), evtW(event))
        print(
            dataset, "Correct:", count, "Incorrect:", count_mm,
        )
    # Add correct pairing and incorrect paring to get combined
    for name_total, dataset, dataset_mm in zip(
        names_total, datasets, datasets_mm
    ):
        ds = dataset.Clone(name_total)
        ds.add(dataset_mm)
        datasets_total.append(ds)

    return datasets, datasets_mm, datasets_total


# Store plots in a folder
mkdir_p("./plots")

# Import the Monte Carlo datasets, and the ATLAS data. Define the TTrees.
tree169 = getTree(["mtop169"])
tree171 = getTree(["mtop171"])
tree172 = getTree(["mtop172"])
tree172p25 = getTree(["mtop172p25"])
tree172p5 = getTree(["mtop172p5"])
tree172p75 = getTree(["mtop172p75"])
tree173 = getTree(["mtop173"])
tree174 = getTree(["mtop174"])
tree176 = getTree(["mtop176"])
treeData = getTree(["data"])

# Setup a workspace
f = ROOT.TFile("workspace.root", "RECREATE")
mlo, mhi = 20, 200
w = ROOT.RooWorkspace()
m = w.factory("m[" + str(mlo) + "," + str(mhi) + "]")


trees = [
    tree169,
    tree171,
    tree172,
    tree172p25,
    tree172p5,
    tree172p75,
    tree173,
    tree174,
    tree176,
]

names = [
    "ds169",
    "ds171",
    "ds172",
    "ds172p25",
    "ds172p5",
    "ds172p75",
    "ds173",
    "ds174",
    "ds176",
]

names_mm = [
    "ds169_mm",
    "ds171_mm",
    "ds172_mm",
    "ds172p25_mm",
    "ds172p5_mm",
    "ds172p75_mm",
    "ds173_mm",
    "ds174_mm",
    "ds176_mm",
]
names_total = [
    "ds169_total",
    "ds171_total",
    "ds172_total",
    "ds172p25_total",
    "ds172p5_total",
    "ds172p75_total",
    "ds173_total",
    "ds174_total",
    "ds176_total",
]

massPoints = [
    169,
    171,
    172,
    172.25,
    172.75,
    173,
    174,
    176,
]

datasets, datasets_mm, datasets_total = initialise_datasets(
    names, names_mm, names_total, trees
)

# dont fit 172p5
ds172p5 = datasets.pop(len(datasets) // 2)
ds172p5_mm = datasets_mm.pop(len(datasets_mm) // 2)
ds172p5_total = datasets_total.pop(len(datasets_total) // 2)

names.pop(len(names) // 2)
names_mm.pop(len(names_mm) // 2)
names_total.pop(len(names_total) // 2)

# import datasets into RooWorkSpace
for ds, ds_mm, ds_total in zip(datasets, datasets_mm, datasets_total):
    getattr(w, "import")(ds)
    getattr(w, "import")(ds_mm)
    getattr(w, "import")(ds_total)

pdfList = ROOT.RooArgList()
pdfList_mm = ROOT.RooArgList()
pdfList_total = ROOT.RooArgList()

w.Print()

# create pdfs from DataHists
for mass in names:
    pdf = w.factory("HistPdf::template{mass}(m,{mass})".format(mass=mass))
    pdfList.add(pdf)

for mass in names_mm:
    pdf = w.factory(
        "HistPdf::template_mm{mass}(m,{mass})".format(mass=mass)
    )
    pdfList_mm.add(pdf)

for mass in names_total:
    pdf = w.factory(
        "HistPdf::template_total{mass}(m,{mass})".format(mass=mass)
    )
    pdfList_total.add(pdf)

# Create the morph objects
lo, hi = 169.0, 176.0
mu = w.factory("mu[{},{}]".format(lo, hi))
mu_mm = w.factory("mu_mm[{},{}]".format(lo, hi))
mu_total = w.factory("mu_total[{},{}]".format(lo, hi))

# Create Vector with each mass point for morphing
paramVec = ROOT.TVectorD(len(massPoints))
for i, mass in enumerate(massPoints):
    paramVec[i] = mass

c = ROOT.TCanvas("c", "c", 900, 700)


# Matched data moment morph fitting
morph = ROOT.RooMomentMorph(
    "RMM",
    "RMM",
    mu,
    ROOT.RooArgList(m),
    pdfList,
    paramVec,
    ROOT.RooMomentMorph.Linear,
)
getattr(w, "import")(morph)

morph.fitTo(
    ds172p5,
    ROOT.RooFit.Hesse(True),
    # ROOT.RooFit.Minos(True),
    ROOT.RooFit.SumW2Error(True),
    # ROOT.RooFit.Offset(True),
    ROOT.RooFit.Strategy(2),
    ROOT.RooFit.Save(),
    ROOT.RooFit.PrintLevel(2),
)
h_ds172p5, h_pdf172p5, integral172p5 = binHist(ds172p5, morph, 250, 25)

h_ds172p5.GetXaxis().SetTitle("m_{l+J/#psi} [GeV]")
h_ds172p5.GetYaxis().SetTitle("Events")
h_ds172p5.SetMaximum(h_ds172p5.GetMaximum() * 1.3)
h_ds172p5.Draw("p")
h_pdf172p5.Draw("hist same")

astyle.ATLASLabel(0.4, 0.87, "Work In Progress")
autils.DrawText(0.5, 0.82, "#sqrt{s} = 13 TeV", size=0.03)
result = "m_{{t}} = {0:.2f} +/- {1:.2f} (stat).\n".format(
    mu.getValV(), mu.getError()
)

autils.DrawText(0.5, 0.77, result, size=0.03)
legend = ROOT.TLegend(0.6, 0.6, 0.9, 0.7)
legend.AddEntry(h_ds172p5, "MC m_{t} = 172.5 GeV data", "p l")
legend.AddEntry(h_pdf172p5, "Morph best Fit", "l")
legend.SetBorderSize(0)
legend.SetFillColor(0)
legend.SetTextSize(0.03)
legend.Draw()

c.Update()
c.SaveAs("./plots/morph_match_data_result.png")
f.Write()


# Mismatched data moment morph fitting
morph_mm = ROOT.RooMomentMorph(
    "RMM_mm",
    "RMM_mm",
    mu_mm,
    ROOT.RooArgList(m),
    pdfList_mm,
    paramVec,
    ROOT.RooMomentMorph.Linear,
)
getattr(w, "import")(morph_mm)

morph_mm.fitTo(
    ds172p5_mm,
    ROOT.RooFit.Hesse(True),
    # ROOT.RooFit.Minos(True),
    ROOT.RooFit.SumW2Error(True),
    # ROOT.RooFit.Offset(True),
    ROOT.RooFit.Strategy(2),
    ROOT.RooFit.Save(),
    ROOT.RooFit.PrintLevel(2),
)
h_ds172p5_mm, h_pdf172p5_mm, integral172p5 = binHist(
    ds172p5_mm, morph_mm, 250, 25
)

h_ds172p5_mm.GetXaxis().SetTitle("m_{l+J/#psi} [GeV]")
h_ds172p5_mm.GetYaxis().SetTitle("Events")
h_ds172p5_mm.SetMaximum(h_ds172p5_mm.GetMaximum() * 1.3)
h_ds172p5_mm.Draw("p")
h_pdf172p5_mm.Draw("hist same")

astyle.ATLASLabel(0.4, 0.87, "Work In Progress")
autils.DrawText(0.5, 0.82, "#sqrt{s} = 13 TeV", size=0.03)
result = "m_{{t}} = {0:.2f} +/- {1:.2f} (stat).\n".format(
    mu_mm.getValV(), mu_mm.getError()
)
# result = '#Delta m_{{t}} = {0:.2f} (stat).\n'.format(mu.getError())
autils.DrawText(0.5, 0.77, result, size=0.03)
legend = ROOT.TLegend(0.6, 0.6, 0.9, 0.7)
legend.AddEntry(h_ds172p5_mm, "MC m_{t} = 172.5 GeV data", "p l")
legend.AddEntry(h_pdf172p5_mm, "Morph best Fit", "l")
legend.SetBorderSize(0)
legend.SetFillColor(0)
legend.SetTextSize(0.03)
legend.Draw()

c.Update()
c.SaveAs("./plots/morph_mismatch_data_result.png")

f.Write()


# Total data moment morph fitting
morph_total = ROOT.RooMomentMorph(
    "RMM_total",
    "RMM_total",
    mu_total,
    ROOT.RooArgList(m),
    pdfList_total,
    paramVec,
    ROOT.RooMomentMorph.Linear,
)
getattr(w, "import")(morph_total)

# Morph to data
# Mismatched data

morph_total.fitTo(
    ds172p5_total,
    ROOT.RooFit.Hesse(True),
    # ROOT.RooFit.Minos(True),
    ROOT.RooFit.SumW2Error(True),
    # ROOT.RooFit.Offset(True),
    ROOT.RooFit.Strategy(2),
    ROOT.RooFit.Save(),
    ROOT.RooFit.PrintLevel(2),
)
h_ds172p5_total, h_pdf172p5_total, integral172p5 = binHist(
    ds172p5_total, morph_total, 250, 25
)

h_ds172p5_total.GetXaxis().SetTitle("m_{l+J/#psi} [GeV]")
h_ds172p5_total.GetYaxis().SetTitle("Events")
h_ds172p5_total.SetMaximum(h_ds172p5_total.GetMaximum() * 1.3)
h_ds172p5_total.Draw("p")
h_pdf172p5_total.Draw("hist same")

astyle.ATLASLabel(0.4, 0.87, "Work In Progress")
autils.DrawText(0.5, 0.82, "#sqrt{s} = 13 TeV", size=0.03)
result = "m_{{t}} = {0:.2f} +/- {1:.2f} (stat).\n".format(
    mu_total.getValV(), mu_total.getError()
)
autils.DrawText(0.5, 0.77, result, size=0.03)
legend = ROOT.TLegend(0.6, 0.6, 0.9, 0.7)
legend.AddEntry(h_ds172p5_total, "MC m_{t} = 172.5 GeV data", "p l")
legend.AddEntry(h_pdf172p5_total, "Morph best Fit", "l")
legend.SetBorderSize(0)
legend.SetFillColor(0)
legend.SetTextSize(0.03)
legend.Draw()

c.Update()
c.SaveAs("./plots/morph_total_data_result.png")
f.Write()

w.writeToFile("workspace.root")


def chi2_template(data_hist, morphing_pdf, masspoints, morphing_var):

    c1 = ROOT.TCanvas("c1", "c1", 800, 700)
    values = []
    for mass in masspoints:
        morphing_var.setVal(mass)
        chi2var = ROOT.RooChi2Var(
            "chi2_var" + str(mass),
            "chi2_var" + str(mass),
            morphing_pdf,
            data_hist,
        )
        chi2 = chi2var.getVal()
        dof = data_hist.numEntries()
        value = chi2 / dof
        print(str(mass), chi2, dof, value)
        values.append(value)

    x_vec = ROOT.TVectorD(len(masspoints))
    y_vec = ROOT.TVectorD(len(values))
    for i, (mass, value) in enumerate(zip(masspoints, values)):
        x_vec[i] = mass
        y_vec[i] = value

    graph = ROOT.TGraph(x_vec, y_vec)
    parabola = ROOT.TF1(
        "para_" + data_hist.GetName(), "[0]*(x - [1])^2 + [2]", 169, 176,
    )

    result = graph.Fit(parabola, "S")
    graph.GetXaxis().SetTitle("m_{l+J/#psi} [GeV]")
    graph.GetYaxis().SetTitle("\\chi^2/dof")
    graph.Draw()
    graph.SetMaximum(8)
    graph.SetMinimum(0)

    caption = "f(x) = {0:.1f}(x - {1:.1f}) + {2:.1f}\n".format(
        result.Parameter(0), result.Parameter(1), result.Parameter(2)
    )
    legend = ROOT.TLegend(0.6, 0.6, 0.9, 0.7)
    legend.AddEntry(graph, caption, "p l")
    legend.SetBorderSize(0)
    legend.SetFillColor(0)
    legend.SetTextSize(0.03)
    legend.Draw()
    c1.Update()
    c1.SaveAs("./plots/chi_square_template_" + data_hist.GetName() + ".png")
    f.Write()

    print(result.Parameter(0), result.Parameter(1), result.Parameter(2))


def chi2_dof(data_hist, histpdfs, masspoints):

    c1 = ROOT.TCanvas("c1", "c1", 800, 700)
    values = []
    for i, mass in enumerate(masspoints):
        chi2var = ROOT.RooChi2Var(
            "chi2_" + str(mass), "chi2_" + str(mass), histpdfs[i], data_hist
        )
        chi2 = chi2var.getVal()
        dof = data_hist.numEntries()
        value = chi2 / dof
        print(str(mass), chi2, dof, value)
        values.append(value)

    x_vec = ROOT.TVectorD(len(masspoints))
    y_vec = ROOT.TVectorD(len(values))
    for i, (mass, value) in enumerate(zip(masspoints, values)):
        x_vec[i] = mass
        y_vec[i] = value

    graph = ROOT.TGraph(x_vec, y_vec)
    parabola = ROOT.TF1(
        "para_" + data_hist.GetName(), "[0]*(x - [1])^2 + [2]", 169, 176,
    )
    parabola.SetParLimits(1, 169, 176)

    result = graph.Fit(parabola, "S")
    graph.GetXaxis().SetTitle("m_{l+J/#psi} [GeV]")
    graph.GetYaxis().SetTitle("\\chi^2/dof")
    graph.Draw()
    graph.SetMaximum(8)
    graph.SetMinimum(0)

    caption = "f(x) = {0:.1f}(x - ({1:.1f})) + {2:.1f}\n".format(
        result.Parameter(0), result.Parameter(1), result.Parameter(2)
    )
    legend = ROOT.TLegend(0.3, 0.7, 0.6, 0.8)
    legend.AddEntry(graph, caption, "p l")
    autils.DrawText(
        0.5,
        0.82,
        (
            f"m_t = {result.Parameter(1):.1f} \\pm {result.Error(1):.1f}\n"
            f"\\chi^2/dof = {result.Chi2()/len(masspoints):.2f}\n"
        ),
        size=0.03
    )
    legend.SetBorderSize(0)
    legend.SetFillColor(0)
    legend.SetTextSize(0.03)
    legend.Draw()
    c1.Update()
    c1.SaveAs("./plots/chi_square_" + data_hist.GetName() + ".png")
    f.Write()

    print(result.Parameter(0), result.Parameter(1), result.Parameter(2))
    return graph


# template_masses = np.linspace(169, 176, 30)
# chi2_template(ds172p5, morph, template_masses, mu)
# chi2_template(ds172p5_mm, morph_mm, template_masses, mu_mm)
# chi2_template(ds172p5_total, morph_total, template_masses, mu_total)


template_hist = ROOT.RooDataHist(
    "template_hist", "template_hist", ROOT.RooArgList(m), h_pdf172p5
)
template_hist_mm = ROOT.RooDataHist(
    "template_hist_mm", "template_hist_mm", ROOT.RooArgList(m), h_pdf172p5_mm
)
template_hist_total = ROOT.RooDataHist(
    "template_hist_total",
    "template_hist_total",
    ROOT.RooArgList(m),
    h_pdf172p5_total,
)


g = chi2_dof(ds172p5, pdfList, massPoints)
g_mm = chi2_dof(ds172p5_mm, pdfList_mm, massPoints)
g_total = chi2_dof(ds172p5_total, pdfList_total, massPoints)

f = g.GetFunction("para_" + ds172p5.GetName())
f.SetLineColor(ROOT.kBlue)

f_mm = g_mm.GetFunction("para_" + ds172p5_mm.GetName())
f_mm.SetLineColor(ROOT.kRed)

f_total = g_total.GetFunction("para_" + ds172p5_total.GetName())


c2 = ROOT.TCanvas("c2", "c2", 800, 700)
f.GetXaxis().SetTitle("m_{l+J/#psi} [GeV]")
f.GetYaxis().SetTitle("\\chi^2/dof")
f.SetMaximum(8)
f.SetMinimum(0)
f.Draw()
f_mm.Draw("same")
f_total.Draw("same")

legend = ROOT.TLegend(0.3, 0.6, 0.6, 0.7)
legend.AddEntry(f, "Correctly Matched", "l")
legend.AddEntry(f_mm, "Incorrectly Matched", "l")
legend.AddEntry(f_total, "Total Events", "l")
legend.SetBorderSize(0)
legend.SetFillColor(0)
legend.SetTextSize(0.03)
legend.Draw()
c2.Update()
c2.SaveAs("./plots/test.png")
