import matplotlib.pyplot as plt
import numpy as np

def significance(N_total, mass_err=0.20, N_fit=132133):
    return mass_err * np.sqrt(N_fit) / np.sqrt(N_total)

def significance_truth(N_total, mass_err=0.17, N_fit=66922, epsilon=66922/132133):
    return mass_err * np.sqrt(N_fit) / np.sqrt(epsilon * N_total)

def significance_ml(N_total, mass_err=0.29, N_fit=67464, epsilon=0.553):
    return mass_err * np.sqrt(N_fit) / np.sqrt(epsilon * N_total)


if __name__ == "__main__":
    plt.style.use(["science", "no-latex"])
    N_total = np.linspace(1000, 200000, 10000)

    s = significance(N_total)
    s_truth = significance_truth(N_total)
    s_ml = significance_ml(N_total)

    plt.ylim(bottom=0)
    plt.figure()
    plt.plot(N_total, s, label="No Selectrion")
    plt.plot(N_total, s_truth, label = "Truth Selection")
    plt.plot(N_total, s_ml, label="ML Selection")
    plt.plot([min(N_total), max(N_total)], [0.3, 0.3], color="grey", linestyle="dotted", label="Current Measurement")
    plt.ylabel("Projected Statistical Uncertainty [GeV]")
    plt.xlabel("$N_{\\mathrm{total}}$")
    plt.legend()
    plt.savefig("./plots/significance.pdf")

