import ROOT
import os
from typing import List
from atlasplots import atlas_style as astyle
from atlasplots import utils as autils

os.environ["KERAS_BACKEND"] = "theano"
import keras  # noqa: E402

astyle.SetAtlasStyle()
ROOT.gROOT.SetBatch(ROOT.kTRUE)


# Take input list and load the tree that has the relevant information,
# checks the number of entries and returns the tree
def getTree(dsidList: List[str]) -> ROOT.TChain:
    fileList = []
    for dsid in dsidList:
        fileList.append(
            "/atlas/aveltman/DATAMC/TopMass/topParentNtuples/" + dsid + "/*"
        )
    tree = ROOT.TChain("nominal")
    for fileName in fileList:
        tree.Add(fileName)
    print(tree.GetEntries(), "entries in dsids", dsidList)
    return tree


# Take the event selection output and matches the lepton with the J/Psi
# that pass the selection criteria
def m3l(event: ROOT.TChain) -> float:
    Ji = event.Jpsi_selected
    Jpt, Jeta, Jphi, Jm = (
        event.Jpsi_pt[Ji],
        event.Jpsi_eta[Ji],
        event.Jpsi_phi[Ji],
        event.Jpsi_m[Ji],
    )
    J4V = ROOT.TLorentzVector()
    J4V.SetPtEtaPhiM(Jpt, Jeta, Jphi, Jm)

    Li, Lpt, Leta, Lphi, Le = -1, 0.0, 0.0, 0.0, 0.0
    if event.electron_selected >= 0:
        Li = event.electron_selected
        Lpt, Leta, Lphi, Le = (
            event.electron_pt[Li],
            event.electron_eta[Li],
            event.electron_phi[Li],
            event.electron_E[Li],
        )
    elif event.muon_selected >= 0:
        Li = event.muon_selected
        Lpt, Leta, Lphi, Le = (
            event.muon_pt[Li],
            event.muon_eta[Li],
            event.muon_phi[Li],
            event.muon_E[Li],
        )
    else:
        print("bad!")
    L4V = ROOT.TLorentzVector()
    L4V.SetPtEtaPhiE(Lpt, Leta, Lphi, Le)
    return (J4V + L4V).M() * 0.001


# Take into account event weights and detector effects
def evtW(event: ROOT.TChain) -> float:
    eventWeight = 1.0
    eventWeight *= (
        # event.mcWeightOrg
        event.jvtEventWeight
        * event.pileupEventWeight
        * event.MV2c10_77_EventWeight
        # / event.totalEventsWeighted
    )

    # Muons from Jpsi
    muonsFromJpsi = []
    for ji, (pt, decay_muons) in enumerate(
        zip(event.Jpsi_pt, event.Jpsi_muons)
    ):
        if ji == event.Jpsi_selected:
            for d in decay_muons:
                muonsFromJpsi.append(d)
    eventWeight *= (
        event.MU_SF_ID_LowPt[muonsFromJpsi[0]]
        * event.MU_SF_ID_LowPt[muonsFromJpsi[1]]
    )

    Li = -1
    if event.electron_selected >= 0:
        Li = event.electron_selected
        eventWeight *= (
            event.EL_SF_Trigger_TightLH[Li]
            * event.EL_SF_Reco[Li]
            * event.EL_SF_ID_TightLH[Li]
            * event.EL_SF_Iso_Gradient[Li]
        )
    elif event.muon_selected >= 0:
        Li = event.muon_selected
        eventWeight *= (
            event.MU_SF_Trigger_Medium[Li]
            * event.MU_SF_ID_Medium[Li]
            * event.MU_SF_TTVA[Li]
            * event.MU_SF_Isol_Gradient[Li]
        )
    else:
        print("bad!")
    return eventWeight


# Create histograms when plotting
def binHist(
    dataset: ROOT.RooDataHist,
    pdf: ROOT.RooAbsPdf,
    smooth: int,
    nBin: int,
    integral: int = -1,
) -> (ROOT.TH1F, ROOT.TH1F, float):
    h_data = dataset.createHistogram("m", nBin)
    h_fit = pdf.createHistogram("m", nBin * smooth)
    if integral < 0:
        dataInt = 1
        fitInt = h_data.Integral() / h_fit.Integral() * smooth
    else:
        dataInt = integral / h_data.Integral()
        fitInt = integral / h_fit.Integral() * smooth

    h_data.Scale(dataInt)
    h_fit.Scale(fitInt)
    return h_data, h_fit, h_data.Integral()


def determine_matching_from_model(
    event: ROOT.TChain, el_model: keras.Model, mu_model: keras.Model
) -> bool:
    from numpy import sqrt, array
    from sklearn.preprocessing import MinMaxScaler

    values = []
    Ji = event.Jpsi_selected
    if event.electron_selected >= 0:
        model = el_model
        Li = event.electron_selected
        values.append(event.electron_pt[Li])
        values.append(event.electron_eta[Li])

        num_of_electrons = len(event.electron_pt)
        values.append(num_of_electrons)
        dphi = event.Jpsi_phi[Ji] - event.electron_phi[Li]
        deta = event.Jpsi_eta[Ji] - event.electron_eta[Li]
        dphi_met = event.met_phi - event.electron_phi[Li]
    elif event.muon_selected >= 0:
        model = mu_model
        Li = event.electron_selected
        values.append(event.muon_pt[Li])
        values.append(event.muon_eta[Li])

        num_of_muons = len(event.muon_pt) - 2
        values.append(num_of_muons)
        dphi = event.Jpsi_phi[Ji] - event.muon_phi[Li]
        deta = event.Jpsi_eta[Ji] - event.muon_eta[Li]
        dphi_met = event.met_phi - event.muon_phi[Li]
    else:
        print("bad!")
    dR = sqrt(dphi ** 2 + deta ** 2)

    values.append(event.Jpsi_pt[Ji])
    # values.append(event.Jpsi_phi[Ji]) # Removed Feature
    values.append(event.Jpsi_eta[Ji])
    values.append(event.met_met)
    values.append(event.met_phi)
    values.append(len(event.m_jet_pt))  # num_of_jets
    values.append(dphi)
    values.append(deta)
    values.append(dR)
    values.append(dphi_met)
    X = array([values])
    scaler = MinMaxScaler()
    Y = scaler.fit_transform(X.reshape(12, -1))
    Y = Y.reshape(-1, 12)
    return model.predict_classes(Y)


def initialise_datasets(
    names: List[str],
    names_mm: List[str],
    names_total: List[str],
    trees: List[ROOT.TChain],
    m: ROOT.RooRealVar,
) -> (List[ROOT.RooDataHist], List[ROOT.RooDataHist]):

    # Create empty datasets
    datasets = []
    datasets_mm = []
    datasets_total = []

    mlo = 0
    mhi = 300

    # Create datasets for each mass point
    for name, name_mm in zip(names, names_mm):
        datasets.append(ROOT.RooDataHist(name, name, ROOT.RooArgSet(m)))
        datasets_mm.append(
            ROOT.RooDataHist(name_mm, name_mm, ROOT.RooArgSet(m))
        )

    # Fill datasets accordingly
    for dataset, dataset_mm, tree in zip(datasets, datasets_mm, trees):
        count = 0
        count_mm = 0
        for event in tree:
            p = m3l(event)
            if mlo < p < mhi:
                lepton_parent = -2
                if event.electron_selected >= 0:
                    Li = event.electron_selected
                    if event.electron_truth_pdgId[Li] < 0:
                        lepton_parent = 1
                    else:
                        lepton_parent = -1
                elif event.muon_selected >= 0:
                    Li = event.muon_selected
                    if event.muon_truth_pdgId[Li] < 0:
                        lepton_parent = 1
                    else:
                        lepton_parent = -1
                else:
                    print("bad!")

                Ji = event.Jpsi_selected
                Jpsi_barcode = event.Jpsi_truthMatch_barcode[Ji]
                Jpsi_parent = -2
                for i, barcode in enumerate(event.truth_barcode):
                    if barcode == Jpsi_barcode:
                        Jpsi_topparent = bool(event.truth_topParent[i])
                        Jpsi_antitopparent = bool(event.truth_antiTopParent[i])
                        if Jpsi_topparent == Jpsi_antitopparent:
                            Jpsi_parent = -2
                        elif Jpsi_topparent == 1:
                            Jpsi_parent = 1
                        elif Jpsi_antitopparent == 1:
                            Jpsi_parent = -1
                        break

                m.setVal(p)
                if Jpsi_parent == lepton_parent:
                    count += 1
                    dataset.add(ROOT.RooArgSet(m), evtW(event))
                else:
                    count_mm += 1
                    dataset_mm.add(ROOT.RooArgSet(m), evtW(event))
        print(
            dataset,
            "Correct:",
            count,
            "Incorrect:",
            count_mm,
            "Total:",
            (count+count_mm)
        )
    # Add correct pairing and incorrect paring to get combined
    for name_total, dataset, dataset_mm in zip(
        names_total, datasets, datasets_mm
    ):
        ds = dataset.Clone(name_total)
        ds.add(dataset_mm)
        datasets_total.append(ds)

    return datasets, datasets_mm, datasets_total


def initialise_datasets_ml(
    names: List[str],
    names_mm: List[str],
    names_total: List[str],
    trees: List[ROOT.TChain],
    m: ROOT.RooRealVar,
    el_model_path: str,
    mu_model_path: str,
) -> (List[ROOT.RooDataHist], List[ROOT.RooDataHist]):

    # Create empty datasets
    datasets = []
    datasets_mm = []
    datasets_total = []

    mlo = 0
    mhi = 300

    el_model = keras.models.load_model(el_model_path)
    mu_model = keras.models.load_model(mu_model_path)

    print(el_model.summary())
    print(mu_model.summary())

    # Create datasets for each mass point
    for name, name_mm in zip(names, names_mm):
        datasets.append(ROOT.RooDataHist(name, name, ROOT.RooArgSet(m)))
        datasets_mm.append(
            ROOT.RooDataHist(name_mm, name_mm, ROOT.RooArgSet(m))
        )

    # Fill datasets accordingly
    for dataset, dataset_mm, tree in zip(datasets, datasets_mm, trees):
        count = 0
        count_mm = 0
        for event in tree:
            p = m3l(event)
            if mlo < p < mhi:
                m.setVal(p)
                if determine_matching_from_model(event, el_model, mu_model):
                    count += 1
                    dataset.add(ROOT.RooArgSet(m), evtW(event))
                else:
                    count_mm += 1
                    dataset_mm.add(ROOT.RooArgSet(m), evtW(event))
        print(
            dataset,
            "Correct:",
            count,
            "Incorrect:",
            count_mm,
        )
    # Add correct pairing and incorrect paring to get combined
    for name_total, dataset, dataset_mm in zip(
        names_total, datasets, datasets_mm
    ):
        ds = dataset.Clone(name_total)
        ds.add(dataset_mm)
        datasets_total.append(ds)

    return datasets, datasets_mm, datasets_total


tree169 = getTree(["mtop169"])
tree171 = getTree(["mtop171"])
tree172 = getTree(["mtop172"])
tree172p25 = getTree(["mtop172p25"])
tree172p5 = getTree(["mtop172p5"])
tree172p75 = getTree(["mtop172p75"])
tree173 = getTree(["mtop173"])
tree174 = getTree(["mtop174"])
tree176 = getTree(["mtop176"])
treeData = getTree(["data"])

trees = [
    tree169,
    tree171,
    tree172,
    tree172p25,
    tree172p5,
    tree172p75,
    tree173,
    tree174,
    tree176,
]

names = [
    "ds169",
    "ds171",
    "ds172",
    "ds172p25",
    "ds172p5",
    "ds172p75",
    "ds173",
    "ds174",
    "ds176",
]

names_mm = [
    "ds169_mm",
    "ds171_mm",
    "ds172_mm",
    "ds172p25_mm",
    "ds172p5_mm",
    "ds172p75_mm",
    "ds173_mm",
    "ds174_mm",
    "ds176_mm",
]
names_total = [
    "ds169_total",
    "ds171_total",
    "ds172_total",
    "ds172p25_total",
    "ds172p5_total",
    "ds172p75_total",
    "ds173_total",
    "ds174_total",
    "ds176_total",
]

massPoints = [
    169,
    171,
    172,
    172.25,
    172.5,
    172.75,
    173,
    174,
    176,
]


def save_datasets(datasets, filename: str):
    w = ROOT.RooWorkspace("w", "workspace")
    for ds in datasets:
        getattr(w, "import")(ds, ROOT.RooFit.Rename(ds.GetName()))
    w.writeToFile(filename)
    del w


def retrieve_datasets(
    filename: str, w: ROOT.RooWorkspace, use_ml: bool = False
) -> (List[ROOT.RooDataHist], List[ROOT.RooDataHist]):
    datasets = []
    datasets_mm = []
    datasets_total = []

    if os.path.isfile(filename):
        for name in names:
            getattr(w, "import")(f"{filename}:w:{name}")
            datasets.append(w.data(name))

        for name in names_mm:
            getattr(w, "import")(f"{filename}:w:{name}")
            datasets_mm.append(w.data(name))

        for name_total, dataset, dataset_mm in zip(
            names_total, datasets, datasets_mm
        ):
            ds = dataset.Clone(name_total)
            ds.add(dataset_mm)
            datasets_total.append(ds)

    else:
        mlo, mhi = 0, 300
        m = ROOT.RooRealVar("m", "Trilepton Mass", mlo, mhi)
        if use_ml:
            datasets, datasets_mm, datasets_total = initialise_datasets_ml(
                names,
                names_mm,
                names_total,
                trees,
                m,
                "../../LeptonMatchingModel/Electron Model.h5",
                "../../LeptonMatchingModel/Muon Model.h5",
            )
        else:
            datasets, datasets_mm, datasets_total = initialise_datasets(
                names,
                names_mm,
                names_total,
                trees,
                m,
            )

        save_datasets(datasets + datasets_mm, filename)
    return datasets, datasets_mm, datasets_total


def pdf_fitting(use_ml: bool = False, has_selection=True):
    from fitting import (
        fit_gaussian,
        fit_combined,
        fit_CB,
        fit_double_gaussian,
        fit_gamma,
    )

    dataset_file = (
        "sorted_datasets_ml.root" if use_ml else "sorted_datasets.root"
    )
    w = ROOT.RooWorkspace("w", "workspace")
    datasets, datasets_mm, datasets_total = retrieve_datasets(
        dataset_file, w, use_ml
    )
    fit_gaussian(datasets)
    fit_gamma(datasets_mm)
    fit_combined(datasets_mm, names_mm)
    fit_double_gaussian(datasets, names)
    fit_CB(datasets)
    fit_combined(datasets, names) if has_selection else fit_combined(
        datasets_total, names_total
    )
    del w


def construct_pdf(
    w: ROOT.RooWorkspace,
    observable: ROOT.RooRealVar,
    mean_results: List[float],
    sigma_results: List[float],
):

    mtop = ROOT.RooRealVar("mtop", "mtop", 172.5, 160, 180)
    grad_mean = ROOT.RooRealVar(
        "g_m", "Gradient of Mean model", mean_results[0]
    )
    grad_mean.setError(mean_results[1])
    intercept_mean = ROOT.RooRealVar(
        "i_m", "Intercept of Mean model", mean_results[2]
    )
    intercept_mean.setError(mean_results[3])

    grad_sigma = ROOT.RooRealVar(
        "g_s", "Gradient of sigma model", sigma_results[0]
    )
    grad_sigma.setError(sigma_results[1])
    intercept_sigma = ROOT.RooRealVar(
        "i_s", "Intercept of sigma model", sigma_results[2]
    )
    intercept_sigma.setError(sigma_results[3])

    mean = ROOT.RooFormulaVar(
        "mean_model",
        "@1 * @0 + @2",
        ROOT.RooArgList(mtop, grad_mean, intercept_mean),
    )
    sigma = ROOT.RooFormulaVar(
        "sigma_model",
        "@1 * @0 + @2",
        ROOT.RooArgList(mtop, grad_sigma, intercept_sigma),
    )

    pdf = ROOT.RooGaussian("model_gaus", "model_gaus", observable, mean, sigma)
    getattr(w, "import")(pdf)


def construct_pdf_2gaus(
    w: ROOT.RooWorkspace,
    observable: ROOT.RooRealVar,
    mean_results: List[float],
    mean_0_results: List[float],
    sigma_results: List[float],
    sigma_0_results: List[float],
    frac_results: List[float],
):
    mtop = ROOT.RooRealVar("mtop", "mtop", 172.5, 160, 180)
    param_names = [
        "m_g",
        "m_i",
        "s_g",
        "s_i",
        "m0_g",
        "m0_i",
        "s0_g",
        "s0_i",
        "frac_g",
        "frac_i",
    ]
    param_titles = [
        "Gradient of Primary Mean Model",
        "Intercept of Primary Mean Model",
        "Gradient of Primary Sigma Model",
        "Intercept of Primary Sigma Model",
        "Gradient of Secondary Mean Model",
        "Intercept of Secondary Mean Model",
        "Gradient of Secondary Sigma Model",
        "Intercept of Seconday Sigma Model",
        "Gradient of Fraction Model",
        "Intercept of Fraction Model",
    ]
    values = (
        mean_results
        + mean_0_results
        + sigma_results
        + sigma_0_results
        + frac_results
    )[0::2]
    errors = (
        mean_results
        + mean_0_results
        + sigma_results
        + sigma_0_results
        + frac_results
    )[1::2]

    real_vars = [
        ROOT.RooRealVar(name, title, value)
        for (name, title, value) in zip(param_names, param_titles, values)
    ]
    for (var, error) in zip(real_vars, errors):
        var.setError(error)

    formulae_names = [
        "mean_model",
        "sigma_model",
        "mean_0_model",
        "sigma_0_model",
        "frac_model",
    ]
    formulae = [
        ROOT.RooFormulaVar(
            name, "@1 * @0 + @2", ROOT.RooArgList(mtop, grad, intercept)
        )
        for (name, grad, intercept) in zip(
            formulae_names, real_vars[0::2], real_vars[1::2]
        )
    ]

    gaus1 = ROOT.RooGaussian(
        "gaus_1", "gaus_1", observable, formulae[0], formulae[1]
    )
    gaus2 = ROOT.RooGaussian(
        "gaus_2", "gaus_2", observable, formulae[2], formulae[3]
    )

    pdf = ROOT.RooAddPdf(
        "model_2gaus", "model_2gaus", gaus1, gaus2, formulae[4]
    )
    getattr(w, "import")(pdf)


def construct_pdf_combined(
    w: ROOT.RooWorkspace,
    observable: ROOT.RooRealVar,
    mean_results: List[float],
    sigma_results: List[float],
    gamma_results: List[float],
    beta_results: List[float],
    mu_results: List[float],
    frac_results: List[float],
):
    mtop = ROOT.RooRealVar("mtop", "mtop", 172.5, 160, 180)
    param_names = [
        "m_g",
        "m_i",
        "s_g",
        "s_i",
        "gamma_g",
        "gamma_i",
        "beta_g",
        "beta_i",
        "mu_g",
        "mu_i",
        "frac_g",
        "frac_i",
    ]
    param_titles = [
        "Gradient of Mean Model",
        "Intercept of Mean Model",
        "Gradient of Sigma Model",
        "Intercept of Sigma Model",
        "Gradient of Gamma Model",
        "Intercept of Gamma Model",
        "Gradient of Beta Model",
        "Intercept of Beta Model",
        "Gradient of Mu Model",
        "Intercept of Mu Model",
        "Gradient of Fraction Model",
        "Intercept of Fraction Model",
    ]

    values = (
        mean_results
        + sigma_results
        + gamma_results
        + beta_results
        + mu_results
        + frac_results
    )[0::2]
    errors = (
        mean_results
        + sigma_results
        + gamma_results
        + beta_results
        + mu_results
        + frac_results
    )[1::2]

    real_vars = [
        ROOT.RooRealVar(name, title, value)
        for (name, title, value) in zip(param_names, param_titles, values)
    ]
    for (var, error) in zip(real_vars, errors):
        var.setError(error)

    formulae_names = [
        "mean_model",
        "sigma_model",
        "gamma_model",
        "beta_model",
        "mu_model",
        "frac_model",
    ]
    formulae = [
        ROOT.RooFormulaVar(
            name, "@1 * @0 + @2", ROOT.RooArgList(mtop, grad, intercept)
        )
        for (name, grad, intercept) in zip(
            formulae_names, real_vars[0::2], real_vars[1::2]
        )
    ]

    gaus = ROOT.RooGaussian(
        "gaus", "gaus", observable, formulae[0], formulae[1]
    )
    gamma = ROOT.RooGamma(
        "gamma", "gamma", observable, formulae[2], formulae[3], formulae[4]
    )

    pdf = ROOT.RooAddPdf(
        "model_combined", "model_combined", gaus, gamma, formulae[5]
    )
    getattr(w, "import")(pdf)


def perform_calibration(use_ml: bool = False, has_selection: bool = True):
    from pandas import read_csv
    from fitting import fit_linear

    if not os.path.exists("gaus_param.csv"):
        pdf_fitting(use_ml, has_selection)

    df = read_csv("gaus_param.csv")
    print(df)
    entry_172p5 = df[df["mass"] == 172.5]
    df = df[df["mass"] != 172.5]

    print(df)
    print(entry_172p5)

    mean_results = list(
        fit_linear(
            df["mass"],
            df["mean"],
            df["mean_err"],
            "Mean Model",
            "Mean",
            entry_172p5["mean"].values.tolist()[0],
            entry_172p5["mean_err"].values.tolist()[0],
        )
    )
    sigma_results = list(
        fit_linear(
            df["mass"],
            df["sigma"],
            df["sigma_err"],
            "Sigma Model",
            "#sigma",
            entry_172p5["sigma"].values.tolist()[0],
            entry_172p5["sigma_err"].values.tolist()[0],
        )
    )
    return (mean_results, sigma_results)


def perform_calibration_2gaus(use_ml: bool = False):
    from pandas import read_csv
    from fitting import fit_linear

    if not os.path.exists("gaus_com_param.csv"):
        pdf_fitting(use_ml)

    df = read_csv("gaus_com_param.csv")
    entry_172p5 = df[df["mass"] == 172.5]
    df = df[df["mass"] != 172.5]

    mean_results = list(
        fit_linear(
            df["mass"],
            df["mean"],
            df["mean_err"],
            "Primary Mean Model",
            "Mean",
            entry_172p5["mean"].values.tolist()[0],
            entry_172p5["mean_err"].values.tolist()[0],
        )
    )
    sigma_results = list(
        fit_linear(
            df["mass"],
            df["sigma"],
            df["sigma_err"],
            "Primary Sigma Model",
            "#sigma",
            entry_172p5["sigma"].values.tolist()[0],
            entry_172p5["sigma_err"].values.tolist()[0],
        )
    )
    mean_0_results = list(
        fit_linear(
            df["mass"],
            df["mean_0"],
            df["mean_0_err"],
            "Secondary Mean Model",
            "Mean",
            entry_172p5["mean_0"].values.tolist()[0],
            entry_172p5["mean_0_err"].values.tolist()[0],
        )
    )
    sigma_0_results = list(
        fit_linear(
            df["mass"],
            df["sigma_0"],
            df["sigma_0_err"],
            "Secondary Sigma Model",
            "#sigma",
            entry_172p5["sigma_0"].values.tolist()[0],
            entry_172p5["sigma_0_err"].values.tolist()[0],
        )
    )
    frac_results = list(
        fit_linear(
            df["mass"],
            df["frac"],
            df["frac_err"],
            "Frac Model",
            "Frac",
            entry_172p5["frac"].values.tolist()[0],
            entry_172p5["frac"].values.tolist()[0],
        )
    )
    return (
        mean_results,
        sigma_results,
        mean_0_results,
        sigma_0_results,
        frac_results,
    )


def perform_calibration_combined(use_ml: bool = False, has_selection=True):
    from pandas import read_csv
    from fitting import fit_linear

    if not os.path.exists("combined_param.csv"):
        pdf_fitting(use_ml, has_selection)

    df = read_csv("combined_param.csv")
    print(df)
    entry_172p5 = df[df["mass"] == 172.5]
    df = df[df["mass"] != 172.5]

    print(df)
    print(entry_172p5)

    mean_results = list(
        fit_linear(
            df["mass"],
            df["mean"],
            df["mean_err"],
            "Mean Model",
            "Mean",
            entry_172p5["mean"].values.tolist()[0],
            entry_172p5["mean_err"].values.tolist()[0],
        )
    )
    sigma_results = list(
        fit_linear(
            df["mass"],
            df["sigma"],
            df["sigma_err"],
            "Sigma Model",
            "#sigma",
            entry_172p5["sigma"].values.tolist()[0],
            entry_172p5["sigma_err"].values.tolist()[0],
        )
    )
    gamma_results = list(
        fit_linear(
            df["mass"],
            df["gamma"],
            df["gamma_err"],
            "Gamma Model",
            "#gamma",
            entry_172p5["gamma"].values.tolist()[0],
            entry_172p5["gamma_err"].values.tolist()[0],
        )
    )
    beta_results = list(
        fit_linear(
            df["mass"],
            df["beta"],
            df["beta_err"],
            "Beta Model",
            "#beta",
            entry_172p5["beta"].values.tolist()[0],
            entry_172p5["beta_err"].values.tolist()[0],
        )
    )
    mu_results = list(
        fit_linear(
            df["mass"],
            df["mu"],
            df["mu_err"],
            "mu Model",
            "#mu",
            entry_172p5["mu"].values.tolist()[0],
            entry_172p5["mu_err"].values.tolist()[0],
        )
    )
    frac_results = list(
        fit_linear(
            df["mass"],
            df["frac"],
            df["frac_err"],
            "Frac Model",
            "frac",
            entry_172p5["frac"].values.tolist()[0],
            entry_172p5["frac_err"].values.tolist()[0],
        )
    )
    return (
        mean_results,
        sigma_results,
        gamma_results,
        beta_results,
        mu_results,
        frac_results,
    )


def smooth_morphing(w, mean_param, sigma_param):
    data = get_172p5_data(w)
    m = w.var("m")
    construct_pdf(w, m, mean_param, sigma_param)
    pdf = w.pdf("model_gaus")
    mtop = w.var("mtop")

    w.Print()

    sample_mean = data.mean(m)
    sample_sigma = data.sigma(m)
    r_start = sample_mean - 2 * sample_sigma
    r_end = sample_mean + 2 * sample_sigma
    if r_start < 0:
        r_start = 0
    if r_end > 300:
        r_end = 300
    assert r_start < r_end

    cutspec = f"{r_start} < m && m < {r_end}"
    mtop.setRange("fitRange", r_start, r_end)

    fit_result = pdf.fitTo(
        data,
        ROOT.RooFit.Range('fitRange'),
        ROOT.RooFit.SumW2Error(True),
        ROOT.RooFit.Strategy(2),
        ROOT.RooFit.Minos(True),
        ROOT.RooFit.Save(),
        ROOT.RooFit.PrintLevel(3),
    )
    print("fit events:", data.sumEntries(cutspec, "fitRange"))
    print("all events:", data.sumEntries())

    frame = m.frame()
    frame.GetXaxis().SetTitle("m_{l+J/#psi} [GeV]")
    frame.SetMinimum(0)
    data.plotOn(frame, ROOT.RooFit.Name("data"))
    pdf.plotOn(frame, ROOT.RooFit.Name("gaus"))
    c = ROOT.TCanvas("c", "c", 800, 700)
    c.cd()
    frame.Draw()

    n = fit_result.floatParsFinal().getSize()
    value = frame.chiSquare("gaus", "data", n)
    print("chi2", value, n)
    autils.DrawText(0.5, 0.69, f"#chi^{{2}}/dof = {value:.2f}", size=0.03)

    astyle.ATLASLabel(0.4, 0.87, "Simulation")
    autils.DrawText(0.5, 0.82, "#sqrt{s} = 13 TeV", size=0.03)
    autils.DrawText(
        0.5,
        0.78,
        (
            f"m_{{top}} = {mtop.getValV():.2f}"
            f" #pm {mtop.getError():.2f} (stat).\n"
        ),
        size=0.03,
    )

    legend = ROOT.TLegend(0.55, 0.5, 0.9, 0.6)
    legend.AddEntry(data, "MC m_{top} = 172.5 GeV Data", "p l")
    ent = legend.AddEntry(pdf, "Smooth Morphing Model", "l")
    ent.SetLineColor(4)
    legend.SetBorderSize(0)
    legend.SetFillColor(0)
    legend.SetTextSize(0.03)
    legend.Draw()

    c.Update()
    c.SaveAs("./plots/smooth_morphing_model.png")


def smooth_morphing_2gaus(
    w, mean_param, sigma_param, mean_0_param, sigma_0_param, frac_param
):
    data = get_172p5_data(w)
    m = w.var("m")
    construct_pdf_2gaus(
        w, m, mean_param, sigma_param, mean_0_param, sigma_0_param, frac_param
    )
    pdf = w.pdf("model_2gaus")
    mtop = w.var("mtop")

    w.Print()

    sample_mean = data.mean(m)
    sample_sigma = data.sigma(m)
    r_start = sample_mean - 2 * sample_sigma
    r_end = sample_mean + 2 * sample_sigma
    if r_start < 0:
        r_start = 0
    if r_end > 300:
        r_end = 300
    assert r_start < r_end

    pdf.fitTo(
        data,
        ROOT.RooFit.Range(r_start, r_end),
        ROOT.RooFit.SumW2Error(True),
        ROOT.RooFit.Strategy(2),
        ROOT.RooFit.Minos(True),
        ROOT.RooFit.Save(),
        ROOT.RooFit.PrintLevel(3),
    )

    frame = m.frame()
    frame.GetXaxis().SetTitle("m_{l+J/#psi} [GeV]")
    frame.SetMinimum(0)
    data.plotOn(frame)
    pdf.plotOn(frame)
    c = ROOT.TCanvas("c", "c", 800, 700)
    c.cd()
    frame.Draw()

    astyle.ATLASLabel(0.4, 0.87, "Simulation")
    autils.DrawText(0.5, 0.82, "#sqrt{s} = 13 TeV", size=0.03)
    autils.DrawText(
        0.5,
        0.78,
        (
            f"m_{{top}} = {mtop.getValV():.2f}"
            f" #pm {mtop.getError():.2f} (stat).\n"
        ),
        size=0.03,
    )

    legend = ROOT.TLegend(0.55, 0.5, 0.9, 0.6)
    legend.AddEntry(data, "MC m_{top} = 172.5 GeV Data", "p l")
    ent = legend.AddEntry(pdf, "Smooth Morphing Model", "l")
    ent.SetLineColor(4)
    legend.SetBorderSize(0)
    legend.SetFillColor(0)
    legend.SetTextSize(0.03)
    legend.Draw()

    c.Update()
    c.SaveAs("./plots/smooth_morphing_model_2gaus.png")


def smooth_morphing_combined(
    w,
    mean_param,
    sigma_param,
    gamma_param,
    beta_param,
    mu_param,
    frac_param,
    use_ml=False,
    has_selection=True,
):
    data = get_172p5_data(w, use_ml, has_selection)
    m = w.var("m")
    construct_pdf_combined(
        w,
        m,
        mean_param,
        sigma_param,
        gamma_param,
        beta_param,
        mu_param,
        frac_param,
    )
    pdf = w.pdf("model_combined")
    mtop = w.var("mtop")
    w.Print()

    sample_mean = data.mean(m)
    sample_sigma = data.sigma(m)
    r_start = sample_mean - 2 * sample_sigma
    r_end = sample_mean + 2 * sample_sigma
    if r_start < 0:
        r_start = 0
    if r_end > 300:
        r_end = 300
    assert r_start < r_end

    cutspec = f"{r_start} < m && m < {r_end}"
    mtop.setRange("fitRange", r_start, r_end)

    fit_result = pdf.fitTo(
        data,
        ROOT.RooFit.Range("fitRange"),
        ROOT.RooFit.SumW2Error(True),
        ROOT.RooFit.Strategy(2),
        ROOT.RooFit.Minos(True),
        ROOT.RooFit.Save(),
        ROOT.RooFit.PrintLevel(2),
    )
    print("fit events:", data.sumEntries(cutspec, "fitRange"))
    print("all events:", data.sumEntries())

    frame = m.frame()
    frame.GetXaxis().SetTitle("m_{l+J/#psi} [GeV]")
    frame.SetMinimum(0)
    data.plotOn(frame, ROOT.RooFit.Name("data"))
    pdf.plotOn(frame, ROOT.RooFit.Name("model_combined"))
    c = ROOT.TCanvas("c", "c", 800, 700)
    c.cd()
    frame.Draw()

    n = fit_result.floatParsFinal().getSize()
    value = frame.chiSquare("model_combined", "data", n)
    print("chi2", value, n)
    autils.DrawText(0.5, 0.69, f"#chi^{{2}}/dof = {value:.2f}", size=0.03)

    astyle.ATLASLabel(0.4, 0.87, "Simulation")
    autils.DrawText(0.5, 0.82, "#sqrt{s} = 13 TeV", size=0.03)
    autils.DrawText(
        0.5,
        0.78,
        (
            f"m_{{top}} = {mtop.getValV():.2f}"
            f" #pm {mtop.getError():.2f} (stat).\n"
        ),
        size=0.03,
    )

    legend = ROOT.TLegend(0.55, 0.5, 0.9, 0.6)
    legend.AddEntry(data, "MC m_{top} = 172.5 GeV Data", "p l")
    ent = legend.AddEntry(pdf, "Smooth Morphing Model", "l")
    ent.SetLineColor(4)
    legend.SetBorderSize(0)
    legend.SetFillColor(0)
    legend.SetTextSize(0.03)
    legend.Draw()

    c.Update()
    c.SaveAs("./plots/smooth_morphing_model_combined.png")


def get_172p5_data(w, use_ml=False, has_selection=True):
    ds = w.data("ds172p5")
    if ds:
        return ds

    filename = "sorted_datasets_ml.root" if use_ml else "sorted_datasets.root"
    getattr(w, "import")(f"{filename}:w:ds172p5")
    getattr(w, "import")(f"{filename}:w:ds172p5_mm")
    ds = w.data("ds172p5")
    if not has_selection:
        ds_mm = w.data("ds172p5_mm")
        ds.add(ds_mm)
    return ds


if __name__ == "__main__":
    # w = ROOT.RooWorkspace("w", "w")
    # mean_param, sigma_param = perform_calibration(
        # use_ml=False, has_selection=True
    # )
    # smooth_morphing(w, mean_param, sigma_param)
    # del w

    # w = ROOT.RooWorkspace("w", "w")
    # (
        # mean_param,
        # sigma_param,
        # mean_0_param,
        # sigma_0_param,
        # frac_param,
    # ) = perform_calibration_2gaus(use_ml=False)
    # smooth_morphing_2gaus(
        # w, mean_param, sigma_param, mean_0_param, sigma_0_param, frac_param
    # )
    # del w

    w = ROOT.RooWorkspace("w", "w")
    results = perform_calibration_combined(use_ml=True, has_selection=True)
    smooth_morphing_combined(w, *results, use_ml=True, has_selection=True)
    del w
