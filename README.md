# Top Mass analysis using a lepton+J/Psi channel

## Setting up the code

All you need is root and python3. On UCT servers (running Scientific Linux):
```bash
ATLAS_LOCAL_ROOT_BASE="/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/"
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
lsetup "views LCG_96bpython3 x86_64-slc6-gcc8-opt"
```

## Event Selection

The event selection code runs over separate decay modes of the W boson, i.e. muon and electron channels.
This code only keeps the events of interest by producing a smaller ntuple.
This code runs over both data and MC for each year in Run2 (2015-2018).

To run the entire event selection:
```shell
source filter_data.sh
```
This will output all data to a folder in the home directory called `DATAMC`.

### Merging

The outputs of the event selection are separate root files for each individual process.
This is to ensure when compare to MC to data, we take into account the correct normalisation coefficiencts for each process.

## Template Morphing

The template morphing technique takes existing distributions (i.e. tamples) for various generated top mass values (i.e. mtop = 169 - 176 GeV)
and interpolates these templates to find the best fit to data. This is done by linearly combining the different templates.
This has been replaced with the smooth morphing method.

### Running template morphing

The outputs of the event selection needs to be merged for each mtop sample for each year. The template morphing code runs over those ntuples.

## Smooth Morphing
This method uses linear modelling created from templates to fit the parameters of the distribution.
This can be run using `main.py`.
