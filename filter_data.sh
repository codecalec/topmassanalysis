#!/bin/bash

HOME_DIR=/atlas/aveltman
EVENT_SELECTION_DIR=$HOME_DIR/Kevin_TopMassAnalysis/Event_Selection
DATA_DIR=$HOME_DIR/DATAMC/TopMass/topParentNtuples

python $EVENT_SELECTION_DIR/topMass_electronChannel.py > $EVENT_SELECTION_DIR/el_log.txt
python $EVENT_SELECTION_DIR/topMass_muonChannel.py > $EVENT_SELECTION_DIR/mu_log.txt

DSIDANTITOPLIST=($(seq 411143 2 411159))
DSIDTOPLIST=($(seq 411144 2 411160))
MASSLIST="mtop169 mtop171 mtop172 mtop172p25 mtop172p5 mtop172p75 mtop173 mtop174 mtop176"


i=0
for MASS in $MASSLIST
do
    mkdir $DATA_DIR/$MASS
    hadd -f $DATA_DIR/$MASS/$MASS.root $DATA_DIR/201*/${DSIDANTITOPLIST[$i]}**.root $DATA_DIR/201*/${DSIDTOPLIST[$i]}**.root >> output.txt
    i=$(( $i + 1 ))
done
