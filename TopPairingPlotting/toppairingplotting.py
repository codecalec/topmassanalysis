import ROOT
import os
import errno
from atlasplots import atlas_style as astyle
from functools import partial

from numpy import sqrt, pi

astyle.SetAtlasStyle()
ROOT.gROOT.SetBatch(ROOT.kTRUE)


# Define some useful functions
def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


# Take input list and load the tree that has the relevant information,
# checks the number of entries and returns the tree
def getTree(dsidList, yearList):
    fileList = []
    for year in yearList:
        for dsid in dsidList:
            fileList.append(
                "/atlas/aveltman/DATAMC/TopMass/topParentNtuples/"
                + year
                + "/"
                + dsid
                + "*"
            )
    tree = ROOT.TChain("nominal")
    for fileName in fileList:
        print("Adding from file", fileName)
        tree.Add(fileName)
    print(tree.GetEntries(), "entries in dsids", dsidList)
    return tree


# Take the event selection output and matches the lepton with the J/Psi
# that pass the selection criteria

# Get the trilepton mass and angular properties
def eventProperties(event):
    Ji = event.Jpsi_selected
    Jpt, Jeta, Jphi, Jm = (
        event.Jpsi_pt[Ji],
        event.Jpsi_eta[Ji],
        event.Jpsi_phi[Ji],
        event.Jpsi_m[Ji],
    )
    J4V = ROOT.TLorentzVector()
    J4V.SetPtEtaPhiM(Jpt, Jeta, Jphi, Jm)

    Li, Lpt, Leta, Lphi, Le = -1, 0.0, 0.0, 0.0, 0.0
    if event.electron_selected >= 0:
        Li = event.electron_selected
        Lpt, Leta, Lphi, Le = (
            event.electron_pt[Li],
            event.electron_eta[Li],
            event.electron_phi[Li],
            event.electron_E[Li],
        )
    elif event.muon_selected >= 0:
        Li = event.muon_selected
        Lpt, Leta, Lphi, Le = (
            event.muon_pt[Li],
            event.muon_eta[Li],
            event.muon_phi[Li],
            event.muon_E[Li],
        )
    else:
        print("bad!")
    L4V = ROOT.TLorentzVector()
    L4V.SetPtEtaPhiE(Lpt, Leta, Lphi, Le)

    delta_phi, delta_eta, delta_R = delta_angles(J4V, L4V)
    return m3l(J4V, L4V), delta_phi, delta_eta, delta_R


def m3l(J4V, L4V):
    return (J4V + L4V).M() * 0.001


# Gain the differences in the angular components of lepton and Jpsi
def delta_angles(J4V, L4V):
    delta_phi = J4V.DeltaPhi(L4V)
    delta_eta = J4V.Eta() - L4V.Eta()
    delta_R = J4V.DeltaR(L4V)
    return delta_phi, delta_eta, delta_R


# Take into account event weights and detector effects
def evtW(event):
    eventWeight = 1.0
    eventWeight *= (
        # event.mcWeightOrg
        event.jvtEventWeight
        * event.pileupEventWeight
        * event.MV2c10_77_EventWeight
        # / event.totalEventsWeighted
    )

    # Muons from Jpsi
    muonsFromJpsi = []
    for ji, (pt, decay_muons) in enumerate(
        zip(event.Jpsi_pt, event.Jpsi_muons)
    ):
        if ji == event.Jpsi_selected:
            for d in decay_muons:
                muonsFromJpsi.append(d)
    eventWeight *= (
        event.MU_SF_ID_LowPt[muonsFromJpsi[0]]
        * event.MU_SF_ID_LowPt[muonsFromJpsi[1]]
    )

    Li = -1
    if event.electron_selected >= 0:
        Li = event.electron_selected
        eventWeight *= (
            event.EL_SF_Trigger_TightLH[Li]
            * event.EL_SF_Reco[Li]
            * event.EL_SF_ID_TightLH[Li]
            * event.EL_SF_Iso_Gradient[Li]
        )
    elif event.muon_selected >= 0:
        Li = event.muon_selected
        eventWeight *= (
            event.MU_SF_Trigger_Medium[Li]
            * event.MU_SF_ID_Medium[Li]
            * event.MU_SF_TTVA[Li]
            * event.MU_SF_Isol_Gradient[Li]
        )
    else:
        print("bad!")
    return eventWeight


# Store plots in a folder
mkdir_p("./plots")

# Create file to store Histograms
f = ROOT.TFile("hists.root", "RECREATE")

# Import the Monte Carlo datasets, and the ATLAS data. Define the TTrees.
tree172p5 = getTree(
    dsidList=["411143", "411144"], yearList=["2015", "2016", "2017", "2018"]
)

# Fill histograms
# Tri lepton mass
hmass_antitop = ROOT.TH1F("hmass_antitop", "hmass_antitop", 60, 0, 300)
hmass_antitop_reject = ROOT.TH1F(
    "hmass_antitop_reject", "hmass_antitop_reject", 60, 0, 300
)

hmass_antitop = ROOT.TH1F("hmass_top", "hmass_top", 60, 0, 300)
hmass_antitop_reject = ROOT.TH1F(
    "hmass_top_reject", "hmass_top_reject", 60, 0, 300
)

hmass = ROOT.TH1F("hmass", "hmass", 60, 0, 300)
hmass_reject = ROOT.TH1F("hmass_reject", "hmass_reject", 60, 0, 300)

# delta phi

hphi = ROOT.TH1F("hphi", "hphi", 60, 0, pi)
hphi_reject = ROOT.TH1F("hphi_reject", "hphi_reject", 60, 0, pi)

# delta eta
heta = ROOT.TH1F("heta", "heta", 60, -4, 4)
heta_reject = ROOT.TH1F("heta_reject", "heta_reject", 60, -4, 4)

# delta R
hR = ROOT.TH1F("hR", "hR", 60, 0, 5.5)
hR_reject = ROOT.TH1F("hR_reject", "hR_reject", 60, 0, 5.5)

# 2d phi eta
h2phi_eta = ROOT.TH2F("h2phi_eta", "h2phi_eta", 60, 0, pi, 60, -7, 7)
h2phi_eta_reject = ROOT.TH2F(
    "h2phi_eta_reject", "h2phi_eta_reject", 60, 0, pi, 60, -7, 7
)

tree_list = [tree172p5]
for tree in tree_list:
    for event in tree:
        mass, phi, eta, R = eventProperties(event)
        if 0 <= mass <= 300:
            lepton_parent = -2
            if event.electron_selected >= 0:
                Li = event.electron_selected
                if event.electron_truth_pdgId[Li] < 0:
                    lepton_parent = 1
                else:
                    lepton_parent = -1
            elif event.muon_selected >= 0:
                Li = event.muon_selected
                if event.muon_truth_pdgId[Li] < 0:
                    lepton_parent = 1
                else:
                    lepton_parent = -1
            else:
                print("bad!")

            Ji = event.Jpsi_selected
            Jpsi_barcode = event.Jpsi_truthMatch_barcode[Ji]
            Jpsi_parent = -2
            for i, barcode in enumerate(event.truth_barcode):
                if barcode == Jpsi_barcode:
                    Jpsi_topparent = bool(event.truth_topParent[i])
                    Jpsi_antitopparent = bool(event.truth_antiTopParent[i])
                    if Jpsi_topparent == Jpsi_antitopparent:
                        Jpsi_parent = -2
                    elif Jpsi_topparent == 1:
                        Jpsi_parent = 1
                    elif Jpsi_antitopparent == 1:
                        Jpsi_parent = -1
                    break

            if Jpsi_parent == lepton_parent:
                hmass.Fill(mass, evtW(event))
                hphi.Fill(phi, evtW(event))
                heta.Fill(eta, evtW(event))
                hR.Fill(R, evtW(event))

                h2phi_eta.Fill(phi, eta, evtW(event))
            else:
                hmass_reject.Fill(mass, evtW(event))
                hphi_reject.Fill(phi, evtW(event))
                heta_reject.Fill(eta, evtW(event))
                hR_reject.Fill(R, evtW(event))

                h2phi_eta_reject.Fill(phi, eta, evtW(event))

# Save Hists to file
f.Write()


c = ROOT.TCanvas("c", "c", 800, 700)

# Plot properties of correctly and incorrectly paired events
hists_accept = [hphi, heta, hR]
hists_reject = [hphi_reject, heta_reject, hR_reject]
hists_list = zip(hists_accept, hists_reject)


hphi_total = hphi.Clone("hphi_ratio")
hphi_total.Add(hphi_reject)
for i in range(hphi_total.GetNbinsX()):
    hphi_total.SetBinContent(i, sqrt(hphi_total.GetBinContent(i)))

hphi_ratio = hphi.Clone("hphi_ratio")
hphi_ratio.Divide(hphi_total)

hcphi_ratio = hphi_ratio.GetCumulative()

hcphi_ratio.GetXaxis().SetTitle("\\Delta \\phi")
hcphi_ratio.SetFillColor(3)
hcphi_ratio.Draw()
legend = ROOT.TLegend(0.2, 0.7, 0.4, 0.9)
legend.AddEntry(hcphi_ratio, "#\\frac{S}{\\sqrt{(S+B)}}")
legend.SetBorderSize(0)
legend.SetFillColor(0)
legend.SetTextSize(0.03)
legend.Draw()
c.Update()
c.SaveAs("./plots/hist_ratio.png")

hR_total = hR.Clone("hphi_ratio")
hR_total.Add(hR_reject)
for i in range(hR_total.GetNbinsX()):
    hR_total.SetBinContent(i, sqrt(hR_total.GetBinContent(i)))

hR_ratio = hR.Clone("hR_ratio")
hR_ratio.Divide(hR_total)

hcR_ratio = hR_ratio.GetCumulative()

hcR_ratio.GetXaxis().SetTitle("\\Delta R")
hcR_ratio.SetFillColor(3)
hcR_ratio.Draw()
legend = ROOT.TLegend(0.2, 0.7, 0.4, 0.9)
legend.AddEntry(hcR_ratio, "#\\frac{S}{\\sqrt{(S+B)}}")
legend.SetBorderSize(0)
legend.SetFillColor(0)
legend.SetTextSize(0.03)
legend.Draw()
c.Update()
c.SaveAs("./plots/hR_ratio.png")


x_labels = ["\\Delta \\phi", "\\Delta \\eta", "\\Delta R"]

for (hist, hist_mm), x_label in zip(
    hists_list, x_labels
):

    hist.GetXaxis().SetTitle(x_label)
    hist.GetYaxis().SetTitle("Count")
    hist_mm.SetMarkerColor(ROOT.kBlue)

    hist.Draw("E P")
    hist_mm.Draw("E P SAME")
    hist.SetMaximum(hist.GetMaximum() * 1.7)
    hist.SetMinimum(0)

    legend = ROOT.TLegend(0.2, 0.7, 0.3, 0.9)
    legend.AddEntry(hist, "Correctly Paired")
    legend.AddEntry(hist_mm, "Incorrectly Paired")
    legend.SetBorderSize(0)
    legend.SetFillColor(0)
    legend.SetTextSize(0.03)
    legend.Draw()
    c.Update()
    c.SaveAs("./plots/hist_" + hist.GetName() + ".png")
    print("Saving plot:", hist.GetName())

hists2d_list = [h2phi_eta, h2phi_eta_reject]

for hist in hists2d_list:
    hist.Draw("colz")
    c.Update()
    c.SaveAs("./plots/hist_" + hist.GetName() + ".png")

# Plot Mass histograms and fit functions
# Accept histogram and fitting Crystal Ball
c = ROOT.TCanvas("c", "c", 800, 700)

var_mass = ROOT.RooRealVar("mass", "mass", 0, 300)
frame_mass = var_mass.frame()

# set up colours for plots
linecolors = [
    ROOT.RooFit.LineColor(ROOT.kBlue),
    ROOT.RooFit.LineColor(ROOT.kGreen),
    ROOT.RooFit.LineColor(ROOT.kRed),
]
histcolors = [
    ROOT.RooFit.MarkerColor(ROOT.kBlue),
    ROOT.RooFit.MarkerColor(ROOT.kGreen),
    ROOT.RooFit.MarkerColor(ROOT.kRed),
]

hmass_combined = hmass + hmass_reject
hmass_combined.SetName("hmass_combined")

hists_mass = [hmass_combined, hmass, hmass_reject]

means, errors = [], []

for hist, linecolor, histcolor in zip(hists_mass, linecolors, histcolors):

    mean = ROOT.RooRealVar("mean", "mean", 70, 60, 80)
    sigma = ROOT.RooRealVar("sigma", "sigma", 20, 10, 35)
    alpha = ROOT.RooRealVar("alpha", "alpha", -2, -10, -0)
    n = ROOT.RooRealVar("n", "n", 8, 0, 20)

    crystalball = ROOT.RooCBShape("cb", "cb", var_mass, mean, sigma, alpha, n)

    data = ROOT.RooDataHist("data", "data", ROOT.RooArgList(var_mass), hist)
    crystalball.fitTo(data, ROOT.RooFit.Range(30.0, 150.0))

    from ROOT.RooFit import Name

    data.plotOn(frame_mass, Name("data_" + hist.GetName()), histcolor)
    crystalball.plotOn(
        frame_mass, Name("fit_" + hist.GetName()), linecolor, histcolor
    )

    means.append(mean.getValV())
    errors.append(mean.getError())

print(means, errors)

frame_mass.GetXaxis().SetTitle("m3l")

frame_mass.Draw("e same")
legend = ROOT.TLegend(0.6, 0.7, 0.8, 0.9)
legend.SetBorderSize(0)
legend.SetTextSize(0.03)
legend.AddEntry("", "m^{gen}_{t} = 172.5 GeV")


def rnd(x):
    return list(map(partial(round, ndigits=1), x))


means, errors = rnd(means), rnd(errors)

fit_hmass_combined = frame_mass.findObject("fit_hmass_combined")
legend.AddEntry(
    fit_hmass_combined, "m_t= {0} \\pm {1}".format(means[0], errors[0]), "P L"
)

fit_hmass = frame_mass.findObject("fit_hmass")
legend.AddEntry(
    fit_hmass, "m_g = {0} \\pm {1}".format(means[1], errors[1]), "P L"
)

fit_hmass_reject = frame_mass.findObject("fit_hmass_reject")
legend.AddEntry(
    fit_hmass_reject, "m_b = {0} \\pm {1}".format(means[2], errors[2]), "P L"
)
legend.Draw()

c.Update()
c.SaveAs("./plots/mass_pairing.png")
