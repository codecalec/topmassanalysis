import ROOT
from ROOT import TFile, TTree, TH1F, TString, std, TH1I, TLorentzVector, TChain,TCanvas, TH2F, TBranch
from array import array
from math import sqrt, cos
import branchCreator as bc
import string
import types
from time import sleep
import os, errno

###Loop over all data + mc samples
datadir = '/atlas/DATAMC/TopMass/data/'
dsamples = ['2015/elChan/','2016/elChan/','2017/elChan/','2018/elChan/']
samples2015 = []
samples2016 = []
samples2017 = []
samples2018 = []
mcdir = '/atlas/DATAMC/TopMass/mc/'
mctag = ['16a/','16d/','16e/']
ttsamples = ['410470/elChan/']
stsamples = ['410644/elChan/','410645/elChan/','410646/elChan/','410647/elChan/','410658/elChan/','410659/elChan/']
wjsamples = ['361100/elChan/','361101/elChan/','361103/elChan/','361104/elChan/']
zjsamples = ['361106/elChan/','361107/elChan/']
dbsamples = []
tt_templatesamples = ['411143/elChan/','411144/elChan/','411145/elChan/','411146/elChan/','411147/elChan/','411148/elChan/','411149/elChan/','411150/elChan/','411151/elChan/','411152/elChan/','411153/elChan/','411154/elChan/','411155/elChan/','411156/elChan/','411157/elChan/','411158/elChan/','411159/elChan/','411160/elChan/']
msamples = ttsamples+stsamples+wjsamples+zjsamples+tt_templatesamples
datasamples = []
for d,data in enumerate(dsamples):
    data = datadir+data
    if d == 0:
        samples2015.append(data)
    elif d == 1:
        samples2016.append(data)
    elif d == 2:
        samples2017.append(data)
    elif d == 3:
        samples2018.append(data)
    datasamples.append(data)
mcsamples = []
for m,mct in enumerate(mctag):
    for mc in msamples:
        mc = mcdir+mct+mc
        if m == 0:
            samples2015.append(mc)
            samples2016.append(mc)
        elif m == 1:
            samples2017.append(mc)
        elif m == 2:
            samples2018.append(mc)
        mcsamples.append(mc)
#electrontrigNames = ['HLT_e24_lhmedium_L1EM20VH','HLT_e60_lhmedium','HLT_e120_lhloose'] #2015 triggers
electrontrigNames = ['HLT_e26_lhtight_nod0_ivarloose','HLT_e60_lhmedium_nod0','HLT_e140_lhloose_nod0'] #2016-2018 triggers
samples = datasamples+mcsamples
sampleNames = ['data','410470','410644','410645','410646','410647','410658','410659','361100','361101','361103','361104','361106','361107','411143','411144','411145','411146','411147','411148','411149','411150','411151','411152','411153','411154','411155','411156','411157','411158','411159','411160']

#samples201* (k=0 is data, k>0 is mc)

for k,dirName in enumerate(zip(samples2018)):
    #if k != 0:
    #    continue
    print(dirName[0])

    ###Input files
    #Sample files
    Indir = str(dirName[0])
    FileList = os.listdir(Indir)
    inTree = TChain('nominal')
    for fileName in FileList[:]:
        inTree.Add(Indir+fileName)
    numEvents = inTree.GetEntries()
    print(numEvents)

    #outFileName = '/home/kbarends/workdir/PhD/Analysis/ntuples/Signal_Region/2015/elChan/'+sampleNames[k]+'.root' #2015
    #outFileName = '/home/kbarends/workdir/PhD/Analysis/ntuples/Signal_Region/2016/elChan/'+sampleNames[k]+'.root' #2016
    #outFileName = '/home/kbarends/workdir/PhD/Analysis/ntuples/Signal_Region/2017/elChan/'+sampleNames[k]+'.root' #2017
    outFileName = '/home/kbarends/workdir/PhD/Analysis/ntuples/Signal_Region/2018/elChan/'+sampleNames[k]+'.root' #2018
        
    # MC totalEventsWeighted value
    mcInd = str(dirName[0])
    mcFilel = os.listdir(mcInd)
    mcWeights = TChain('sumWeights')
    for fileName in mcFilel[:]:
        mcWeights.Add(mcInd+fileName)
    mcSWeights = 0
    for mcw in xrange(mcWeights.GetEntries()):
        mcWeights.GetEvent(mcw)
        mcSWeights += mcWeights.totalEventsWeighted

    ###Cuts to applied
    cuts = {} # list cuts here
    cuts['bJet_WP'] = 0.64 #77% efficiency
    cuts['bJet_num'] = 1
    cuts['bJet_pt'] = 25
    cuts['Jpsi_mass_min'] = 2.9
    cuts['Jpsi_mass_max'] = 3.3
    cuts['Jpsi_pt_min'] = 8
    cuts['Jpsi_y_max'] = 2.1
    cuts['Jpsi_tau_min'] = 0.0
    cuts['barrel_pt_min'] = 3.5
    cuts['endcap_pt_min'] = 2.5
    cuts['barrel_eta_max'] = 1.3
    cuts['endcap_eta_max'] = 2.5
    cuts['MET'] = 20
    cuts['MTW'] = 40
    cuts['Z_min'] = 81
    cuts['Z_max'] = 101
    cuts['W_lepton_pt'] = 25
    cuts['W_lepton_eta'] = 2.5
    cuts['W_electron_sigd0'] = 5.0
    cuts['W_muon_sigd0'] = 3.0
    cuts['W_lepton_deltaZsintheta'] = 0.5

    ###Jpsi_muons
    def Jpsi_muon_selections(tree,J_mu,muTrig,W_mu,PV,cuts):
        if J_mu == W_mu:
            return False
        endcap = False
        if cuts['barrel_eta_max']<abs(tree.muon_eta[J_mu])<cuts['endcap_eta_max'] and tree.muon_pt[J_mu]*.001>cuts['endcap_pt_min'] and tree.muon_isLowPt[J_mu] == 1:
            endcap = True
        barrel = False
        if abs(tree.muon_eta[J_mu])<cuts['barrel_eta_max'] and tree.muon_pt[J_mu]*.001>cuts['barrel_pt_min'] and tree.muon_isLowPt[J_mu] == 1:
            barrel = True
        muon_pass = endcap or barrel
            
        if muTrig==1 and tree.muon_ID[J_mu] == -tree.muon_ID[W_mu]: # if W->mu,nu, check that the oppositely charged muon does not make a Z boson
            JMuon4V,WMuon4V = ROOT.TLorentzVector(),ROOT.TLorentzVector()
            JMuon4V.SetPtEtaPhiM(tree.muon_pt[J_mu],tree.muon_eta[J_mu],tree.muon_phi[J_mu],105.66)
            WMuon4V.SetPtEtaPhiM(tree.muon_pt[W_mu],tree.muon_eta[W_mu],tree.muon_phi[W_mu],105.66)
            Z_mass[0] = (JMuon4V+WMuon4V).M()*.001
            if cuts['Z_min']<Z_mass<cuts['Z_max']:
                muon_pass = False
            
        return muon_pass
    
    ###define list of useful branches to keep
    hltBranches = electrontrigNames
        
    # Remove truth type branches in MC as branch address' is not 
    if k != 0:
        truthRemove = ["electron_truth_type","electron_truth_origin","muon_truth_type","muon_truth_origin"]
        for branch in truthRemove:
            inTree.SetBranchStatus(branch,0)

    outFile = TFile( outFileName, 'recreate' )
    outTree = inTree.CloneTree(0)
    
    electron_selected = bc.createIntBranch(outTree,'electron_selected',-1)
    electron_isTrigger = bc.createBoolBranch(outTree,'electron_isTrigger')
    muon_selected = bc.createIntBranch(outTree,'muon_selected',-1)
    muon_isTrigger = bc.createBoolBranch(outTree,'muon_isTrigger')
    Jpsi_selected = bc.createIntBranch(outTree,'Jpsi_selected',-1)
    Jmu_selected_1 = bc.createIntBranch(outTree,'Jmu_selected',-1)
    Jmu_selected_2 = bc.createIntBranch(outTree,'Jmu_selected',-1)
    Jpsi_z0 = bc.createFloatBranch(outTree,'Jpsi_z0')
    pVtx_z = bc.createFloatBranch(outTree,'pVtx_z',0)
    pVtx_index = bc.createFloatBranch(outTree,'pVtx_index',-1)
    deltaZ = bc.createFloatBranch(outTree,'deltaZ',-1)
    deltaPhi = bc.createFloatBranch(outTree,'deltaPhi',-1)
    top_m3l = bc.createFloatBranch(outTree,'top_m3l',0)
    Z_mass = bc.createFloatBranch(outTree,'Z_mass',0)
    totalEventsWeighted = bc.createFloatBranch(outTree,'totalEventsWeighted',0)

    #define steps in the cutflow
    cutflowKeys = ['Preselection','slTrigger','bJet','Wlepton','MET&MTW','JPsi']
    cutflowDict = dict((cutflowKey,0) for cutflowKey in cutflowKeys)

    #begin event loop
    for event in xrange(numEvents):
        if (event%1000000==0): print(event)
        #load the event
        nb = inTree.GetEvent(event)
        if nb<=0: '0 bytes read for event. Error.'
            
        #Save totalEventsWeighted in a branch
        totalEventsWeighted[0] = mcSWeights

        #find primary vertex
        for i,(v_z,v_type) in enumerate(zip(inTree.vtx_z,inTree.vtx_type)):
            if v_type==1:
                pVtx_index[0],pVtx_z[0] = i,v_z
                continue
                
        #Before event selection: setup cut values and counting variables
        bJet_WP = cuts['bJet_WP']
        bJet_num = cuts['bJet_num']
        bJet_pt = cuts['bJet_pt']
        Jpsi_mass_min = cuts['Jpsi_mass_min']
        Jpsi_mass_max = cuts['Jpsi_mass_max']
        Jpsi_pt_min = cuts['Jpsi_pt_min']
        Jpsi_y_max = cuts['Jpsi_y_max']
        Jpsi_tau_min = cuts['Jpsi_tau_min']
        barrel_pt_min = cuts['barrel_pt_min']
        endcap_pt_min = cuts['endcap_pt_min']
        barrel_eta_max = cuts['barrel_eta_max']
        endcap_eta_max = cuts['endcap_eta_max']
        METcut = cuts['MET']
        MTWcut = cuts['MTW']
        Z_min = cuts['Z_min']
        Z_max = cuts['Z_max']
        W_lepton_pt = cuts['W_lepton_pt']
        W_lepton_eta = cuts['W_lepton_eta']
        W_muon_sigd0 = cuts['W_electron_sigd0']
        W_electron_sigd0 = cuts['W_electron_sigd0']
        W_lepton_deltaZsintheta = cuts['W_lepton_deltaZsintheta']

        numElectronsperevent = 0
        numMuonsperelectronevent = 0
        numJpsiperevent = 0

        #Begin Event Selection
        #1) Single Lepton Trigger: Check that one of the desired event triggers is passed.
        if not any([getattr(inTree,_) for _ in hltBranches]):
            cutflowDict['slTrigger'] += 1
            continue            
            
        #2) b-Jet selection: Ensure 1 b-jet (i.e. MV2c10 working point > 0.64) with pt > 25 GeV
        if not sum([(flavor>bJet_WP and pt*.001>bJet_pt) for flavor,pt in zip(inTree.m_jet_flavorWeight_MV2c10,inTree.m_jet_pt)])>=bJet_num:
            cutflowDict['bJet'] += 1
            continue
            
        #3) Lepton from W decay
        electron_isTrigger[0] = False
        electron_selected[0] = -1
        #Muon from W
        for mu_i,(mu_pt,mu_eta,mu_phi,mu_sd0,mu_z0st) in enumerate(zip(inTree.muon_pt,inTree.muon_eta,inTree.muon_phi,inTree.muon_d0sig,inTree.muon_z0SinTheta)):
            if mu_pt*.001 > W_lepton_pt and abs(mu_eta)<W_lepton_eta and abs(mu_sd0) <= W_muon_sigd0 and abs(mu_z0st) <= W_lepton_deltaZsintheta and inTree.muon_isMedium[mu_i] == 1 and inTree.muon_isolationGradient[mu_i]:
                numMuonsperelectronevent += 1
            
        #Electron from W
        for el_i,(el_pt,el_eta,el_phi,el_sd0,el_z0st) in enumerate(zip(inTree.electron_pt,inTree.electron_eta,inTree.electron_phi,inTree.electron_d0sig,inTree.electron_z0SinTheta)):
            if el_pt*.001 > W_lepton_pt and abs(el_eta)<W_lepton_eta and abs(el_sd0) <= W_electron_sigd0 and abs(el_z0st) <= W_lepton_deltaZsintheta and inTree.electron_isTight[el_i] == 1 and inTree.electron_isolationGradient[el_i]:
                '''
                if inTree.electron_match_HLT_e24_lhmedium_L1EM20VH[el_i]:
                    electron_isTrigger[0] = True
                    electron_selected[0] = el_i
                    numElectronsperevent += 1
                elif inTree.HLT_e60_lhmedium and el_pt*.001 > 61. and inTree.electron_match_HLT_e60_lhmedium[el_i]:
                    electron_isTrigger[0] = True
                    electron_selected[0] = el_i
                    numElectronsperevent += 1
                elif inTree.HLT_e120_lhloose and el_pt*.001 > 121 and inTree.electron_match_HLT_e120_lhloose[el_i]:
                    electron_isTrigger[0] = True
                    electron_selected[0] = el_i
                    numElectronsperevent += 1
                '''
                if el_pt*.001 > 27 and inTree.electron_match_HLT_e26_lhtight_nod0_ivarloose[el_i]:
                    electron_isTrigger[0] = True
                    electron_selected[0] = el_i
                    numElectronsperevent += 1
                elif inTree.HLT_e60_lhmedium_nod0 and el_pt*.001 > 61. and inTree.electron_match_HLT_e60_lhmedium_nod0[el_i]:
                    electron_isTrigger[0] = True
                    electron_selected[0] = el_i
                    numElectronsperevent += 1
                elif inTree.HLT_e140_lhloose_nod0 and el_pt*.001 > 141. and inTree.electron_match_HLT_e140_lhloose_nod0[el_i]:
                    electron_isTrigger[0] = True
                    electron_selected[0] = el_i
                    numElectronsperevent += 1
                
        if numMuonsperelectronevent > 0: #ElectronChannel (no mu)
            cutflowDict['Wlepton'] += 1
            continue
        if numElectronsperevent != 1:#ElectronChannel (1 el)
            cutflowDict['Wlepton'] += 1
            continue
            

        #4) MET&MTW cuts
        MTW = sqrt(2.0*(inTree.electron_pt[electron_selected[0]]*.001)*abs(inTree.MET_RefFinal_et*.001)*(1-cos(inTree.electron_phi[electron_selected[0]]-inTree.MET_RefFinal_phi)))
        if inTree.MET_RefFinal_et*.001 < METcut or MTW < MTWcut:
            cutflowDict['MET&MTW'] += 1
            continue
            
        #5) Find only one J/psi's that pass selections
        Jpsi_selected[0] = -1 
        for i,(mass,pt,y,decay_muons,tau) in enumerate(zip(inTree.Jpsi_m,inTree.Jpsi_pt,inTree.Jpsi_y,inTree.Jpsi_muons,inTree.Jpsi_tau)):
            # check that the decay muons pass selections
            PV = pVtx_z[0]
            muons_pass = sum([Jpsi_muon_selections(inTree,J_mu,muon_isTrigger[0],muon_selected[0],PV,cuts) for J_mu in decay_muons])
            # if Jpsi passes all selections, claim that it is the selected object, unless another passing Jpsi has a smaller chi**2
            if cuts['Jpsi_mass_min']<mass*.001<cuts['Jpsi_mass_max']:
                if pt*.001>cuts['Jpsi_pt_min']:
                    if abs(y)<cuts['Jpsi_y_max']:
                        if muons_pass==2:
                            if tau > 0.0:
                                Jpsi_selected[0] = i
                                numJpsiperevent += 1
        if numJpsiperevent != 1:
            cutflowDict['JPsi'] += 1
            continue
        
        # =============================================================================================
        # End of event selections. If it has made it this far, decorate event with remaining variables.
        # =============================================================================================            
        
        # add variable for the top mass
        lep4V,Jpsi4V = TLorentzVector(),TLorentzVector()
        el_pt = inTree.electron_pt[electron_selected[0]]
        el_eta = inTree.electron_eta[electron_selected[0]]
        el_phi = inTree.electron_phi[electron_selected[0]]
        el_m = 0.511
        lep4V.SetPtEtaPhiM(el_pt,el_eta,el_phi,el_m)
        Jpsi_pt = inTree.Jpsi_pt[Jpsi_selected[0]]
        Jpsi_eta = inTree.Jpsi_eta[Jpsi_selected[0]]
        Jpsi_phi = inTree.Jpsi_phi[Jpsi_selected[0]]
        Jpsi_m = inTree.Jpsi_m[Jpsi_selected[0]]
        Jpsi4V.SetPtEtaPhiM(Jpsi_pt,Jpsi_eta,Jpsi_phi,Jpsi_m)
        top_m3l[0] = (lep4V+Jpsi4V).M()*.001

        # end of event loop. If all selections have been passed, add to the outgoing TTree.
        outTree.Fill()
	
    cf_len = len(cutflowKeys)
    cutflow = TH1F('cutflow','cutflow',cf_len,0,cf_len)
    previousBin = numEvents
    for i,binLabel in enumerate(cutflowKeys[:cf_len-1]):
        previousBin = previousBin-cutflowDict[binLabel]
        cutflow.Fill(i,previousBin)
        cutflow.GetXaxis().SetBinLabel(i+1,binLabel)
    cutflow.Fill(cf_len-1,outTree.GetEntries())
    cutflow.GetXaxis().SetBinLabel(cf_len,cutflowKeys[cf_len-1])
    cutflow.GetYaxis().SetTitle('events')
    outTree.Branch('cutflow','TH1F',cutflow)
    
    outFile.cd()
    outFile.Write() # writes to outFile
    outFile.Close()
