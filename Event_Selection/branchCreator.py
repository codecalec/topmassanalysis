from ROOT import TTree, std
from array import array

def createIntBranch(tree,branchName,initial=0):
	branch = array('i',[initial])
	tree.Branch(branchName,branch,branchName+'/I')
	return branch

def createFloatBranch(tree,branchName,initial=0.0):
	branch = array('f',[initial])
	tree.Branch(branchName,branch,branchName+'/F')
	return branch

def createStringBranch(tree,branchName,initial=''):
	branch = initial
	tree.Branch(branchName,branch,'string/C')
	return branch

def createVectorBranch(tree,branchName,vtype):
	branch = std.vector(vtype)()
	tree.Branch(branchName,branch)
	return branch

def createArrayBranch(tree,branchName,vtype):
	branch = std.vector(std.vector(vtype))()
	tree.Branch(branchName,branch)
	return branch

def createBoolBranch(tree,branchName,initial=False):
	branch = array('b',[initial])
	tree.Branch(branchName,branch,branchName+'/B')
	return branch
